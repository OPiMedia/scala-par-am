.. -*- restructuredtext -*-

============
Scala-Par-AM
============
**Parallel (Abstract) Abstract Machine Experiments using Scala**
(with Akka, to analyze Scheme programs)

This parallel abstract interpreter is a parallel version
(restricted to Scheme programs and the AAM implementation) of Scala-AM_.
It is done for the **master thesis**: `An efficient and parallel abstract interpreter in Scala`_.

* `scala-par-am/`_: the actual implementation, with README_

  * `src/main/scala/`_: **main sources** of this repository

    * `machine/`_: **implementations** of sequential and **parallel machines**

  * `Scheme-examples/`_: **Scheme program examples** used to benchmarks parallel implementations

.. _`An efficient and parallel abstract interpreter in Scala`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala/
.. _`machine/`: https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/machine/
.. _README: https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/README.rst
.. _Scala-AM: https://bitbucket.org/OPiMedia/scala-am
.. _`scala-par-am/`: https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/
.. _`Scheme-examples/`: https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/Scheme-examples/
.. _`src/main/scala/`: https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/

|


Other links
===========
* `scala-par-am.jar`_: the complete **executable** (for a JVM)
  (version compiled with hash memoization disabled: `scala-par-am-without-memo.jar`_)
* `scala-am.jar`_: the JAR file (from Scala-AM repository) to run the oldAAM machine (only required for this specific machine)

* `HTML online documentation`_ from the source code
* `HTML page of benchmark results`_ on Scheme examples
* Scala-AM_: the abstract sequential interpreter that was parallelized

.. _`HTML online documentation`: http://www.opimedia.be/DS/online-documentations/Scala-Par-AM/html/be/opimedia/scala_par_am/Main$.html
.. _`HTML page of benchmark results`: http://www.opimedia.be/CV/2017-2018-ULB/MEMO-F524-Masters-thesis/benchmark-results/
.. _`scala-am.jar`: https://bitbucket.org/OPiMedia/scala-am/downloads/scala-am.jar
.. _`scala-par-am.jar`: https://bitbucket.org/OPiMedia/scala-par-am/downloads/scala-par-am.jar
.. _`scala-par-am-without-memo.jar`: https://bitbucket.org/OPiMedia/scala-par-am/downloads/scala-par-am-without-memo.jar

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



|Scala-Par-AM|

`References for this picture`_

.. _`References for this picture`: https://bitbucket.org/OPiMedia/scala-par-am/src/master/_img/_src/_original/README.rst

.. |Scala-Par-AM| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/18/3258961855-5-scala-par-am-logo_avatar.png
