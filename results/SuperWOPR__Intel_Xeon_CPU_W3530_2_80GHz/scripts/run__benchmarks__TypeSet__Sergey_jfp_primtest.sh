#!/bin/sh

# ~1 hours 30

export PATH=/QUICK/progs/graalvm-ee-19.1.1/bin:$PATH


echo "=== Run " `date` "==="
LOGDIR=$PWD
cd ../../../scala-par-am

./cpuinfos.sh > $LOGDIR/log/benchmarks__TypeSet__Sergey_jfp_primtest__cpuinfos.log 2>&1

./scala-par-am.sh  \
  --root Scheme-examples \
  -f Scheme-examples/Sergey/jfp/primtest.scm \
  -l TypeSet \
  -m SeqAAMLS,ParAAMLSAState,ParAAMLSASet,ParAAMLSAPart,ParAAMCSState,ParAAMCSSet,ParAAMCSPart,ParAAMCState,ParAAMCSet,ParAAMCPart \
  -p 1,2,3,4,5,6,7,8 \
  --prepare-file Scheme-examples/Sergey/gcfa2/sat.scm \
  --repetition 13 \
  -t 10minute \
  > $LOGDIR/log/benchmarks__TypeSet__Sergey_jfp_primtest.tsv 2>&1

echo "=== Done " `date` "==="
