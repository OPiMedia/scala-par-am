#!/bin/sh

# Without ParAAMCSState,ParAAMCSSet
# ~15h10

echo "=== Run " `date` "==="

./cpuinfos.sh > log/benchmarks__TypeSet__OPi_linear_10000__cpuinfos.log 2>&1

./scala-par-am.sh \
  --root Scheme-examples \
  -f Scheme-examples/OPi/linear_10000.scm \
  -l TypeSet \
  -m SeqAAMLS,ParAAMLSAState,ParAAMLSASet,ParAAMLSAPart,ParAAMCSPart,ParAAMCState,ParAAMCSet,ParAAMCPart \
  -p 1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,24,28,32,40,48,56,64 \
  --prepare-file Scheme-examples/Sergey/gcfa2/sat.scm \
  --repetition 13 \
  -t 20minutes \
  > log/benchmarks__TypeSet__OPi_linear_10000.tsv 2>&1

echo "=== Done " `date` "==="
