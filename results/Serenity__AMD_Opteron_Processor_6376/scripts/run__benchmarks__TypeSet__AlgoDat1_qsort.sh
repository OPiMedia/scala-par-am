#!/bin/sh

# Without ParAAMCSState
# ~60h30

echo "=== Run " `date` "==="

./cpuinfos.sh > log/benchmarks__TypeSet__AlgoDat1_qsort__cpuinfos.log 2>&1

./scala-par-am.sh \
  --root Scheme-examples \
  -f Scheme-examples/AlgoDat1/qsort.scm \
  -l TypeSet \
  -m SeqAAMLS,ParAAMLSAState,ParAAMLSASet,ParAAMLSAPart,ParAAMCSSet,ParAAMCSPart,ParAAMCState,ParAAMCSet,ParAAMCPart \
  -p 1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,24,28,32,40,48,56,64 \
  --prepare-file Scheme-examples/Sergey/gcfa2/sat.scm \
  --repetition 13 \
  -t 10minutes \
  > log/benchmarks__TypeSet__AlgoDat1_qsort.tsv 2>&1

echo "=== Done " `date` "==="
