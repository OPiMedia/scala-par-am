#!/bin/bash

FILE="$1"
FILENAME="${FILE##*/}"
FILENAME="${FILENAME%.*}"

echo === yerrorbars_plots.sh on "$FILENAME" ===

TMP=_tmp.tsv
cp "$FILE" $TMP

./yerrorbars_time.pg > "eps/yerrorbars__${FILENAME}__time.eps"

./yerrorbars_speedup.pg > "eps/yerrorbars__${FILENAME}__speedup.eps"

./yerrorbars_efficiency.pg > "eps/yerrorbars__${FILENAME}__efficiency.eps"

./yerrorbars_overhead.pg > "eps/yerrorbars__${FILENAME}__overhead.eps"

rm $TMP
