#!/bin/bash

FILE="$1"
FILENAME="${FILE##*/}"
FILENAME="${FILENAME%.*}"

echo === plots.sh on "$FILENAME" ===

TMP=_tmp.tsv
cp "$FILE" $TMP

./time.pg > "eps/${FILENAME}__time.eps"

./speedup.pg > "eps/${FILENAME}__speedup.eps"

./efficiency.pg > "eps/${FILENAME}__efficiency.eps"

./overhead.pg > "eps/${FILENAME}__overhead.eps"

rm $TMP
