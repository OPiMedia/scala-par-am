#!/bin/sh

if [ -z $2 ]
then
    export GNUPLOT_P=32.3
else
    export GNUPLOT_P=$2
fi

if [ -z $3 ]
then
    export GNUPLOT_XTIC=2
else
    export GNUPLOT_XTIC=$3
fi

./plots.sh $1

./yerrorbars_plots.sh $1
