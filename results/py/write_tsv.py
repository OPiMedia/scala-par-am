#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
write_tsv.py (August 17, 2019)
"""

import collections
import sys

from typing import Any, Dict, Optional, Tuple

import data
import graph
import scheme


#############
# Functions #
#############

def print_benchmarks(benchmarks: data.Benchmarks,
                     scheme_program: str='', step_filter: Optional[bool]=None,
                     machine: str='', filter_time_min: float=0.0,
                     output_file=sys.stdout) -> None:
    for key, tmp in sorted(benchmarks.items(), key=sort_fct_key):
        for key_config, values in sorted(tmp.items(), key=sort_fct_key_config):
            if (((scheme_program == '') or key.scheme_program.endswith(scheme_program)) and
                    ((step_filter is None) or (key_config.step_filter == step_filter) or (key_config.machine == data.MACHINE_REF)) and
                    ((machine == '') or (key_config.machine == machine)) and
                    (filter_time_min <= values['time_min'])):
                print(tsv(key), tsv(key_config),
                      values['beta'],
                      values['nb_initial_chain'],
                      values['nb_state'],
                      values['nb_error_state'], values['nb_error_value'],
                      values['nb_final_state'], values['nb_final_value'],
                      values['finals'],
                      values['nb_word'], values['nb_exclamation'],

                      values['nb_result'],
                      values['nb_state/s'],
                      values['ideal'],

                      values['time_average'], values['time_error'],
                      values['time_min'], values['time_max'],

                      values['ideal_abstract_speedup'],
                      values['chain_abstract_speedup'],
                      values['ideal_abstract_speedup_weighted'],
                      values['chain_abstract_speedup_weighted'],
                      values['speedup_average'], values['speedup_error'],
                      values['speedup_min'], values['speedup_max'],

                      values['ideal_abstract_efficiency'],
                      values['chain_abstract_efficiency'],
                      values['efficiency_average'], values['efficiency_error'],
                      values['efficiency_min'], values['efficiency_max'],

                      values['minimal_abstract_overhead'],
                      values['chain_abstract_overhead'],
                      values['overhead_average'], values['overhead_error'],
                      values['overhead_min'], values['overhead_max'],
                      sep='\t',
                      file=output_file)


def sort_fct_key(key_value: Tuple[data.Key, Dict]) -> Tuple:
    key = key_value[0]
    path = key.scheme_program.upper().split('/')[1:]
    lenpath = len(path)
    return (key.lattice,
            key.bound,
            key.address,
            tuple(zip(path, [lenpath]*lenpath)))


def sort_fct_key_config(key_config_value: Tuple[data.KeyConfig, Dict]) -> Tuple:
    key_config = key_config_value[0]
    return (key_config.step_filter,
            sort_fct_machine(key_config.machine),
            key_config.nb_process)


def sort_fct_machine(machine: str) -> int:
    assert machine in data.MACHINES, machine

    return data.MACHINES.index(machine)


def tsv(item: Any) -> str:
    if isinstance(item, str):
        return item
    elif isinstance(item, collections.Sequence):
        return '\t'.join(tsv(item) for item in item)
    elif isinstance(item, bool):
        return ('true' if item
                else 'false')
    else:
        return str(item)


########
# Main #
########
def main():
    filename = '-'
    skip_repetition = 0

    filter_time_min = 0.0
    scheme_program = ''
    step_filter = None
    machine = ''

    output_file = sys.stdout
    output_filename = None

    def get_param():
        if not params:
            print('Missing param!', file=sys.stderr)

            exit(1)

        return params[0], params[1:]

    params = sys.argv[1:]
    while params:
        param, params = get_param()
        if param == '--machine':
            machine, params = get_param()
        elif param == '--time-min':
            param, params = get_param()
            filter_time_min = float(param)
        elif param == '-o':
            output_filename, params = get_param()
        elif param == '--reference':
            param, params = get_param()
            # ??? global data.MACHINE_REF
            data.MACHINE_REF = param
        elif param == '--scheme':
            scheme_program, params = get_param()
        elif param == '--skip-repetition':
            param, params = get_param()
            skip_repetition = int(param)
        elif param == '--step-filter':
            param, params = get_param()
            step_filter = (param.upper() == 'TRUE')
        elif param.startswith('-'):
            print('Error param "{}"!'.format(param), file=sys.stderr)

            exit(1)
        else:
            filename = param

    assert data.MACHINE_REF in data.MACHINES, data.MACHINE_REF

    benchmarks, max_nb_result = data.read_data(filename, skip_repetition=skip_repetition)
    data.check(benchmarks)
    data.add_performance_info(benchmarks)
    scheme.add_scheme_info(benchmarks)
    graph.add_graph_info(benchmarks)

    if output_filename:
        output_file = open(output_filename, 'w', encoding='utf-8')

    # Print header
    print('Lattice', 'Bound', 'Address', 'Scheme program',
          'Step filter?', 'Machine', 'p',
          'β',
          '# initial chain',
          '# states',
          '# error states', '# error values',
          '# final states', '# final values',
          'Final values',
          '# words', '# !',

          '#',
          '# states /s',
          'Ideal time',

          ('Time' if max_nb_result == 1 else 'Average time'), 'Time error',
          'Time min', 'Time max',

          'Ideal abstract speedup',
          'Chain abstract speedup',
          'Abstract speedup ideal weighted',
          'Abstract speedup chain weighted',
          'Average speedup', 'Speedup error',
          'Speedup min', 'Speedup max',

          'Ideal abstract efficiency',
          'Chain abstract efficiency',
          'Average efficiency', 'Efficiency error',
          'Efficiency min', 'Efficiency max',

          'Minimal abstract overhead',
          'Chain abstract overhead',
          'Average overhead', 'Overhead error',
          'Overhead min', 'Overhead max',
          sep='\t',
          file=output_file)

    print_benchmarks(benchmarks,
                     scheme_program=scheme_program, step_filter=step_filter,
                     machine=machine, filter_time_min=filter_time_min,
                     output_file=output_file)

    if output_filename:
        output_file.close()

if __name__ == '__main__':
    main()
