# -*- coding: utf-8 -*-

"""
graph.py (August 17, 2019)
"""

import os
import re
import sys
import zipfile

from typing import Dict, List, Optional, Set, Tuple

import abstract_parallel
import data


MEMO_STATS_GRAPH = dict()


#############
# Functions #
#############

def add_graph_info(benchmarks: data.Benchmarks) -> None:
    """
    Add some "abstract" metrics.
    """
    for key, tmp in benchmarks.items():
        for key_config, values in tmp.items():
            nb_state, nb_initial_chain = stats_graph(automatic_name(key, key_config))

            assert (nb_state is None) or (nb_state == values['nb_state']), \
                (nb_state, values['nb_state'])

            if nb_initial_chain is None:
                values['beta'] = ''
                values['nb_initial_chain'] = ''

                values['chain_abstract_speedup'] = ''
                values['chain_abstract_efficiency'] = ''
                values['chain_abstract_overhead'] = ''

                values['chain_abstract_speedup_weighted'] = ''
                values['chain_abstract_efficiency_weighted'] = ''
                values['chain_abstract_overhead_weighted'] = ''

                values['ideal_abstract_speedup_weighted'] = ''
                values['ideal_abstract_efficiency_weighted'] = ''
                values['minimal_abstract_overhead_weighted'] = ''
            else:
                t_seq = tmp[data.key_config_reference(key_config)]['time_average']

                values['beta'] = nb_initial_chain / values['nb_state']
                values['nb_initial_chain'] = nb_initial_chain

                values['chain_abstract_speedup'] = abstract_parallel.abstract_speedup(
                    values['nb_state'], values['nb_initial_chain'], key_config.nb_process)
                values['chain_abstract_efficiency'] = abstract_parallel.abstract_efficiency(
                    values['nb_state'], values['nb_initial_chain'], key_config.nb_process)
                values['chain_abstract_overhead'] = abstract_parallel.abstract_overhead(
                    values['nb_state'], values['nb_initial_chain'], key_config.nb_process, t_seq)

                values['chain_abstract_speedup_weighted'] = (
                    values['speedup_average'] * key_config.nb_process /
                    values['chain_abstract_speedup'])

                values['ideal_abstract_speedup_weighted'] = (
                    values['speedup_average'] * key_config.nb_process /
                    values['ideal_abstract_speedup'])


def automatic_name(key, key_config) -> str:
    """
    Return the filename like automaticName() function in Configuration.scala
    but always with the data.MACHINE_REF machine.
    """
    return ('{}__{}__bound_{}__{}__{}__{}.dot'
            .format(key.scheme_program.split('.')[0].replace('/', '_'),
                    key.lattice,
                    key.bound,
                    key.address,
                    ('step-filter' if key_config.step_filter
                     else 'step'),
                    data.MACHINE_REF))


def parse_dot(filename: str) -> Tuple[Set[int], Dict[int, List[int]]]:
    """
    Parse a DOT file
    and returns (set of nodes, dictionary of edges).

    read_dot() function of Networkx failed on big files!
    "ValueError: agread: bad input data"
    from "pygraphviz/agraph.py line 1195, in read"
    """
    re_edge = re.compile('\s*(\d+)\s*->\s*(\d+).*;')
    nodes = set()
    edges = dict()

    is_zip = filename[-4:] == '.zip'
    if is_zip:
        zip_file = zipfile.ZipFile(filename)
        zip_filenames = zip_file.namelist()

        assert len(zip_filenames) == 1, zip_filenames

        fin = zip_file.open(zip_filenames[0])
    else:
        fin = open(filename)

    for line in fin:
        if is_zip:
            line = str(line, encoding='latin-1')

        match = re_edge.match(line)
        if match:
            a = int(match.group(1))
            b = int(match.group(2))

            nodes.add(a)
            nodes.add(b)

            if a not in edges:
                edges[a] = []
            edges[a].append(b)

    if not is_zip:
        fin.close()

    return (nodes, edges)


def stats_graph(filename: str) -> Tuple[Optional[int], Optional[int]]:
    """
    Returns the total number of states
    and the number of states in the initial chain.
    """
    result = MEMO_STATS_GRAPH.get(filename, None)
    if result is not None:
        return result

    complete_filename = None
    for path in ('../../../scala-par-am/output-graph/dot/',
                 '../../Hydra__Intel_Xeon_Gold_6148_CPU_2_40GHz/Scala_Par_AM_00_09_15/dot/'):
        if os.path.isfile(path + filename):
            complete_filename = path + filename

            break
        elif os.path.isfile(path + filename + '.zip'):
            complete_filename = path + filename + '.zip'

            break

    if complete_filename is not None:
        graph_nodes, graph_edges = parse_dot(complete_filename)
        visited = set((None, ))
        state = 0
        nb_initial_chain = 1
        while state not in visited:
            visited.add(state)
            neighbors = graph_edges.get(state, [])

            if len(neighbors) == 1:
                state = neighbors[0]
                sys.stderr.flush()
                if state not in visited:  # to avoid infinite "chain"
                    nb_initial_chain += 1
            else:
                state = None

        result = (len(graph_nodes), nb_initial_chain)
    else:
        print('! "{}" NOT found!'.format(filename), file=sys.stderr)
        result = (None, None)

    MEMO_STATS_GRAPH[filename] = result

    return result
