#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
write_plots.py (August 14, 2019)
"""

import sys

from typing import Tuple

import matplotlib.pyplot as plt

import data
import scheme


#############
# Constants #
#############
COLORS = {'oldAAM': '',

          'SeqAAM': '',
          'SeqAAMS': '',
          'SeqAAMLS': '#ff8000',
          'SeqAAMLS-halt': '',

          'ParAAM-L-SA-state': '#a00000',
          'ParAAM-L-SA-set': '#00a000',
          'ParAAM-L-SA-part': '#0000a0',

          'ParAAM-C-S-state': '#c00000',
          'ParAAM-C-S-set': '#00c000',
          'ParAAM-C-S-part': '#0000c0',

          'ParAAM-C-state': '#ff0000',
          'ParAAM-C-set': '#00ff00',
          'ParAAM-C-part': '#0000ff',
          'ParAAM-C-hybrid': '#00ffff',

          'ParAAM-C-set-halt': '#80ff80',
          'ParAAM-C-part-halt': '#8080ff',
          'ParAAM-C-hybrid-halt': '#80ffff'}

DOTTED = (0, (2, 20))
DASHED = (0, (20, 40))
PLAIN = '-'

STYLES = {'oldAAM': '',

          'SeqAAM': '',
          'SeqAAMS': '',
          'SeqAAMLS': '',
          'SeqAAMLS-halt': '',

          'ParAAM-L-SA-state': DOTTED,
          'ParAAM-L-SA-set': DOTTED,
          'ParAAM-L-SA-part': DOTTED,

          'ParAAM-C-S-state': DASHED,
          'ParAAM-C-S-set': DASHED,
          'ParAAM-C-S-part': DASHED,

          'ParAAM-C-state': PLAIN,
          'ParAAM-C-set': PLAIN,
          'ParAAM-C-part': PLAIN,
          'ParAAM-C-hybrid': PLAIN,

          'ParAAM-C-set-halt': PLAIN,
          'ParAAM-C-part-halt': PLAIN,
          'ParAAM-C-hybrid-halt': PLAIN}


#############
# Functions #
#############
def plot_with_error(xs: Tuple, ys: Tuple, errors: Tuple,
                    color: str, linewidth, linestyle):
    plt.errorbar(xs, ys,
                 yerr=errors,
                 linewidth=linewidth, elinewidth=0.1,
                 marker='.', markersize=(10 if len(xs) == 1 else 5),
                 color=color, ecolor=color,
                 linestyle=linestyle)


def plot_with_min_max(xs: Tuple, ys: Tuple, mins: Tuple, maxs: Tuple,
                      color: str, linewidth, linestyle):
    plt.errorbar(xs, ys,
                 yerr=(tuple(ys[i] - m for i, m in enumerate(mins)),
                       tuple(m - ys[i] for i, m in enumerate(maxs))),
                 linewidth=linewidth, elinewidth=0.1,
                 marker='.', markersize=5,
                 color=color, ecolor=color,
                 linestyle=linestyle)


def write_plot_times(benchmarks: data.Benchmarks, filename: str,
                     scheme_program: str,
                     processes: Tuple[int], machines: Tuple[str],
                     data_name: str, min_max: bool):
    if data_name == 'time':
        key = data.Key(lattice='TypeSet', bound=100, address='Classical',
                       scheme_program=scheme_program)
        key_config = data.KeyConfig(step_filter=False, machine=data.MACHINE_REF,
                                    nb_process=1)

        t_seq = benchmarks[key][key_config]['time_average']
        ideals = tuple(t_seq/p for p in processes)

        plt.plot(processes, ideals, linestyle=(0, (1, 5)), linewidth=0.5, color="#a0a0a0")
    elif data_name == 'speedup':
        plt.plot(processes, processes, linestyle=(0, (1, 5)), linewidth=0.5, color="#a0a0a0")

    y_max = 0
    for machine in machines:
        ps, ys, errors, mins, maxs = data.extract_data(
            benchmarks,
            lattice='TypeSet', bound=100, address='Classical',
            scheme_program=scheme_program,
            step_filter=False, machine=machine, processes=processes,
            data_name=data_name)
        if ps and COLORS[machine]:
            y_max = max(y_max, max(ys))
            color = COLORS[machine]
            linewidth = (1 if machine.endswith('-halt') else 0.1)
            linestyle = STYLES[machine]
            if min_max:
                plot_with_min_max(ps, ys, mins, maxs,
                                  color=color, linewidth=linewidth, linestyle=linestyle)
            else:
                plot_with_error(ps, ys, errors,
                                color=color, linewidth=linewidth, linestyle=linestyle)

    plt.xlim(0, max(processes) + 0.5)
    plt.ylim(bottom=0)
    plt.ylim(0, y_max)
    # plt.legend(('ideal', ) + machines)
    plt.savefig(filename, format=filename.split('.')[-1])
    plt.close()


########
# Main #
########
def main():
    filename = '-'
    output = None
    skip_repetition = 0

    scheme_program = ''
    step_filter = None
    machine = ''

    def get_param():
        if not params:
            print('Missing param!', file=sys.stderr)

            exit(1)

        return params[0], params[1:]

    params = sys.argv[1:]
    while params:
        param, params = get_param()
        if param == '--machine':  # ???
            machine, params = get_param()
        elif param == '-o':
            output, params = get_param()
        elif param == '--scheme':
            scheme_program, params = get_param()
        elif param == '--skip-repetition':
            param, params = get_param()
            skip_repetition = int(param)
        elif param == '--step-filter':
            param, params = get_param()
            step_filter = (param.upper() == 'TRUE')
        elif param.startswith('-'):
            print('Error param "{}"!'.format(param), file=sys.stderr)

            exit(1)
        else:
            filename = param

    assert data.MACHINE_REF in data.MACHINES, data.MACHINE_REF

    benchmarks, max_nb_result = data.read_data(filename, skip_repetition=skip_repetition)
    data.check(benchmarks)
    data.add_performance_info(benchmarks)

    # Print plots
    for data_name in ('time', 'speedup', 'efficiency', 'overhead'):
        write_plot_times(benchmarks,
                         output.replace('__time.eps',
                                        '__{}.eps'.format(data_name)),
                         scheme_program=scheme_program,
                         processes=tuple(range(1, 16 + 1)),
                         machines=data.MACHINES,
                         data_name=data_name, min_max=False)


if __name__ == '__main__':
    main()
