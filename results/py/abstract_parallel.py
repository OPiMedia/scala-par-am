# -*- coding: utf-8 -*-

"""
abstract_parallel.py

Simple function of speedup, efficiency and overhead
about ideal states graph of abstract interpretation.

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 17, 2019
"""


#############
# Functions #
#############

def abstract_efficiency(N: int, L: int, p: int) -> float:
    assert p >= 1, p

    return N / ((p - 1) * L + N)


def abstract_overhead(N: int, L: int, p: int, t_seq: float) -> float:
    assert 1 <= L <= N, (L, N)
    assert t_seq >= 0, t_seq

    return ((p - 1) * L / N) * t_seq


def abstract_speedup(N: int, L: int, p: int) -> float:
    assert 1 <= L <= N, (L, N)
    assert p >= 1, p

    return (p * N) / ((p - 1) * L + N)
