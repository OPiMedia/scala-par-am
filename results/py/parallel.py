# -*- coding: utf-8 -*-

"""
Simple function of speedup, efficiency and overhead
for parallel context.

:license: GPLv3 --- Copyright (C) 2018, 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: March 8, 2019
"""

from typing import Union


########
# Type #
########
Number = Union[int, float]


#############
# Functions #
#############

def efficiency(t_seq: Number, t_par: Number, p: int) -> float:
    assert p >= 1, p

    return speedup(t_seq, t_par) / p


def overhead(t_seq: Number, t_par: Number, p: int) -> Number:
    assert p >= 1, p

    return t_par * p - t_seq


def speedup(t_seq: Number, t_par: Number) -> float:
    return t_seq / t_par
