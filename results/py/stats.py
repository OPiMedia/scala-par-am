# -*- coding: utf-8 -*-

"""
Simple stats functions.

:license: GPLv3 --- Copyright (C) 2018, 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: March 20, 2019
"""

import math

from typing import Optional, Sequence, Union


########
# Type #
########
Number = Union[int, float]


#############
# Functions #
#############

def confidence(seq: Sequence[Number], alpha: Number=0.05,
               avg: Optional[Number]=None) -> Optional[float]:
    """
    Return the confidence interval of data in seq for alpha.

    If seq is empty or only contains one number
    then return None.

    If avg is None
    then use the arithmetic mean,
    else use avg.

    See https://en.wikipedia.org/wiki/Confidence_interval

    :param seq: sequence of (int or float)
    :param alpha: 0 <= (int or float) <= 1 (implemented only for alpha = 0.05)
    :param avg: None or int or float

    :return: float or None
    """
    assert 0.0 <= alpha <= 1.0, alpha

    n = len(seq)

    if n <= 1:
        return None

    if alpha != 0.05:
        raise NotImplementedError

    return (1.96 * deviation(seq, avg=avg) / math.sqrt(n) if seq
            else 0.0)


def deviation(seq: Sequence[Number],
              avg: Optional[Number]=None) -> Optional[float]:
    """
    Return the *corrected* sample standard deviation of data in seq.

    If seq is empty or only contains one number
    then return None

    If avg is None
    then use the arithmetic mean,
    else use avg.

    See http://mathworld.wolfram.com/StandardDeviation.html

    :param seq: sequence of (int or float)
    :param avg: None or int or float

    :return: float or None
    """
    n = len(seq)

    if n <= 1:
        return None

    if avg is None:
        avg = mean(seq)

    return math.sqrt(sum((x - avg)**2 for x in seq) / (n - 1))


def geometric_mean(seq: Sequence[Number]) -> Optional[float]:
    """
    Return the *geometric* mean of data in seq.

    If seq is empty
    then return None.

    See http://mathworld.wolfram.com/GeometricMean.html

    :param seq: sequence of (int or float)

    :return: float or None
    """
    if not seq:
        return None

    prod = 1.0
    for x in seq:
        prod *= x

    return math.pow(prod, 1.0 / len(seq))


def mean(seq: Sequence[Number]) -> Optional[float]:
    """
    Return the *arithmetic* mean (average) of data in seq.

    If seq is empty
    then return None.

    See http://mathworld.wolfram.com/ArithmeticMean.html

    :param seq: sequence of (int or float)

    :return: float or None
    """
    if not seq:
        return None
    elif len(seq) == 1:
        return seq[0]

    return sum(seq) / len(seq)


def population_confidence(seq: Sequence[Number], alpha: Number=0.05,
                          avg: Optional[Number]=None) -> Optional[float]:
    """
    Return the confidence interval of data in seq for alpha
    that use the population standard deviation
    instead than the corrected sample standard deviation.

    If seq is empty
    then return None.

    If avg is None
    then use the arithmetic mean,
    else use avg.

    See https://en.wikipedia.org/wiki/Confidence_interval

    :param seq: sequence of (int or float)
    :param alpha: 0 <= (int or float) <= 1 (implemented only for alpha = 0.05)
    :param avg: None or int or float

    :return: float or None
    """
    assert 0.0 <= alpha <= 1.0, alpha

    if not seq:
        return None

    if alpha != 0.05:
        raise NotImplementedError

    return (1.96 * population_deviation(seq, avg=avg) / math.sqrt(len(seq))
            if seq
            else 0.0)


def population_deviation(seq: Sequence[Number],
                         avg: Optional[Number]=None) -> Optional[float]:
    """
    Return the population standard deviation of data in seq.

    Better than corrected sample standard deviation
    if less than 30 numbers in seq.

    If seq is empty
    then return None.

    If avg is None
    then use the arithmetic mean,
    else use avg.

    See https://en.wikipedia.org/wiki/Standard_deviation

    :param seq: sequence of (int or float)
    :param avg: None or float

    :return: float
    """
    if not seq:
        return None

    if avg is None:
        avg = mean(seq)

    return math.sqrt(sum((x - avg)**2 for x in seq) / len(seq))


def standard_error(seq: Sequence[Number],
                   avg: Optional[Number]=None) -> Optional[float]:
    """
    Return the standard error of data in seq.

    If seq is empty
    then return None.

    If avg is None
    then use the arithmetic mean,
    else use avg.

    :param seq: sequence of (int or float)
    :param avg: None or int or float

    :return: float
    """
    if avg is None:
        avg = mean(seq)
    elif len(seq) == 1:
        return 0

    return sum(abs(x - avg) for x in seq) / len(seq)


########
# Main #
########
def main():
    """
    Simple test with values obtained here :

    https://www.mathsisfun.com/data/confidence-interval-calculator.html

    https://www.omnicalculator.com/statistics/confidence-interval

    http://www.wolframalpha.com/widget/widgetPopup.jsp?p=v&id=974e2945a18e0bfb8e3aa8becac3e65c&title=Confidence+Interval+Calculator&theme=blue&i0=0.99&i1=12&i2=260.1&i3=4263.1&podSelect&includepodid=Xx%25ConfidenceInterval&showAssumptions=1&showWarnings=1
    """
    assert mean([]) is None
    assert mean([3]) == 3.0
    assert mean([0, 0, 0]) == 0.0
    assert round(mean([10, 10, 10]), 10) == 10.0

    #
    assert geometric_mean([]) is None
    assert geometric_mean([3]) == 3.0
    assert geometric_mean([1, 1, 1]) == 1.0
    assert round(geometric_mean([10, 10, 10]), 10) == 10.0

    #
    data = tuple()

    assert len(data) == 0
    assert deviation(data) is None
    assert confidence(data) is None
    assert population_deviation(data) is None

    data = (666, )

    assert len(data) == 1
    assert deviation(data) is None
    assert confidence(data) is None
    assert population_deviation(data) == 0.0

    data = (666, )*42

    assert len(data) == 42
    assert deviation(data) == 0.0
    assert confidence(data) == 0.0
    assert population_deviation(data) == 0.0

    data = (90, 86, 88, 91, 94, 92, 90, 84, 87)
    avg = mean(data)
    dev = deviation(data)
    conf = confidence(data)
    pop_dev = population_deviation(data)
    pop_conf = population_confidence(data)

    if __debug__:
        print(data)
        print(avg)
        print(dev, pop_dev)
        print(conf, pop_conf)
        print((avg - conf, avg + conf), (avg - pop_conf, avg + pop_conf))

    assert min(data) == 84
    assert max(data) == 94

    assert len(data) == 9
    assert round(avg, 1) == 89.1

    assert dev == deviation(data, avg)
    assert round(dev, 2) == 3.14

    assert round(conf, 4) == round(2.0515858, 4)
    assert conf == confidence(data, avg=avg)

    assert round(avg - conf, 4) == round(87.0595242, 4)
    assert round(avg + conf, 4) == round(91.1626958, 4)

    data = (90, 86, 88, 91, 94, 92, 90, 84)
    avg = mean(data)
    dev = deviation(data)
    conf = confidence(data)
    pop_conf = population_confidence(data)

    if __debug__:
        print()
        print(data)
        print(avg)
        print(dev, pop_dev)
        print(conf, pop_conf)
        print((avg - conf, avg + conf), (avg - pop_conf, avg + pop_conf))

    assert min(data) == 84
    assert max(data) == 94

    assert len(data) == 8
    assert round(avg, 1) == 89.4

    assert dev == deviation(data, avg)
    assert round(dev, 2) == 3.25

    assert round(conf, 2) == 2.25
    assert conf == confidence(data, avg=avg)

    assert round(avg - conf, 2) == 87.12
    assert round(avg + conf, 2) == 91.63

if __name__ == '__main__':
    main()
