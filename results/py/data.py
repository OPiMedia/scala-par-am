# -*- coding: utf-8 -*-

"""
data.py (August 17, 2019)
"""

import sys

from typing import Dict, NamedTuple, Optional, Tuple, Union

import abstract_parallel
import parallel
import stats


#############
# Constants #
#############
MACHINE_REF = 'SeqAAMLS'

MACHINES = (
    'oldAAM',
    'SeqAAM', 'SeqAAMS', 'SeqAAMLS', 'SeqAAMLS-halt',
    'ParAAM-L-SA-state', 'ParAAM-L-SA-set', 'ParAAM-L-SA-part',
    'ParAAM-C-S-state', 'ParAAM-C-S-set', 'ParAAM-C-S-part',
    'ParAAM-C-state', 'ParAAM-C-set', 'ParAAM-C-part', 'ParAAM-C-hybrid',
    'ParAAM-C-set-halt', 'ParAAM-C-part-halt', 'ParAAM-C-hybrid-halt')


###########
# Classes #
###########
Key = NamedTuple('Key', (('lattice', str),
                         ('bound', int),
                         ('address', str),
                         ('scheme_program', str)))

KeyConfig = NamedTuple('KeyConfig', (('step_filter', bool),
                                     ('machine', str),
                                     ('nb_process', int)))


########
# Type #
########
Benchmarks = Dict[Key, Dict[KeyConfig, Dict[str, Union[str, int, float]]]]


#############
# Functions #
#############

def add_performance_info(benchmarks: Benchmarks) -> None:
    """
    Add 'time_*', 'speedup_*', 'efficiency_*', 'overhead_*'
    and 'nb_state/s' information.
    """
    def error_fct(seq: Tuple[float],
                  avg: Optional[float]=None) -> Optional[float]:
        return (stats.standard_error(seq, avg) if len(seq) >= 30
                else stats.population_deviation(seq, avg))

    for tmp in benchmarks.values():
        for key_config, values in tmp.items():
            times = values['times']
            values['time_average'] = stats.mean(times)
            values['time_error'] = stats.population_deviation(times)

    for tmp in benchmarks.values():
        for key_config, values in tmp.items():
            t_seq = tmp[key_config_reference(key_config)]['time_average']
            t_par = tmp[key_config]['time_average']

            times = values['times']
            values['time_min'] = min(times)
            values['time_max'] = max(times)

            values['ideal'] = t_seq / key_config.nb_process

            speedups = tuple(parallel.speedup(t_seq, time)
                             for time in times)
            values['speedup_average'] = stats.geometric_mean(speedups)
            values['speedup_error'] = error_fct(speedups)
            values['speedup_min'] = min(speedups)
            values['speedup_max'] = max(speedups)

            efficiencies = tuple(parallel.efficiency(t_seq, t_par,
                                                     key_config.nb_process)
                                 for time in times)
            values['efficiency_average'] = stats.geometric_mean(efficiencies)
            values['efficiency_error'] = error_fct(efficiencies)
            values['efficiency_min'] = min(efficiencies)
            values['efficiency_max'] = max(efficiencies)

            overheads = tuple(parallel.overhead(t_seq, t_par,
                                                key_config.nb_process)
                              for time in times)
            values['overhead_average'] = stats.mean(overheads)
            values['overhead_error'] = error_fct(overheads)
            values['overhead_min'] = min(overheads)
            values['overhead_max'] = max(overheads)

            values['nb_state/s'] = values['nb_state']/t_par

            values['ideal_abstract_speedup'] = abstract_parallel.abstract_speedup(
                values['nb_state'], 1, key_config.nb_process)
            values['ideal_abstract_efficiency'] = abstract_parallel.abstract_efficiency(
                values['nb_state'], 1, key_config.nb_process)
            values['minimal_abstract_overhead'] = abstract_parallel.abstract_overhead(
                values['nb_state'], 1, key_config.nb_process, t_seq)


def check(benchmarks: Benchmarks) -> None:
    for tmp in benchmarks.values():
        for key_config, values in tmp.items():
            aam = tmp[key_config_reference(key_config)]

            for key_value in ('nb_final_value', 'finals', 'nb_state'):
                assert values[key_value] == aam[key_value], \
                    (key_value, values[key_value], aam[key_value])


def extract_data(benchmarks: Benchmarks,
                 lattice: str, bound: int, address: str, scheme_program: str,
                 step_filter: bool, machine: str, processes: Tuple[int],
                 data_name: str) -> Tuple[Tuple, Tuple, Tuple, Tuple, Tuple]:
    data = dict()

    for key, tmp in benchmarks.items():
        if ((key.lattice == lattice) and
                (key.bound == bound) and
                (key.address == address) and
                (key.scheme_program == scheme_program)):
            for key_config, values in tmp.items():
                if ((key_config.step_filter == step_filter) and
                        (key_config.machine == machine) and
                        (key_config.nb_process in processes)):
                    assert key_config.nb_process not in data

                    data[key_config.nb_process] = (
                        values[data_name + '_average'],
                        values[data_name + '_error'],
                        values[data_name + '_min'],
                        values[data_name + '_max'])

    if not data:
        return (tuple(), tuple(), tuple(), tuple(), tuple())

    ps, tuples = tuple(zip(*sorted(data.items())))
    indices = tuple(range(len(ps)))

    return (ps,
            tuple(tuples[i][0] for i in indices),
            tuple(tuples[i][1] for i in indices),
            tuple(tuples[i][2] for i in indices),
            tuple(tuples[i][3] for i in indices))


def get_bool(string: str) -> bool:
    assert string in ('false', 'true'), string

    return string == 'true'


def key_config_reference(key_config: KeyConfig) -> KeyConfig:
    return KeyConfig(step_filter=False, machine=MACHINE_REF, nb_process=1)


def read_data(filename: str, skip_repetition: int=0) -> Tuple[Benchmarks, int]:
    """
    Reads the TSV file, parses data,
    returns a Benchmarks dictionary
    and the maximum number of results.
    """
    fin = (sys.stdin if filename == '-'
           else open(filename))

    for line in fin:  # skip useless first lines and header
        if line.startswith('Lattice'):
            assert tuple(line.split('\t')[:15]) == (
                'Lattice', 'Bound', 'Address', 'Scheme program',
                'Step filter?', 'Machine', 'p',
                '# states',
                '# error states', '# error values',
                '# final states', '# final values', 'Final values',
                '#', '?'), (line.split('\t')[:15], line)

            break

    benchmarks = dict()
    max_nb_result = 0

    for line in fin:
        pieces = line[:-1].split('\t')
        # print(pieces, file=sys.stderr)

        assert len(pieces) >= 15, (pieces, line)

        # Get first information
        (lattice, bound_str, address, scheme_program,
         step_filter_str, machine, nb_process_str,
         nb_state_str,
         nb_error_state_str, nb_error_value_str,
         nb_final_state_str, nb_final_value_str, finals,
         nb_result_str) = pieces[:14]

        pieces = pieces[14:]

        if nb_state_str == '':
            print(line, file=sys.stderr)

            continue

        bound = int(bound_str)
        step_filter = get_bool(step_filter_str)
        nb_process = int(nb_process_str)
        nb_state = int(nb_state_str)
        nb_error_state = int(nb_error_state_str)
        nb_error_value = int(nb_error_value_str)
        nb_final_state = int(nb_final_state_str)
        nb_final_value = int(nb_final_value_str)
        nb_result = int(nb_result_str)

        assert lattice == 'TypeSet', lattice
        assert bound > 0, bound
        assert address == 'Classical', address
        assert scheme_program.endswith('.scm'), scheme_program
        assert step_filter_str in ('false', 'true'), step_filter_str
        assert machine in MACHINES, machine
        assert nb_process > 0, nb_process
        assert nb_state > 0, nb_state
        assert nb_error_state >= nb_error_value, (nb_error_state,
                                                  nb_error_value)
        assert nb_error_value >= 0, nb_error_value
        assert nb_final_state >= nb_final_value, (nb_final_state,
                                                  nb_final_value)
        assert nb_final_value >= 0, nb_final_value
        if nb_final_value == 0:
            assert finals == '', finals
        else:
            assert nb_final_value <= len(finals.split(', ')), (nb_final_value,
                                                               finals)
        assert nb_result > skip_repetition, (nb_result, skip_repetition)

        # Get times
        times = tuple(float(t)
                      for t in pieces[1:1 + nb_result * 2:2])[skip_repetition:]

        if pieces[0] == 'false':  # skip if computation not finished
            print(line, file=sys.stderr)

            continue

        assert skip_repetition + len(times) == nb_result, \
            (nb_result, skip_repetition, times)
        assert pieces[:nb_result * 2:2] == ['true'] * nb_result, pieces

        pieces = pieces[nb_result * 2:]

        if nb_result > 1:
            assert int(pieces[0]) == nb_result, (pieces[0], nb_result)
            assert pieces[3] == 'true', pieces[3]

        nb_result -= skip_repetition

        # All results are correct?
        if skip_repetition + nb_result > 1:
            assert pieces[0] == str(skip_repetition + nb_result), \
                (pieces[0], skip_repetition, nb_result)
            assert pieces[3] == 'true', pieces[3]
            assert pieces[4:] == [''] * (skip_repetition + nb_result), \
                pieces[4:]

        # Add to dictionary
        key = Key(lattice=lattice, bound=bound, address=address, scheme_program=scheme_program)
        key_config = KeyConfig(step_filter=step_filter, machine=machine,
                               nb_process=nb_process)

        if key not in benchmarks:
            benchmarks[key] = dict()

        assert key_config not in benchmarks[key], (key, key_config)

        benchmarks[key][key_config] = {'nb_state': nb_state,
                                       'nb_error_state': nb_error_state,
                                       'nb_error_value': nb_error_value,
                                       'nb_final_state': nb_final_state,
                                       'nb_final_value': nb_final_value,
                                       'finals': finals,
                                       'nb_result': nb_result,
                                       'times': times}
        max_nb_result = max(max_nb_result, nb_result)

    if filename != '-':
        fin.close()

    assert max_nb_result > 0, max_nb_result

    return (benchmarks, max_nb_result)
