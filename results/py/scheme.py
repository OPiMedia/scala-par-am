# -*- coding: utf-8 -*-

"""
scheme.py (August 14, 2019)
"""

import re

from typing import Tuple

import data


#############
# Functions #
#############

def add_scheme_info(benchmarks: data.Benchmarks) -> None:
    """
    Add 'nb_word' and 'nb_exclamation' information.
    """
    for key, tmp in benchmarks.items():
        nb_word, nb_exclamation = stats_scheme('../../../scala-par-am/' + key.scheme_program)
        for values in tmp.values():
            values['nb_word'] = nb_word
            values['nb_exclamation'] = nb_exclamation


def stats_scheme(filename: str) -> Tuple[int, int]:
    """
    Reads a Scheme program
    and returns the number of words and the number of '!' characters.
    (Comments and strings are ignored.)
    """
    # print(filename, file=sys.stderr)
    nb_word = 0
    nb_exclamation = 0
    with open(filename) as fin:
        for line in fin:
            line = line.strip()

            # Replace Scheme string by fake word
            line = re.sub('"+[^"]*"+', ' WORD ', line).strip()

            # Remove Scheme comment
            i = line.find(';')
            if i != 1:
                line = line[:i].rstrip()

            # Count word
            words = [None for word in re.split(r'[\s()]+', line) if word != '']
            nb_word += len(words)

            # Search !
            nb_exclamation += line.count('!')

            # Print to check line with !
            # if line.count('!') > 0:
            #     print('Scheme', filename, line, sep='\t', file=sys.stderr)

    return (nb_word, nb_exclamation)
