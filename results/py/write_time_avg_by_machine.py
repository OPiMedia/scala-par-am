#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
write_time_avg_by_machine.py (July 31, 2019)
"""

import sys

from typing import Dict, List


#############
# Functions #
#############

def read_data(filename: str) -> Dict[str, List[float]]:
    """
    Reads the TSV file, parses data,
    returns a Benchmarks dictionary
    and the maximum number of results.
    """
    data = dict()

    with open(filename) as fin:
        pieces = fin.readline()[:-1].split('\t')  # skip header
        assert pieces[3] == 'Scheme program', pieces[3]
        assert pieces[5] == 'Machine', pieces[5]
        assert pieces[6] == 'p', pieces[6]
        assert pieces[18] == 'Average time', pieces[18]
        assert pieces[19] == 'Time error', pieces[19]

        for line in fin:
            pieces = line[:-1].split('\t')
            # scheme_program = pieces[3]
            machine = pieces[5]
            nb_process = int(pieces[6])
            time = float(pieces[18])
            time_error = float(pieces[19])

            assert nb_process > 0, nb_process
            assert time >= 0, time

            assert nb_process == 1, nb_process

            if time >= 1.0:
                if machine not in data:
                    data[machine] = []

                data[machine].append(time)

    return data


def print_avg(data: Dict[str, List[float]]) -> None:
    for machine, times in sorted(data.items()):
        print(machine, len(times), sum(times)/len(times), sep='\t')


########
# Main #
########
def main():
    filename = sys.argv[1]
    data = read_data(filename)
    print_avg(data)


if __name__ == '__main__':
    main()
