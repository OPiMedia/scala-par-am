#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
extract_excepts.py (April 10, 2019)

From a file as testing__TypeSet__complete_list__false__SeqAAM.tsv
extract the list of Scheme examples that failed because exceed time out.
"""

import sys


########
# Main #
########
def main():
    """
    Read the file and print the list.
    """
    if len(sys.argv) < 2:
        exit(1)

    print('# List of Scheme examples that should be ignored because exceed timeout\n')

    already = set()

    with open(sys.argv[1]) as fin:
        for line in fin:  # skip useless first lines and header
            if line.startswith('Lattice'):
                assert tuple(line.split('\t')[:15]) == (
                    'Lattice', 'Bound', 'Address', 'Scheme program',
                    'Step filter?', 'Machine', 'p',
                    '# states',
                    '# error states', '# error values',
                    '# final states', '# final values', 'Final values',
                    '#', '?'), (line.split('\t')[:15], line)

                break

        for line in fin:
            pieces = line[:-1].split('\t')

            (_, _, _, scheme_program,
             _, _, _,
             _,
             _, _,
             _, _, _,
             _,
             succeeded_str, _,
             _, error) = pieces

            if error not in ('', 'TimedOut'):
                print(error, file=sys.stderr)

            already.add(scheme_program)

            if succeeded_str == 'false':
                print('{}  # {}'.format(scheme_program, error))

if __name__ == '__main__':
    main()
