#!/bin/bash -l

#PBS -l nodes=1:ppn=20
#PBS -l walltime=20:00:00
#PBS -l mem=20gb
#PBS -l file=50mb
#PBS -o log/benchmarks__TypeSet__AlgoDat1_qsort.log
#PBS -e log/benchmarks__TypeSet__AlgoDat1_qsort.err
#PBS -N benchmarks__TypeSet__AlgoDat1_qsort

# Without ParAAMCSState

echo "=== Config module ==="
export PATH=$HOME/graalvm-ee-19.1.1/bin:$PATH


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/scala-par-am/

LOGDIR=scripts/log/

./cpuinfos.sh > $LOGDIR/benchmarks__TypeSet__AlgoDat1_qsort__cpuinfos.log 2>&1

./scala-par-am.sh \
  --root Scheme-examples \
  -f Scheme-examples/AlgoDat1/qsort.scm \
  -l TypeSet \
  -m SeqAAMLS,ParAAMLSAState,ParAAMLSASet,ParAAMLSAPart,ParAAMCSSet,ParAAMCSPart,ParAAMCState,ParAAMCSet,ParAAMCPart \
  -p 1,2,3,4,5,6,7,8,9,10,12,14,16,18,20 \
  --prepare-file Scheme-examples/Sergey/gcfa2/sat.scm \
  --repetition 13 \
  -t 5minutes \
  > $LOGDIR/benchmarks__TypeSet__AlgoDat1_qsort.tsv 2>&1

echo "=== Done " `date` "==="
