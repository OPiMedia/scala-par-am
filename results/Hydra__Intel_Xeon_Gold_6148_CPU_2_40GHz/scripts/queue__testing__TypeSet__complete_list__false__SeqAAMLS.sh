#!/bin/bash -l

#PBS -l nodes=1:ppn=4
#PBS -l walltime=20:00:00
#PBS -l mem=5gb
#PBS -l file=50mb
#PBS -o log/testing__TypeSet__complete_list__false__SeqAAMLS.log
#PBS -e log/testing__TypeSet__complete_list__false__SeqAAMLS.err
#PBS -N testing__TypeSet__complete_list__false__SeqAAMLS

echo "=== Config module ==="
export PATH=$HOME/graalvm-ee-1.0.0-rc15/bin:$PATH


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/scala-par-am/

LOGDIR=scripts/log/

./cpuinfos.sh > $LOGDIR/testing__TypeSet__complete_list__false__SeqAAMLS__cpuinfos.log 2>&1

./scala-par-am.sh \
  --root Scheme-examples \
  -l TypeSet \
  -m SeqAAMLS \
  --prepare-file Scheme-examples/Sergey/gcfa2/sat.scm \
  -t 10minutes \
  > $LOGDIR/testing__TypeSet__complete_list__false__SeqAAMLS.tsv 2>&1

echo "=== Done " `date` "==="
