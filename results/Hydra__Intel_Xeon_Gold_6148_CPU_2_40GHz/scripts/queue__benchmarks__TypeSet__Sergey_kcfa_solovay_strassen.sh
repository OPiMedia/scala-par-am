#!/bin/bash -l

#PBS -l nodes=1:ppn=20
#PBS -l walltime=32:00:00
#PBS -l mem=20gb
#PBS -l file=50mb
#PBS -o log/benchmarks__TypeSet__Sergey_kcfa_solovay_strassen.log
#PBS -e log/benchmarks__TypeSet__Sergey_kcfa_solovay_strassen.err
#PBS -N benchmarks__TypeSet__Sergey_kcfa_solovay_strassen

# Without ParAAMCSState

echo "=== Config module ==="
export PATH=$HOME/graalvm-ee-19.1.1/bin:$PATH


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/scala-par-am/

LOGDIR=scripts/log/

./cpuinfos.sh > $LOGDIR/benchmarks__TypeSet__Sergey_kcfa_solovay_strassen__cpuinfos.log 2>&1

./scala-par-am.sh \
  --root Scheme-examples \
  -f Scheme-examples/Sergey/kcfa/solovay-strassen.scm \
  -l TypeSet \
  -m SeqAAMLS,ParAAMLSAState,ParAAMLSASet,ParAAMLSAPart,ParAAMCSSet,ParAAMCSPart,ParAAMCState,ParAAMCSet,ParAAMCPart \
  -p 1,2,3,4,5,6,7,8,9,10,12,14,16,18,20 \
  --prepare-file Scheme-examples/Sergey/gcfa2/sat.scm \
  --repetition 13 \
  -t 10minutes \
  > $LOGDIR/benchmarks__TypeSet__Sergey_kcfa_solovay_strassen.tsv 2>&1

echo "=== Done " `date` "==="
