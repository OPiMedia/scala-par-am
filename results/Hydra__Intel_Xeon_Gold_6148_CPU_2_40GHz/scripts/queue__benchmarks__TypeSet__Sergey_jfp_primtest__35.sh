#!/bin/bash -l

#PBS -l nodes=1:ppn=40
#PBS -l walltime=15:00:00
#PBS -l mem=40gb
#PBS -l file=50mb
#PBS -o log/benchmarks__TypeSet__Sergey_jfp_primtest__35.log
#PBS -e log/benchmarks__TypeSet__Sergey_jfp_primtest__35.err
#PBS -N benchmarks__TypeSet__Sergey_jfp_primtest__35

echo "=== Config module ==="
export PATH=$HOME/graalvm-ee-19.1.1/bin:$PATH


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/scala-par-am/

LOGDIR=scripts/log/

./cpuinfos.sh > $LOGDIR/benchmarks__TypeSet__Sergey_jfp_primtest__35__cpuinfos.log 2>&1

./scala-par-am.sh \
  --root Scheme-examples \
  -f Scheme-examples/Sergey/jfp/primtest.scm \
  -l TypeSet \
  -m SeqAAMLS,ParAAMLSAState,ParAAMLSASet,ParAAMLSAPart,ParAAMCSState,ParAAMCSSet,ParAAMCSPart,ParAAMCState,ParAAMCSet,ParAAMCPart \
  -p 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,24,26,28,30,32,34,36,38,40 \
  --prepare-file Scheme-examples/Sergey/gcfa2/sat.scm \
  --repetition 35 \
  -t 5minutes \
  > $LOGDIR/benchmarks__TypeSet__Sergey_jfp_primtest__35.tsv 2>&1

echo "=== Done " `date` "==="
