#!/bin/bash -l

#PBS -l nodes=1:ppn=4
#PBS -l walltime=6:00:00
#PBS -l mem=5gb
#PBS -l file=50mb
#PBS -o log/benchmarks__TypeSet__10_minutes_list__false__all_SeqAAM_.log
#PBS -e log/benchmarks__TypeSet__10_minutes_list__false__all_SeqAAM_.err
#PBS -N benchmarks__TypeSet__10_minutes_list__false__all_SeqAAM_

echo "=== Config module ==="
export PATH=$HOME/graalvm-ee-19.1.1/bin:$PATH


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/scala-par-am/

LOGDIR=scripts/log/

./cpuinfos.sh > $LOGDIR/benchmarks__TypeSet__10_minutes_list__false__all_SeqAAM___cpuinfos.log 2>&1

./scala-par-am.sh \
  --root Scheme-examples \
  --excepts-file Scheme-examples/excepts.txt \
  -l TypeSet \
  --all-SeqAAM_ \
  --prepare-file Scheme-examples/Sergey/gcfa2/sat.scm \
  --repetition 13 \
  -t 20minutes \
  > $LOGDIR/benchmarks__TypeSet__10_minutes_list__false__all_SeqAAM_.tsv 2>&1

echo "=== Done " `date` "==="
