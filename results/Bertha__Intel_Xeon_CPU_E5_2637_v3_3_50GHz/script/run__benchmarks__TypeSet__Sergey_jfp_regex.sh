#!/bin/sh

# TEST with a lot of memory (set by java64.sh)
# ~19h45 and stop because:
# Uncaught error from thread [Akka-System-ParAAMCPart-eval-scheduler-1]: GC overhead limit exceeded, shutting down JVM since 'akka.jvm-exit-on-fatal-error' is enabled for ActorSystem[Akka-System-ParAAMCPart-eval]
# java.lang.OutOfMemoryError: GC overhead limit exceeded
# Uncaught error from thread [Akka-System-ParAAMCPart-eval-scheduler-12

echo "=== Run " `date` "==="

./cpuinfos.sh > log/benchmarks__TypeSet__Sergey_jfp_regex__cpuinfos.log 2>&1

./java64.sh -jar scala-par-am.jar \
  --root Scheme-examples \
  -f Scheme-examples/Sergey/jfp/regex.scm \
  -l TypeSet \
  -m ParAAMCPart \
  -p 8 \
  --prepare-file Scheme-examples/Sergey/gcfa2/sat.scm \
  > log/benchmarks__TypeSet__Sergey_jfp_regex.tsv 2>&1

echo "=== Done " `date` "==="
