#!/bin/sh

# ~9h40

echo "=== Run " `date` "==="

./cpuinfos.sh > log/benchmarks__TypeSet__Sergey_jfp_primtest__35__cpuinfos.log 2>&1

./scala-par-am.sh \
  --root Scheme-examples \
  -f Scheme-examples/Sergey/jfp/primtest.scm \
  -l TypeSet \
  -m SeqAAMLS,ParAAMLSAState,ParAAMLSASet,ParAAMLSAPart,ParAAMCSState,ParAAMCSSet,ParAAMCSPart,ParAAMCState,ParAAMCSet,ParAAMCPart \
  -p 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,18,20,24,28,32 \
  --prepare-file Scheme-examples/Sergey/gcfa2/sat.scm \
  --repetition 35 \
  -t 2minute \
  > log/benchmarks__TypeSet__Sergey_jfp_primtest__35.tsv 2>&1

echo "=== Done " `date` "==="
