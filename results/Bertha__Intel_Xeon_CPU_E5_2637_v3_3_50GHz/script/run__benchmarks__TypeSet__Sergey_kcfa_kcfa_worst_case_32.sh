#!/bin/sh

# Without ParAAMCSState
# ~4h15

echo "=== Run " `date` "==="

./cpuinfos.sh > log/benchmarks__TypeSet__Sergey_kcfa_kcfa_worst_case_32__cpuinfos.log 2>&1

./scala-par-am.sh \
  --root Scheme-examples \
  -f Scheme-examples/Sergey/kcfa/kcfa-worst-case-32.scm \
  -l TypeSet \
  -m SeqAAMLS,ParAAMLSAState,ParAAMLSASet,ParAAMLSAPart,ParAAMCSSet,ParAAMCSPart,ParAAMCState,ParAAMCSet,ParAAMCPart \
  -p 1,2,3,4,5,6,7,8,9,10,12,14,16,24,32 \
  --prepare-file Scheme-examples/Sergey/gcfa2/sat.scm \
  --repetition 13 \
  -t 10minutes \
  > log/benchmarks__TypeSet__Sergey_kcfa_kcfa_worst_case_32.tsv 2>&1

echo "=== Done " `date` "==="
