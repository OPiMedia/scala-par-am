#!/bin/sh

# Without ParAAMCSState,ParAAMCSSet
# ~2h40

echo "=== Run " `date` "==="

./cpuinfos.sh > log/benchmarks__TypeSet__OPi_linear_5000_gen4fibonacci_recur__cpuinfos.log 2>&1

./scala-par-am.sh \
  --root Scheme-examples \
  -f Scheme-examples/OPi/linear_5000_gen4fibonacci_recur.scm \
  -l TypeSet \
  -m SeqAAMLS,ParAAMLSAState,ParAAMLSASet,ParAAMLSAPart,ParAAMCSPart,ParAAMCState,ParAAMCSet,ParAAMCPart \
  -p 1,2,3,4,5,6,7,8,9,10,12,14,16,24,32 \
  --prepare-file Scheme-examples/Sergey/gcfa2/sat.scm \
  --repetition 13 \
  -t 10minutes \
  > log/benchmarks__TypeSet__OPi_linear_5000_gen4fibonacci_recur.tsv 2>&1

echo "=== Done " `date` "==="
