# Build TSV, HTML and graphes --- August 15, 2019
.SUFFIXES:

BITBUCKET=https://bitbucket.org/OPiMedia/scala-par-am/src/default



SRC_TSVS = $(sort $(wildcard tsv/testing__*.tsv)) \
	$(sort $(wildcard tsv/benchmarks__*.tsv))

DEST_TSVS = $(sort $(patsubst tsv/%.tsv,tsv/resume__%.tsv,$(SRC_TSVS)))

HTMLS = $(sort $(patsubst tsv/%.tsv,html/%.html,$(SRC_TSVS))) \
	$(sort $(patsubst tsv/%.tsv,html/%.html,$(DEST_TSVS)))

DEST_EPSS = $(sort $(patsubst tsv/benchmarks__%.tsv,eps/benchmarks__%__time.eps,$(SRC_TSVS)))

SRC_EPSS = $(wildcard eps/*.eps)
DEST_PDFS = $(patsubst eps/%.eps,pdf/%.pdf,$(SRC_EPSS))



EPSTOPDF      = epstopdf
EPSTOPDFFLAGS =


EXTRACTEXCEPTS = python3 ../../py/extract_excepts.py


GNUPLOT      = gnuplot
GNUPLOTFLAGS =


WRITEPLOTS      = python3 ../../py/write_plots.py
WRITEPLOTSFLAGS =

WRITETSV      = python3 ../../py/write_tsv.py
WRITETSVFLAGS =


TSV2HTMLTABLE           = tsv2htmltable  # https://bitbucket.org/OPiMedia/tsv2htmltable
TSV2HTMLTABLEFLAGS      = --easy --src-skip-until-regex 'Lattice' --dest-html-css-inlined
TSV2HTMLTABLEFLAGSAFTER =


ECHO  = echo
RM    = rm
SHELL = sh



###
# #
###
all:

list:
	@echo --- src TSV ---
	@for FILE in $(SRC_TSVS) ; do echo $$FILE ; done
	@echo '\n--- dest TSV ---'
	@for FILE in $(DEST_TSVS) ; do echo $$FILE ; done
	@echo '\n--- HTML ---'
	@for FILE in $(HTMLS) ; do echo $$FILE ; done
	@echo '\n--- DEST_EPS ---'
	@for FILE in $(DEST_EPSS) ; do echo $$FILE ; done
	@echo '\n--- SRC_EPS ---'
	@for FILE in $(SRC_EPSS) ; do echo $$FILE ; done
	@echo '\n--- DEST_PDF ---'
	@for FILE in $(DEST_PDFS) ; do echo $$FILE ; done



########
# Rule #
########
.PRECIOUS:	eps/benchmarks__%__time.eps tsv/resume__%.tsv

eps/benchmarks__%__time.eps:	tsv/benchmarks__%.tsv
	@$(ECHO) ---------- PLOTS EPS ----------
	$(WRITEPLOTS) $(WRITEPLOTSFLAGS) $< -o $@

html/%.html:	tsv/%.tsv
	@$(ECHO) ========== HTML ==========
	$(TSV2HTMLTABLE) $(TSV2HTMLTABLEFLAGS) $< -o $@ \
		--column-replace-regex 3 '^(Scheme-examples/(.*/)(.+?)\.scm)$$' '<a href="$(BITBUCKET)/scala-par-am/\1">\2<strong>\3</strong></a>' \
		--dest-column-type 0 avoid --dest-column-type 1 avoid --dest-column-type 2 avoid \
		--dest-column-type 3 html \
		--dest-html-title '$(TITLE) &mdash; $(PLATFORM) &mdash; Scala-Par-AM' \
		--dest-html-caption '$(CAPTION) (time in seconds, computed on $(HTMLPLATFORM))' \
		--dest-html-colophon '    <footer class="colophon medskip">Data of <a href="https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala"><em>Efficient Parallel Abstract Interpreter in Scala</em></a> master thesis<br>computed with <a href="https://bitbucket.org/OPiMedia/scala-par-am">Scala-Par-AM</a><br><br>\tsv2htmltable</footer>' \
		--dest-html-source-data-url ../$< $(TSV2HTMLTABLEFLAGSAFTER)

pdf/%.pdf:	eps/%.eps
	@$(ECHO) ---------- PDF ----------
	$(EPSTOPDF) $(EPSTOPDFFLAGS) -o $@ $<

tsv/resume__%.tsv:	tsv/%.tsv
	@$(ECHO) ========== RESUME TSV ==========
	$(WRITETSV) $(WRITETSVFLAGS) --reference SeqAAMLS -o $@ $<
