#!/bin/sh

# Runs Scala-Par-AM,
# duplicates results on error output and converts them in HTML table to standard output

TSV2HTMLTABLE=tsv2htmltable  # https://bitbucket.org/OPiMedia/tsv2htmltable
TSV2HTMLTABLEFLAGS='--src-get-table-header --dest-add-column-indices --dest-decimal-precision 3 --dest-html-inlined-default'

./scala-par-am.sh "$@" | python3 -u ./dup.py | $TSV2HTMLTABLE $TSV2HTMLTABLEFLAGS -
