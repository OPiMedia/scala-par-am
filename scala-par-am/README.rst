.. -*- restructuredtext -*-

===========================
Scala-Par-AM / scala-par-am
===========================
**Parallel (Abstract) Abstract Machine Experiments using Scala**
(with Akka, to analyze Scheme programs)

This **parallel abstract interpreter** is a parallel version
(restricted to Scheme programs and the AAM implementation) of Scala-AM_.
It is done for the **master thesis**: `An efficient and parallel abstract interpreter in Scala`_.

* `src/main/scala/`_: **main sources** of this repository

  * `machine/`_: **implementations** of sequential and **parallel machines**

* `Scheme-examples/`_: **Scheme program examples** used to benchmarks parallel implementations

.. _`An efficient and parallel abstract interpreter in Scala`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala/
.. _`machine/`: https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/machine/
.. _Scala-AM: https://bitbucket.org/OPiMedia/scala-am
.. _`Scheme-examples/`: https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/Scheme-examples/
.. _`src/main/scala/`: https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/

See the `HTML online documentation`_ from the source code.

See the main directory of this repository Scala-Par-AM_
for other information.

.. _`HTML online documentation`: http://www.opimedia.be/DS/online-documentations/Scala-Par-AM/html/be/opimedia/scala_par_am/Main$.html
.. _Scala-Par-AM: https://bitbucket.org/OPiMedia/scala-par-am

And see the `README.md`_ file of Scala-AM_ to more information about the abstract interpreter itself.

.. _`README.md`: https://bitbucket.org/OPiMedia/Scala-AM/src/master/scala-am/README.md

|



Usage
=====
To compile all the main code:

.. code-block:: sh

  $ sbt compile

To build the JAR file (but you can download it `scala-par-am.jar`_):

.. code-block:: sh

  $ sbt assembly

.. _`scala-par-am.jar`: https://bitbucket.org/OPiMedia/scala-par-am/downloads/scala-par-am.jar

To compile and run tests:

.. code-block:: sh

  $ sbt test


With the JAR file built,
run with `--help` command line parameter to show all options:

.. code-block:: sh

  $ java -jar scala-par-am.jar --help

Or directly with this shell script that also set some JVM configurations:

.. code-block:: sh

  $ ./scala-par-am.sh --help

An example with use of several options:

.. code-block:: sh

  $ ./scala-par-am.sh --files "Scheme-examples/OPi/factorial*.scm" --machines SeqAAMLS,ParAAMLSAPart,ParAAMCPart --processes 1,4,2

That runs on files that match the "Scheme-examples/OPi/factorial*.scm" path
with machine SeqAAMLS on 1 process,
with machine ParAAMLSAPart on 4 and 2 processes,
and with machine ParAAMCPart with 1, 4 and 2 processes.
The combination of options runs several times
ignoring incompatible

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬 (from the biggest part from Scala-AM_ written by Quentin Stiévenart)
======================================================================================================================================

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png



|Scala-Par-AM|

`References for this picture`_

.. _`References for this picture`: https://bitbucket.org/OPiMedia/scala-par-am/src/master/_img/_src/_original/README.rst

.. |Scala-Par-AM| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/18/3258961855-5-scala-par-am-logo_avatar.png
