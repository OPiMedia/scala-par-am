#!/bin/sh

# Print some information about the hardware and the environment

date

echo '\n\n\n===== uname -a ====='
uname -a

echo '\n\n\n===== scala-par-am.sh --version ====='
./scala-par-am.sh --version

echo '\n\n\n===== cat /etc/os-release ====='
cat /etc/os-release

echo '\n\n\n===== java -XX:+PrintFlagsFinal -version [filtered] ====='
./java.sh -XX:+PrintFlagsFinal -version | grep -iE 'HeapSize|PermSize|ThreadStackSize'

echo '\n\n\n===== nproc ====='
nproc

echo '\n\n\n===== free -h ====='
free -h

echo '\n\n\n===== cat /proc/sys/kernel/threads-max ====='
cat /proc/sys/kernel/threads-max

echo '\n\n\n===== ulimit -a ====='
ulimit -a

echo '\n\n\n===== cat /proc/cpuinfo ====='
cat /proc/cpuinfo
