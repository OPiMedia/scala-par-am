(define (priority-first-traversal graph
                                  start-label
                                  make-queue-element
                                  priority-ft
                                  eq-queue-element
                                  access-label
                                  action)
  ;;; make-queue-element and priority-ft take 5 parameters
  ;;; from-label from-info to-label to-info . edgeinfo
  ;;; edge-info is not used for non weighted graph
  ;;; eq-queue-element takes two and access-label and action take one ;;; argument, i.e. an item(s) from the queue
  (define (processed? label)
    (eq?  (graph 'lookup-node-status label)
          'P))
  ;the body
  (if (graph 'empty?)
      #f
      (let ((queue (create-priority-queue)))
        ;the loop
        (define (iter)
          ;(display "iter")
          (cond
            (((queue 'empty?)) 'empty)
            (else (let* ((priority-item ((queue 'dequeue)))
                         ;(item (cdr priority-item)))
                         (item priority-item))
                    (graph 'change-node-status (access-label item) 'P)
                    (action item)
                    (graph 'foreach-neighbour (access-label item)
                           (lambda (from-label from-info
                                               to-label to-info edge-info)
                             (if (not (processed? to-label))
                                 (begin
                                   ;(display "nieuwe erin")
                                   (graph 'change-node-status
                                          to-label 'R)
                                   ((queue 'enqueue)
                                    (priority-ft
                                      from-label from-info
                                      to-label to-info edge-info)
                                    (make-queue-element
                                      from-label from-info
                                      to-label to-info edge-info))))))
                    (iter)))))
        ;start
        (graph 'foreach-node
               (lambda (label info)
                 (graph 'change-node-status label 'W)))

        ;eerste element in de queue
        ((queue 'enqueue) (priority-ft #f #f
                                       start-label
                                       (graph 'lookup-node-info start-label)
                                       #f)
         (make-queue-element #f #f
                             start-label
                             (graph 'lookup-node-info start-label)
                             #f))
        (iter))))
