.. -*- restructuredtext -*-

==========================
Scheme examples / AlgoDat1
==========================
All files in this and underlying directories have been taken (2019-02-21) from
ftp://cw.vub.ac.be/pub/courses/curriculum/AlgoDat1/programmacode/

Some of them have been modified to avoid Scala-(Par-)AM limitations (MODIFIED mark)
and/or to have something to run (ADDED mark).


These following examples are not included (because Scala-(Par-)AM limitations):
* bfirst.scm
* OefRBtreeADT.scm
