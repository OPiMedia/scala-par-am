(define (quick-sort a-list)
  (define (rearrange pivot some-list)
    (define (rearrange-iter rest result)
;      (display pivot)(display " :: ")(display rest)(display (car result))(display (cdr result))(newline)
      (if(null? rest)
	 result
	 (if( <= (car rest) pivot)
	    (rearrange-iter (cdr rest)
			    (cons (cons (car rest)
					(car result))
				  (cdr result)))
	    (rearrange-iter (cdr rest)
			    (cons (car result)
				  (cons (car rest)
					(cdr result)))))))
    (rearrange-iter some-list (cons '() '())))
  (if (<= (length a-list) 1)
      a-list
      (let* ((pivot (car a-list))
	     (sub-lists (rearrange pivot (cdr a-list))))
;	(display (car sub-lists))(display pivot)(display (cdr sub-lists))(newline)
	(append (quick-sort (car sub-lists))
		(list pivot)
		(quick-sort (cdr sub-lists))))))


;;; ADDED to run something
(equal? (quick-sort '(9 8 7 6 5 4 0 9)) '(0 4 5 6 7 8 9 9))
