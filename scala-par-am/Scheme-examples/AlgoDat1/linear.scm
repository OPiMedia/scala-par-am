;;; ADAPTED to avoid Scala-(Par-)AM limitations

;;; MODIFIED (define (create-hash-table size hash-fct . equal-fct)
(define (create-hash-table size hash-fct equal-fct)
  (let ((content (make-vector size))
        (same? (if (null? equal-fct) = (car equal-fct))))
    ;auxiliaries
    (define (next-index index)
      (remainder (+ index 1) size))
    ; an abstraction for the items in the hash table
    (define (make-item status key info)
      ; status = data, empty or deleted
      (list status key info))
    (define (get-status item) (car item))
    (define (set-status! item status) (set-car! item status))
    (define (get-key item) (cadr item))
    (define (set-key! item key) (set-car! (cdr item) key))
    (define (get-info item) (caddr item))
    (define (set-info! item info) (set-car! (cddr item) info))
    ; the operations
    (define (insert key info)
      (define (rehash-iter current)
        (let*  ((item (vector-ref content current))
                (status (get-status item)))
          (cond
            ((not (eq? status 'data))
             (set-status! item 'data)
             (set-key! item key)
             (set-info! item info))
            ((same? key (get-key item))
             (set-info! item info))
            (else (rehash-iter (next-index current))))))
      (rehash-iter (hash-fct key)))
    (define (find-item key)
      (define (rehash-iter current)
        (let* ((item (vector-ref content current))
               (status (get-status item)))
          (cond
            ((eq? status 'data)
             (if (same? key (get-key item))
                 item
                 (rehash-iter (next-index current))))
            ((eq? status 'empty) #f)
            (else (rehash-iter (next-index current))))))
      (rehash-iter (hash-fct key)))
    (define (retrieve key)
      (let ((temp (find-item key)))
        (if temp
            (get-info temp)
            #f)))
    (define (delete key)
      (let ((temp (find-item key)))
        (cond
          (temp
            (set-status! temp 'deleted)
            #t)
          (else #f))))
    (define (display-table) ;*
      (let ((stop (vector-length content)))
        (define (iter current)
          (cond
            ((< current stop)
             (display current)
             (display "  ")
             (display (vector-ref content current))
             (newline)
             (iter (+ current 1)))))
        (iter 0)))
    ;;; MODIFIED (define (dispatch msg . args) ;*
    (define (dispatch msg args) ;*
      (cond
        ((eq? msg 'insert) (insert (car args) (cadr args)))
        ((eq? msg 'delete) (delete (car args)))
        ((eq? msg 'retrieve) (retrieve (car args)))
        ((eq? msg 'display) (display-table))
        (else (error "unknown request -- create-hash-table" msg))))
    (do ;*
      ((index (- (vector-length content) 1) (- index 1)))
      ((negative? index) 'done)
      (vector-set! content index (make-item 'empty '() '())))
    dispatch))

;;; Following MODIFIED to run without optional arguments
(define table (create-hash-table 13 (lambda (key) (modulo key 13)) '()))
; Voorbeeld uit het boek.
(table 'insert (list 1 79))
(table 'insert (list 4 69))
(table 'insert (list 14 98))
(table 'insert (list 7 72))
(table 'insert (list 27 14))
(table 'insert (list 11 50))
(table 'display '())
