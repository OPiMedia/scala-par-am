;;; ADAPTED to avoid Scala-(Par-)AM limitations

(define (create-dictionary)
  (let ((content '()))
    (define (empty?)
      (null? content))
    (define (insert key info)
      (let ((temp (assoc key content)))
	(if temp
	    (set-cdr! temp info)
	    (set! content (cons (cons key info) content))))
      #t)
    (define (delete key)
      (define (remove-iter current prev)
	(cond
	 ((null? current) #f)
	 ((eq? key (caar current))
	  (if (null? prev)
	      (set! content (cdr content))
	      (set-cdr! prev (cdr current)))
	  #t)
	 (else (remove-iter (cdr current) current))))
      (remove-iter content '()))
    (define (lookup key)
      (let ((temp (assoc key content)))
	(if temp
	    (cdr temp)
	    #f)))
    (define (map a-function)
      ;a-function takes 2 arguments, i.e. key and info
      (define (map-iter the-current result)
	(if (null? the-current)
	    (reverse result)
	    (map-iter (cdr the-current)
		      (cons (a-function (caar the-current) (cdar the-current))
			    result))))
      (map-iter content '()))
    (define (foreach a-action)
      ;a-action takes 2 arguments, i.e. key and info
      (define (foreach-iter the-current)
	(cond
	 ((null? the-current) #t)
	 (else
	  (a-action (caar the-current) (cdar the-current))
	  (foreach-iter (cdr the-current)))))
      (foreach-iter content)
      #t)
    (define (display-dict)
      (foreach (lambda (key info)
		 (display key)
		 (display " ")
		 (display info)
		 (newline))))
    ;;; MODIFIED (define (dispatch msg . args) ;*
    (define (dispatch msg args) ;*
      (cond
       ((eq? msg 'empty?) (empty?))
       ((eq? msg 'insert) (insert (car args) (cadr args)))
       ((eq? msg 'delete) (delete (car args)))
       ((eq? msg 'lookup) (lookup (car args)))
       ((eq? msg 'map) (map (car args)))
       ((eq? msg 'foreach) (foreach (car args)))
       ((eq? msg 'display) (display-dict))
       (else (error "unknown request -- create-dictionary" msg))))
    dispatch))


;;; ADDED from dictExamples.scm to run something, and adapted to run without optional arguments
(define nl->fr (create-dictionary))
(nl->fr 'insert (list 'fiets '(bicyclette)))
(nl->fr 'insert (list 'auto '(voiture)))
(nl->fr 'insert (list 'huis '(maison)))
(nl->fr 'insert (list 'vrachtwagen '(camion)))
(nl->fr 'insert (list 'tientonner '(camion)))
(nl->fr 'lookup (list 'fiets))
(nl->fr 'display '())

(define fr->eng (create-dictionary))
(fr->eng 'insert (list 'bicyclette '(bike)))
(fr->eng 'insert (list 'voiture '(car)))
(fr->eng 'insert (list 'maison '(house home)))
(fr->eng 'insert (list 'camion '(truck)))
(fr->eng 'lookup (list 'bicyclette))
