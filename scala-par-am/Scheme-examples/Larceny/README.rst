.. -*- restructuredtext -*-

=========================
Scheme examples / Larceny
=========================
Scheme benchmarks of Larceny_ (2019-02-22)
http://www.larcenists.org/Twobit/benchmarksAbout.html

.. _Larceny: http://www.larcenists.org/

A lot of these benchmarks are also in the Gambit benchmarks
https://github.com/gambit/gambit/tree/master/bench/src


For all of these Scheme examples the main function have been commented (COMMENTED mark)
to avoid Scala-(Par-)AM limitations,
and some code have been added to run something (ADDED mark).

Access to external data (file bib) are also adapted to access to the sub-directory data/.



These following examples are not included (because use of non-standard primitives or Scala-(Par-)AM limitations):
* Kernighan and Van Wyk - sum1.scm
* Numerical - fft.scm
* Numerical - fibfp.scm
* Numerical - mbrot.scm
* Numerical - nucleic.scm
* Numerical - pnpoly.scm
* Numerical - sumfp.scm
* other - gcold.scm
* other - parsing.scm
* other - ray.scm
* other - scheme.scm
* other - simplex.scm
* other - slatex.scm
