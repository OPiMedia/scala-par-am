;;; ADAPTED to avoid Scala-(Par-)AM limitations

;;; SUM -- Compute sum of integers from 0 to 10000

(define (run n)
  (let loop ((i n) (sum 0))
    (if (< i 0)
      sum
      (loop (- i 1) (+ i sum)))))

;;; COMMENTED
;;; (define (main . args)
;;;   (run-benchmark
;;;     "sum"
;;;     sum-iters
;;;     (lambda (result) (equal? result 50005000))
;;;     (lambda (n) (lambda () (run n)))
;;;     10000))

;;; ADDED to run something
(run 10000) ; = 50005000
(run 1000)
(run 100000)
(run 1000000)
(run 2000000)
