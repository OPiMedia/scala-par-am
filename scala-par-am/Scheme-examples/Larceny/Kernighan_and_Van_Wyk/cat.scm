;;; ADAPTED to avoid Scala-(Par-)AM limitations

;;; CAT -- One of the Kernighan and Van Wyk benchmarks.

(define inport #f)
(define outport #f)

(define (catport port)
  (let ((x (read-char port)))
    (if (eof-object? x)
        (close-output-port outport)
        (begin
          (write-char x outport)
          (catport port)))))

(define (go)
  ;; MODIFIED (set! inport (open-input-file "../../src/bib"))
  (set! inport (open-input-file "Larceny/Kernighan_and_Van_Wyk/data/bib"))
  (set! outport (open-output-file "foo"))
  (catport inport)
  (close-input-port inport))

;;; COMMENTED
;;; (define (main . args)
;;;   (run-benchmark
;;;    "cat"
;;;    cat-iters
;;;    (lambda (result) #t)
;;;    (lambda () (lambda () (go)))))

;;; ADDED to run something
(go)
