.. -*- restructuredtext -*-

===============
Scheme examples
===============

A lot of little Scheme programs used to test and benchmark Scala-**Par**-AM.


* `All benchmarks`_ of Scala-**Par**-AM in fancy HTML tables and TSV format:
* HTML page of results with SeqAAMLS machine (of Scala-**Par**-AM) for `all Scheme examples`_:
* HTML page of results and **information** with SeqAAMLS machine (of Scala-**Par**-AM) for Scheme examples that `correctly finished`_ in less than 10 minutes:

.. _`All benchmarks`: http://www.opimedia.be/CV/2017-2018-ULB/MEMO-F524-Masters-thesis/benchmark-results/
.. _`all Scheme examples`: http://www.opimedia.be/CV/2017-2018-ULB/MEMO-F524-Masters-thesis/benchmark-results/Hydra__Intel_Xeon_Gold_6148_CPU_2_40GHz/Scala_Par_AM_01_00_02/html/testing__TypeSet__complete_list__false__SeqAAMLS.html
.. _`correctly finished`: http://www.opimedia.be/CV/2017-2018-ULB/MEMO-F524-Masters-thesis/benchmark-results/Hydra__Intel_Xeon_Gold_6148_CPU_2_40GHz/Scala_Par_AM_01_00_02/html/resume__testing__TypeSet__complete_list__false__SeqAAMLS.html

The Scheme interpreter Guile_ is used (by the the script ``check.sh``)
to check that all of these examples are syntactically correct.

.. _Guile: https://www.gnu.org/software/guile/guile.html
