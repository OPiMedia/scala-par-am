(define (tree n)
  (if (>= n 0)
      (if (>= n 1)
          (if (>= n 2)
              (if (>= n 3)
                  0
                  1)
              (if (>= n 3)
                  2
                  3))
          (if (>= n 2)
              (if (>= n 3)
                  4
                  5)
              (if (>= n 3)
                  6
                  7)))
      (if (>= n 1)
          (if (>= n 2)
              (if (>= n 3)
                  8
                  9)
              (if (>= n 3)
                  10
                  11))
          (if (>= n 2)
              (if (>= n 3)
                  12
                  13)
              (if (>= n 3)
                  14
                  15)))))

(tree 0)
