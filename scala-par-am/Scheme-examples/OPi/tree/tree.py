#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
tree.py (August 13, 2019)
"""

import sys


def tree(height, depth=0, leave=0, indent=6):
    assert height >= 0, height
    assert depth >= 0, depth

    if height == 0:
        return str(leave)
    else:
        return ("""(if (>= n {0})
{1}{2}
{1}{3})""".format(depth,
                  ' '*indent,
                  tree(height - 1, depth + 1, leave*2, indent + 4),
                  tree(height - 1, depth + 1, leave*2 + 1, indent + 4)))


def main():
    height = (int(sys.argv[1]) if len(sys.argv) > 1
              else 1)

    print("""(define (tree n)
  {})

(tree 0)""".format(tree(height)))

if __name__ == '__main__':
    main()
