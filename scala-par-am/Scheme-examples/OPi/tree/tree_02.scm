(define (tree n)
  (if (>= n 0)
      (if (>= n 1)
          0
          1)
      (if (>= n 1)
          2
          3)))

(tree 0)
