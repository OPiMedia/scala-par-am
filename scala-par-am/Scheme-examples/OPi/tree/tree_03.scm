(define (tree n)
  (if (>= n 0)
      (if (>= n 1)
          (if (>= n 2)
              0
              1)
          (if (>= n 2)
              2
              3))
      (if (>= n 1)
          (if (>= n 2)
              4
              5)
          (if (>= n 2)
              6
              7))))

(tree 0)
