;; nth number of a similar Fibonacci sequence (by recursive process)
(define (gen4fibonacci n)
  (if (<= n 3)
      n
      (+ (gen4fibonacci (- n 1))
         (gen4fibonacci (- n 2))
         (gen4fibonacci (- n 3))
         (gen4fibonacci (- n 4)))))

(gen4fibonacci 10)  ;; 316
