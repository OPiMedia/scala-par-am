;; nth number of a similar Fibonacci sequence (by recursive process)
(define (gen6fibonacci n)
  (if (<= n 5)
      n
      (+ (gen6fibonacci (- n 1))
         (gen6fibonacci (- n 2))
         (gen6fibonacci (- n 3))
         (gen6fibonacci (- n 4))
         (gen6fibonacci (- n 5))
         (gen6fibonacci (- n 6)))))

(gen6fibonacci 10)  ;; 229
