;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Ackermann                         ;;;
;;;   Affiche les termes consecutifs  ;;;
;;;   de la fonction d'Ackermann.     ;;;
;;; (c) Olivier Pirson --- DragonSoft ;;;
;;; Debute le 26 fevrier 2004         ;;;
;;; v.01.00.00 --- 14 avril 2004      ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define VERSION "v.01.00.00 --- 2004 April 14")
(define DRAGONSOFT "(c) Olivier Pirson --- DragonSoft")

;;; Fonction d'Ackermann : pour tout m, n naturels
;;;   A(0,   n)   := n+1
;;;   A(m+1, 0)   := A(m, 1)
;;;   A(m+1, n+1) := A(m, A(m+1, n))
;;;
;;; ==> A(1, n)   = n+2
;;;     A(2, n)   = 2n + 3
;;;     A(3, n)   = 2^(n+3) - 3
;;;     A(4, n)   = 2^2^...^2 - 3
;;;                 ---------
;;;                 n+3 fois
;;;     A(5, n+1) = 2^2^...^2 -3
;;;                 ---------
;;;               A(5, n)+3 fois

;;; Renvoie 2^2^...^2
;;;         ---------
;;;          nb fois
;;; PRE: nb : naturel
(define (super-power2 nb)
  (let loop ((nb nb) (r 1))
    (if (> nb 0)
        (loop (- nb 1) (expt 2 r))
        r)))

;;; Renvoie le naturel A(m, n)
;;; PRE: m, n : naturels
(define (Ackermann m n)
  (cond
   ((>= m 6)
    (if (> n 0)
        (Ackermann (- m 1) (Ackermann m (- n 1)))  ; A(m, n) = A(m-1, A(m, n-1))
        (Ackermann (- m 1) 1)))                    ; A(m, 0) = A(m-1, 1)
   ((= m 5)
    (if (> n 0)
        (- (super-power2 (+ (Ackermann 5 (- n 1)) 3)) 3)
                                        ; A(5, n) = 2^2^...^2 - 3 [A(5, n-1)+3 fois]
        (Ackermann 4 1)))                          ; A(5, 0) = A(4, 1)
   ((= m 4)
    (- (super-power2 (+ n 3)) 3))                  ; A(4, n) = 2^2^...^2 - 3 [n+3 fois]
   ((= m 3)
    (- (expt 2 (+ n 3)) 3))                        ; A(3, n) = 2^(n+3) - 3
   ((= m 2)
    (+ (* n 2) 3))                                 ; A(2, n) = 2n + 3
   ((= m 1)
    (+ n 2))                                       ; A(1, n) = n+2
   (else
    (+ n 1))))                                     ; A(0, n) = n+1

;;; Affiche le message d'aide sur la sortie des erreurs.
(define (help)
  (display "Ackermann m n\n")
  (display (string-append "  " DRAGONSOFT "\n"))
  (display (string-append "          " VERSION "\n"))
  (display "  Print A(m, n)\n")
  (display "  with A(0,   n)   := n+1\n")
  (display "       A(m+1, 0)   := A(m, 1)\n")
  (display "       A(m+1, n+1) := A(m, A(m+1, n)), m, n positive integers\n")
  (exit 1))

;;;;;;;;;;;;
;;; main ;;;
;;;;;;;;;;;;
(display (list "Ackermann 3 2 =" (Ackermann 3 2)))
(newline)

(let ((nb (length (command-line))))
  (if (not (= nb 3))
      (help))
  (let* ((m (car(cdr(command-line))))
         (n (car(cdr(cdr(command-line))))))
    (set! m (string->number m))
    (set! n (string->number n))
    (if (or (not (integer? m)) (negative? m) (inexact? m)
            (not (integer? n)) (negative? n) (inexact? n))
        (help))
    (display (Ackermann m n))
    (newline)))
