;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; FiboNbSystem                         ;;;
;;;    Essai d'implementation du systeme ;;;
;;;    de numeration de Finonacci        ;;;
;;; (c) Olivier Pirson ------ DragonSoft ;;;
;;; Debute le 2 mai 2005                 ;;;
;;; v.00.01.00 --- 27 mai 2005           ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define VERSION "v.00.01.00 --- 2005 May 27")
(define DRAGONSOFT "(c) Olivier Pirson --- DragonSoft")

;;;;;;;;;;;;;;;;;
;;; Fonctions ;;;
;;;;;;;;;;;;;;;;;
;;; Renvoie la suite des chiffres que constitue l, l'ecriture en numeration de Fibonacci d'un natural
;;; pre: l: list vide ou d'exact natural
;;; O(length l)
(define (display-Fibonacci-nb-system l)
  (while (not (null? l))
         (display (if (car l)
                      "1"
                      "0"))
         (set! l (cdr l))))

;;; Renvoie la paire (F(k) . F(k+1)) ou F est la suite de Fibonacci definie par
;;;   F(0)   := 0
;;;   F(1)   := 1
;;;   F(k+2) := F(k+1) + F(k), pour tout k natural
;;;
;;; k... 0 1 2 3 4 5 6  7  8  9 10 11  12  13  14  15  16   17   18   19   20    21...
;;; F(k) 0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597 2584 4181 6765 10946
;;;
;;; ==> pour tout q naturel : F(2q+1) = F(q+1)^2 + F(q)^2
;;;                           F(2q+2) = F(q+2)^2 - F(q)^2
;;;                           F(2q+3) = F(q+2)^2 + F(q+1)^2
;;; pre: k: exact natural
;;; result: pair d'exact natural
;;; O( lg(k) * F(k)^lg(3) ) ! C'est meilleur que ca puisque
;;;                           les valeurs intermediaires sont plus petites que F(k)
(define (Fibonacci2 k)
  (if (>= k 2)
      (let* ((FqFq1 (Fibonacci2 (quotient (1- k) 2)))             ; (F(q) . F(q+1)) ou q = (k-1)/2
             (Fq  (natural-square (car FqFq1)))                   ; F(q  )^2
             (Fq1 (natural-square (cdr FqFq1)))                   ; F(q+1)^2
             (Fq2 (natural-square (+ (cdr FqFq1) (car FqFq1)))))  ; F(q+2)^2 = (F(q+1) + F(q))^2
        (if (odd? k)
            ;; F(k  ) = F(2q+1) = F(q+1)^2 + F(q)^2
            ;; F(k+1) = F(2q+2) = F(q+2)^2 - F(q)^2
            (cons (+ Fq1 Fq) (- Fq2 Fq))
            ;; F(k  ) = F(2q+2) = F(q+2)^2 - F(q  )^2
            ;; F(k+1) = F(2q+3) = F(q+2)^2 + F(q+1)^2
            (cons (- Fq2 Fq) (+ Fq2 Fq1))))
      ;;    (F(0)=0 . F(1)=1)
      ;; ou (F(1)=1 . F(2)=1)
      (cons k 1)))

;;; Avec k le plus grand natural tq F(k) <= n,
;;;   renvoie la pair (k . (F(k) . F(k+1)))
;;; pre: n: exact natural
;;; result: exact natural
;;; O( lg(n) * F(lg(n))^lg(3) + ((k - lg(n)) * F(k)) )
(define (Fibonacci-max n)
  (let* ((k (1+ (integer-length n)))  ; 1 + nombre de chiffres de n dans sa representation binaire
         (p (Fibonacci2 k))           ; (F(k) . F(k+1))
         (prev (cons 0 1)))           ; (F(0) . F(1))
    (while (<= (cdr p) n)
           ;; F(k) <= F(k+1) < n
           (set! k (+ k 2))
           (set! prev p)
           (set! p (Fibonacci2-next2 p)))
    (if (<= (car p) n)
        ;; F(k) <= n < F(k+1)
        (cons k p)
        ;; n < F(k) <= F(k+1)
        (cons (1- k) (cons (cdr prev) (car p))))))

;;; Renvoie une liste de boolean,
;;;   representant les chiffres 0 ou 1 de n dans le systeme de numeration de Fibonacci
;;; pre: n: exact natural
;;; result: list non vide d'exact natural
(define (Fibonacci-nb-system n)
  (let ((p-m (Fibonacci-max n)))
    (Fibonacci-nb-system-rec n '(#t) (car p-m) (Fibonacci2-prev (cdr p-m)))))  ; n '(#t) k (F(k-1) . F(k))

(define (Fibonacci-nb-system-rec n l k p)
  (if (zero? n)
      (if (<= k 1)
          (if (null? l)
              '(#f)
              l)
          (append (make-list (1- k) #f) l))  ; ne reste que des 0
      (if (<= (cdr p) n)
          (if (<= k 2)
              (Fibonacci-nb-system-rec (- n (cdr p))          (cons #t l)  (1- k)  (Fibonacci2-prev  p))     ; ajoute 1
              (Fibonacci-nb-system-rec (- n (cdr p)) (cons #f (cons #t l)) (- k 2) (Fibonacci2-prev2 p)))    ; ajoute 01
          (Fibonacci-nb-system-rec        n                   (cons #f l)  (1- k)  (Fibonacci2-prev  p)))))  ; ajoute 0

;;; Renvoie le natural n
;;;   dont l est la liste de boolean correspondant aux chiffres 0 ou 1
;;    de sa representation en systeme de numeration de Fibonacci
;;; pre: l: list non vide de boolean
;;; result: exact natural
(define (Fibonacci-nb-system->natural l)
  (Fibonacci-nb-system->natural-rec l 0 (cons 1 2)))  ; l 0 (F(2) . F(3))

(define (Fibonacci-nb-system->natural-rec l n p)
  (if (null? (cdr l))
      n
      (Fibonacci-nb-system->natural-rec (cdr l)
                                        (if (car l)
                                            (+ n (car p))
                                            n)
                                        (Fibonacci2-next p))))

;;; Avec p = (F(k) . F(k+1)), renvoie la pair (F(k+1) . F(k+2))
;;; pre: p: pair d'exact natural in {F(k) | k>=0}
;;; result: pair d'exact natural
;;; O( F(k) )
(define (Fibonacci2-next p)
  (cons (cdr p)                ; F(k+1)
        (+ (cdr p) (car p))))  ; F(k+2) = F(k+1) + F(k)

;;; Avec p = (F(k) . F(k+1)), renvoie la pair (F(k+2) . F(k+3))
;;; pre: p: pair d'exact natural in {F(k) | k>=0}
;;; result: pair d'exact natural
;;; O( F(k) )
(define (Fibonacci2-next2 p)
  (let ((Fk+2 (+ (cdr p) (car p))))  ; F(k+2) = F(k+1) + F(k)
    (cons Fk+2
          (+ Fk+2 (cdr p)))))  ; F(k+3) = F(k+2) + F(k+1)

;;; Avec p = (F(k) . F(k+1)), renvoie la pair (F(k-1) . F(k))
;;; pre: p: pair d'exact natural in {F(k) | k>=0}
;;; result: pair d'exact natural
;;; O( F(k) )
(define (Fibonacci2-prev p)
  (cons (- (cdr p) (car p))  ; F(k-1) = F(k+1) - F(k)
        (car p)))            ; F(k)

;;; Avec p = (F(k) . F(k+1)), renvoie la pair (F(k-2) . F(k-1))
;;; pre: p: pair d'exact natural in {F(k) | k>=1}
;;; result: pair d'exact natural
;;; O( F(k) )
(define (Fibonacci2-prev2 p)
  (let ((Fk-1 (- (cdr p) (car p))))  ; F(k-1) = F(k+1) - F(k)
    (cons (- (car p) Fk-1)  ; F(k-2) = F(k) - F(k-1)
          Fk-1)))

;;; Renvoie n * n
;;; pre: n exact natural
;;; result: exact natural
;;; O( n^lg(3) )
(define (natural-square n)
  (integer-expt n 2))

;;;;;;;;;;;;
;;; Main ;;;
;;;;;;;;;;;;
(let ((p-m (cons 0 (cons 0 1)))
      (l '()))
  (do ((i 0 (1+ i)))
      ((> i 55))
    (set! p-m (Fibonacci-max i))
    (set! l (Fibonacci-nb-system i))

    (display i)
    (display "\t")
    (display-Fibonacci-nb-system l)
    (if (not (= (Fibonacci-nb-system->natural l) i))
        (display " !!!"))
    (newline)))
