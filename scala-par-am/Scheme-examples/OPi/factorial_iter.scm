;; Factorial of n (by iterative process)
(define (factorial n)
  (define (loop n acc)
    (if (= n 0)
        acc
        (loop (- n 1) (* acc n))))
  (loop n 1))

(factorial 10)
