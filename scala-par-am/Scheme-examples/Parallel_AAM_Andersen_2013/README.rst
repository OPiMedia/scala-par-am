.. -*- restructuredtext -*-

============================================
Scheme examples / Parallel_AAM_Andersen_2013
============================================
Examples from paper
"Multi-core Parallelization of Abstracted Abstract Machines"
(Leif Andersen and Matthew Might, 2013).

http://matt.might.net/papers/andersen2013multicore.pdf
