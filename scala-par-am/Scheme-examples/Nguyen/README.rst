.. -*- restructuredtext -*-

========================
Scheme examples / Nguyen
========================
Examples taken from the paper:
Phúc C. Nguyễn, Thomas Gilray, Sam Tobin-Hochstadt, and David Van Horn,
"Size-Change Termination as a Contract:
Dynamically and Statically Enforcing Termination for Higher-Order Programs"
https://thomas.gilray.org/pdf/termination-contract.pdf
