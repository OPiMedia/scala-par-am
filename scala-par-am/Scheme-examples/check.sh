#!/bin/sh

# Run (during max 5 seconds) all Scheme examples to check that they are syntactically correct.

TIMEOUT=5
FILES=`ls */*.scm */*/*.scm | sort`

echo 'Output for each Scheme program:\n' > check_out.log
echo 'Error messages for each Scheme program:\n' > check_err.log

for FILE in $FILES
do
    echo $FILE >> check_out.log
    echo $FILE >> check_err.log

    timeout $TIMEOUT guile --debug --auto-compile $FILE >> check_out.log 2>> check_err.log

    echo ======================================== >> check_out.log
    echo ======================================== >> check_err.log
done
