#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
dup.py (March 12, 2019)

Simple duplicate the standard input
to standard output and error output.
"""

import sys


########
# Main #
########
def main():
    """
    Read and write
    """
    for line in sys.stdin:
        print(line, end='', file=sys.stdout)
        sys.stdout.flush()

        print(line, end='', file=sys.stderr)
        sys.stderr.flush()

if __name__ == '__main__':
    main()
