addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.10")  // to build JAR of the complete project
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")  // Scala style checker

/* Not used
resolvers += Resolver.bintrayRepo("kamon-io", "sbt-plugins")
addSbtPlugin("io.kamon" % "sbt-aspectj-runner" % "1.1.1")  // use by Kamon

addSbtPlugin("com.lightbend.sbt" % "sbt-javaagent" % "0.1.5")  // use by Kamon
*/
