name := "scala-par-am"
version := "01.00.02"
organization := "be.opimedia"
homepage := Some(url("https://bitbucket.org/OPiMedia/scala-par-am"))
description := "Parallel version of Scala-AM - A Framework for Static Analysis of Dynamic Languages"



cancelable in Global := true  // don't quit SBT when CTRL-C

fork in run := true



scalaVersion := "2.12.7"

// https://docs.scala-lang.org/overviews/compiler-options/
scalacOptions ++= Seq("-J-Xmx3G")
scalacOptions ++= Seq(
  "-deprecation",
  "-explaintypes",
  "-feature",
  "-unchecked",

  "-Xlint:constant",
  "-Xlint:doc-detached",
  "-Xlint:unused",

  "-Ywarn-dead-code",
  "-Ywarn-nullary-override",
  "-Ywarn-nullary-unit",
  "-Ywarn-unused:_",

  "-Ywarn-unused:params",
  "-Ywarn-unused:patvars",
  "-Ywarn-unused:privates",
  "-Ywarn-value-discard"
)

// Options to disable during development and debug
scalacOptions ++= Seq("-g:none", "-Xdisable-assertions", "-opt:unreachable-code", "-opt:simplify-jumps", "-opt:compact-locals", "-opt:copy-propagation", "-opt:redundant-casts", "-opt:box-unbox", "-opt:nullness-tracking", "-opt:l:inline", "-opt-inline-from")

maxErrors := 5
mainClass in (Compile, run) := Some("be.opimedia.scala_par_am.Main")




libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.23"  // concurrency actors
libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"  // parser: scala.util.parsing.combinator
libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.2.28"  // functional programming
libraryDependencies += "com.github.scopt" %% "scopt" % "3.7.1"  // command line options



/* Not used (disabled because can caused dependency issues)
libraryDependencies ++= Seq(  // to monitor Akka
  "io.kamon" %% "kamon-core" % "1.1.3",
  "io.kamon" %% "kamon-akka-2.5" % "1.1.2",
  "io.kamon" %% "kamon-prometheus" % "1.1.1"
)
*/

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.0" % "test"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8" % "test"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"



// Build complete JAR file with project (without test) and all libraries included Scala
// Adapted from
// https://kamon.io/docs/latest/guides/setting-up-the-agent/#using-sbt-assembly
// and https://github.com/sbt/sbt-assembly#merge-strategy
/* Not used
val aopMerge: sbtassembly.MergeStrategy = new sbtassembly.MergeStrategy {
  val name = "aopMerge"
  import scala.xml._
  import scala.xml.dtd._

  def apply(tempDir: File, path: String, files: Seq[File]): Either[String, Seq[(File, String)]] = {
    val dt = DocType("aspectj", PublicID("-//AspectJ//DTD//EN", "http://www.eclipse.org/aspectj/dtd/aspectj.dtd"), Nil)
    val file = MergeStrategy.createMergeTarget(tempDir, path)
    val xmls: Seq[Elem] = files.map(XML.loadFile)
    val aspectsChildren: Seq[Node] = xmls.flatMap(_ \\ "aspectj" \ "aspects" \ "_")
    val weaverChildren: Seq[Node] = xmls.flatMap(_ \\ "aspectj" \ "weaver" \ "_")
    val options: String = xmls.map(x => (x \\ "aspectj" \ "weaver" \ "@options").text).mkString(" ").trim
    val weaverAttr = if (options.isEmpty) Null else new UnprefixedAttribute("options", options, Null)
    val aspects = new Elem(null, "aspects", Null, TopScope, false, aspectsChildren: _*)
    val weaver = new Elem(null, "weaver", weaverAttr, TopScope, false, weaverChildren: _*)
    val aspectj = new Elem(null, "aspectj", Null, TopScope, false, aspects, weaver)
    XML.save(file.toString, aspectj, "UTF-8", xmlDecl = false, dt)
    IO.append(file, IO.Newline.getBytes(IO.defaultCharset))
    Right(Seq(file -> path))
  }
}

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "aop.xml") =>
    aopMerge
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
*/

mainClass in assembly := Some("be.opimedia.scala_par_am.Main")
test in assembly := {}
assemblyJarName in assembly := "../../scala-par-am.jar"
