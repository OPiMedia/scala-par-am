package be.opimedia.scala_par_am

case class Color(hex: String) extends MemoHashCode {
  override def toString = hex
}
object Colors {
  object StateInit  extends Color("#AAFFAA")  // green
  object StateEval  extends Color("#DDFFDD")  // "light" green
  object StateKont  extends Color("#DDDDFF")  // "light" blue
  object Halted     extends Color("#FFFFAA")  // "light" yellow
  object Error      extends Color("#FFAAAA")  // "light" red
  object Node       extends Color("white")
}



trait GraphOutputNode[N] {
  def label(node: N): String = node.toString
  def color(node: N): Color = Colors.Node
}
object GraphOutputNode {
  def apply[N](implicit g: GraphOutputNode[N]): GraphOutputNode[N] = g
}



trait GraphOutput {
  def graphInfo[N](graph: Graph[N]): Option[GraphInfo] = Some(graph.info)

  def out[N](config: Option[Configuration.MachineConfig], graph: Graph[N])(writer: java.io.Writer)(implicit g: GraphOutputNode[N]): Unit

  def toFile[N](config: Option[Configuration.MachineConfig], graph: Graph[N])(path: String)(implicit g: GraphOutputNode[N]): Unit = {
    Util.withFileWriter(path) { out(config, graph) }
  }
}



object GraphDOTOutput extends GraphOutput {
  /**
    * Writes a graph representation of graph in DOT format ([[https://www.graphviz.org/]]).
    */
  def out[N](config: Option[Configuration.MachineConfig], graph: Graph[N])(writer: java.io.Writer)(implicit g: GraphOutputNode[N]): Unit = {
    val filename: String = config match {
      case Some(c) => s" ${c.filename}"
      case None => ""
    }
    writer.append(s"""digraph "Scala-Par-AM$filename" {
  node[shape=box, style=filled, fillcolor=<${Colors.StateEval}>];

""")
    // Pseudo-node with graph info
    val graphInfo = graph.info.toString.replace("\t", "\\l")

    val machineInfo: String = config match {
      case Some(c) =>
        val processStr: String =
          if (Configuration.machineIsParallel(c.machine)) s"\\lp${c.process}"
          else ""

        List(
          s"lattice: ${c.lattice.toString}",
          s"bound: ${c.bound}",
          s"address: ${c.address.toString}",
          if (c.stepFilterEnabled) "step-filter" else "step",
          s"machine: ${c.machine.toString}"
        ).mkString("\\l") + processStr

      case None => ""
    }

    writer.append(f"""stats[style=rounded, label="$graphInfo\\l"];
config[style=rounded, label="$machineInfo\\l"]

""")
    writer.flush

    // List of nodes
    val nodes: Array[N] = graph.nodesArray

    for ((node, id) <- nodes.zipWithIndex) {
      val label = GraphOutputNode[N].label(node).replace("<", "&lt;").replace(">", "&gt;")
      val color = GraphOutputNode[N].color(node)
      val optionalParams = ((if (id == 0)
                             s", fillcolor=<${Colors.StateInit}>"
                             else (if (color == Colors.StateEval) "" else s", fillcolor=<$color>")))
      writer.append(s"$id[xlabel=$id, label=<$label>$optionalParams];\n")
    }
    writer.flush

    // List of edges
    writer.append("\n")
    for ((nodeSrc, idSrc) <- nodes.zipWithIndex) {
      for (idDest <- graph.neighborsId(nodeSrc).toArray.sorted)
        writer.append(s"$idSrc -> $idDest;\n")
    }

    writer.append("}\n")
    writer.flush
  }
}
