package be.opimedia.scala_par_am

import reflect.ClassTag

/** Several information about a graph.
  */
class GraphInfo(val nbNodes: Int, val nbEdges: Int,
                val graphDensity: Double,
                val outdegreePosMin: Int, val outdegreeMax: Int, val outdegreeAverage: Double,
                val outdegrees: Map[Int, Int]) {
  override
  def toString: String = f"# nodes: ${nbNodes}\t# edges: ${nbEdges}\tgraph density: ${graphDensity}%.3f\toutdegree positive min: ${outdegreePosMin}\toutdegree max: ${outdegreeMax}\toutdegree avg: ${outdegreeAverage}%.3f"
}



/**
  * Represents an immutable directed graph where nodes are elements of N:
  * (source node) -> (id of source node, set of neighbors)
  */
class Graph[N:ClassTag](val nodeIdNeighbors: Map[N, (Int, Set[N])]) {
  /** Returns a graph with node added. */
  def addNode(node: N): Graph[N] =
    if (nodeIdNeighbors.contains(node)) this
    else new Graph(nodeIdNeighbors + (node -> (size, Set.empty[N])))

  /**
    * Returns a graph with a set of edges added from the same node.
    * All not existing nodes are also added.
    */
  def addEdges(nodeSrc: N, nodesDest: Set[N]): Graph[N] =
    if (nodesDest.isEmpty) addNode(nodeSrc)
    else {
      var size: Int = nodeIdNeighbors.size

      // Get source node with these neighbors, or new node
      val (id: Int, neighbors: Set[N]) = nodeIdNeighbors.getOrElse(nodeSrc, (size, Set.empty[N]))
      if (id == nodeIdNeighbors.size)  // nodeSrc is a new node
        size += 1

      var newNodeIdNeighbors: Map[N, (Int, Set[N])] = nodeIdNeighbors

      // Add each new node of nodesDest
      for (nodeDest: N <- nodesDest if !nodeIdNeighbors.contains(nodeDest)) {
        newNodeIdNeighbors += (nodeDest -> (size, Set.empty[N]))
        size += 1
      }

      // Add nodeSrc (if new) and each new edge of edges
      new Graph(newNodeIdNeighbors + (nodeSrc -> (id, neighbors.union(nodesDest))))
    }

  /**
    * Returns a graph with edges added.
    * All not existing nodes are also added.
    */
  def addEdges(edges: Map[N, Set[N]]): Graph[N] = {
    if (edges.isEmpty) this
    else {
      val allPosibleNewNodes: Set[N] = edges.values.foldLeft(edges.keySet)(
        (possibleNewNodes: Set[N], neighbors: Set[N]) => possibleNewNodes.union(neighbors))

      var size: Int = nodeIdNeighbors.size
      var newNodeIdNeighbors: Map[N, (Int, Set[N])] = nodeIdNeighbors

      // Add each new node
      for (node: N <- allPosibleNewNodes if !nodeIdNeighbors.contains(node)) {
        newNodeIdNeighbors += (node -> (size, Set.empty[N]))
        size += 1
      }

      // Add each new edges
      for ((nodeSrc: N, newNeighbors: Set[N]) <- edges) {
        assert(newNodeIdNeighbors.contains(nodeSrc))
        val (id: Int, neighbors: Set[N]) = newNodeIdNeighbors(nodeSrc)
        newNodeIdNeighbors += (nodeSrc -> (id, neighbors.union(newNeighbors)))
      }

      new Graph(newNodeIdNeighbors)
    }
  }

  /** Returns the set of all neighbors of existing node. */
  def neighbors(node: N): Set[N] = {
    assume(nodeIdNeighbors.contains(node))
    nodeIdNeighbors(node)._2
  }

  /** Returns a set of all neighbors id of existing node. */
  def neighborsId(node: N): Set[Int] = {
    assume(nodeIdNeighbors.contains(node))

    neighbors(node).map(neighbor => nodeIdNeighbors(neighbor)._1)
  }

  /** Returns the node corresponding to id. */
  def nodeById(id: Int): Option[N] = nodeIdNeighbors.find(_._2._1 == id).map(_._1)

  /** Returns the id of existing node. */
  def nodeId(node: N): Int = {
    assume(nodeIdNeighbors.contains(node))
    nodeIdNeighbors(node)._1
  }

  /** Returns an array of all nodes. The index of each corresponding to their ids. */
  def nodesArray: Array[N] = {
    val array: Array[N] = Array.ofDim[N](size)

    for ((node, (id, _)) <- nodeIdNeighbors) {
      assume(id < array.size)
      array(id) = node
    }
    array
  }

  /** Returns the number of nodes. */
  def size: Int = nodeIdNeighbors.size


  /** Calculates and returns information about the graph. */
  def info: GraphInfo = {
    var nbEdges: Int = 0
    var outdegreePosMin: Int = Int.MaxValue
    var outdegreeMax: Int = 0
    var outdegrees: Map[Int, Int] = Map.empty[Int, Int]

    for ((nodeSrc, (_, neighbors)) <- nodeIdNeighbors) {
      val outdegree: Int = neighbors.size

      nbEdges += outdegree
      if (outdegree > 0) outdegreePosMin = math.min(outdegree, outdegreePosMin)
      outdegreeMax = math.max(outdegree, outdegreeMax)
      outdegrees += outdegree -> (outdegrees.getOrElse(outdegree, 0) + 1)
    }

    val graphDensity = if (size > 1) nbEdges.toDouble/(size*(size - 1)) else 0
    val outdegreeAverage = if (size > 0) nbEdges.toDouble/size else 0

    new GraphInfo(size, nbEdges,
      graphDensity,
      outdegreePosMin, outdegreeMax, outdegreeAverage,
      outdegrees)
  }
}
object Graph {
  /** Returns a new empty graph */
  def empty[N:ClassTag]: Graph[N] = new Graph(Map.empty[N, (Int, Set[N])])
}
