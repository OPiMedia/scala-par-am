package be.opimedia.scala_par_am

import scalaz.Semigroup

trait Count extends MemoHashCode {
  def inc: Count
}
case object CountOne extends Count {
  def inc = CountInfinity  // ???
}
case object CountInfinity extends Count {
  def inc = CountInfinity
}

object Count {
  /* We need it to form a semigroup to use |+| to join stores */
  implicit val isSemigroup  = new Semigroup[Count] {
    def append(x: Count, y: => Count) = CountInfinity
  }
}
