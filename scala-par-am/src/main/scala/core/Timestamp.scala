package be.opimedia.scala_par_am

trait Timestamp[T] {
  def name: String
  def initial(seed: String): T
  def tick(t: T): T
  def tick[Exp](t: T, e: Exp): T
}

object Timestamp {
  def apply[T : Timestamp]: Timestamp[T] = implicitly
}

trait TimestampWrapper {
  type T
  val isTimestamp: Timestamp[T]
}

/* Similar to KCFA(0), but doesn't include the empty list */
object ZeroCFA extends TimestampWrapper {
  trait T
  case class Time(seed: String) extends T
  implicit val isTimestamp = new Timestamp[T] {
    def name = "0-CFA"
    def initial(seed: String) = Time(seed)
    def tick(t: T) = t
    def tick[Exp](t: T, e: Exp) = t
  }
}
