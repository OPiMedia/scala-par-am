package be.opimedia.scala_par_am

/**
  * Memoize the default hash code method.
  * That can be improve use of all immutable object.
  */
trait MemoHashCode extends Product {
  // Comment the following code to compile a version without the hash code memoization.
  // (When this code is disabled only the class State memoizes its hash code.)
  private lazy val memoizedHashCode: Int = scala.runtime.ScalaRunTime._hashCode(this)

  /**
    * Memoized default hash code
    */
  override def hashCode: Int = memoizedHashCode
}
