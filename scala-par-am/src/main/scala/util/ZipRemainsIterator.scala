package be.opimedia.scala_par_am

/**
  * Iterator class to iterates on results equal to the zip(seq1, seq2) function
  * and then get the remains of the two initial sequences.
  *
  * https://www.scala-lang.org/api/current/scala/collection/Iterator.html#zip[B](that:scala.collection.IterableOnce[B]):Iterator[(A,B)]
  */
final class ZipRemainsIterator[Item1, Item2](seq1: Iterable[Item1], seq2: Iterable[Item2])
    extends Iterator[(Item1, Item2)] {
  val iter1: Iterator[Item1] = seq1.iterator
  val iter2: Iterator[Item2] = seq2.iterator
  val iterZip: Iterator[(Item1, Item2)] = iter1 zip iter2


  def hasNext: Boolean = iterZip.hasNext


  def next: (Item1, Item2) = iterZip.next


  /**
    * Returns iterators on remains of the initial seq1 and seq2.
    */
  def remains: (Iterator[Item1], Iterator[Item2]) = (iter1, iter2)
}
final object ZipRemainsIterator {
  def apply[Item1, Item2](seq1: Iterable[Item1], seq2: Iterable[Item2]): ZipRemainsIterator[Item1, Item2] =
    new ZipRemainsIterator[Item1, Item2](seq1, seq2)
}



/**
  * Similar to [[ZipRemainsIterator]] but the second sequence is a sequence of non empty sequences
  * and the iteration is done on items of seq1 and items of inner sequences of seqSeq2.
  */
final class ZipRemainsIterator2[Item1, Item2](seq1: Iterable[Item1], seqSeq2: Iterable[Iterable[Item2]])
    extends Iterator[(Item1, Item2)] {
  val iter1: Iterator[Item1] = seq1.iterator
  val iterSeq2: Iterator[Iterable[Item2]] = seqSeq2.iterator
  var iter2: Iterator[Item2] = if (iterSeq2.hasNext) iterSeq2.next.iterator else Nil.iterator
  var iterZip: Iterator[(Item1, Item2)] = iter1 zip iter2

  assert(!iterSeq2.hasNext || iter2.hasNext)  // inner sequences of seqSeq2 are assume non empty

  private def takeNextIter2IfNecessary(): Unit =
    if (!iter2.hasNext && iterSeq2.hasNext) {
      iter2 = iterSeq2.next.iterator

      assert(iter2.hasNext)  // inner sequences of seqSeq2 are assume non empty

      iterZip = iter1 zip iter2
    }


  def hasNext: Boolean = {
    takeNextIter2IfNecessary
    iterZip.hasNext
  }


  def next: (Item1, Item2) = {
    takeNextIter2IfNecessary
    iterZip.next
  }


  /**
    * Returns iterators on remains of the initial seq1 and seqSeq2.
    * Warning! Don't use again this ZipRemainsIterator2 iterator after a call of remains().
    */
  def remains: (Iterator[Item1], Iterator[Iterable[Item2]]) =
    (iter1, if (iter2.hasNext) List(iter2.toIterable).iterator ++ iterSeq2
            else iterSeq2)
}
final object ZipRemainsIterator2 {
  def apply[Item1, Item2](seq1: Iterable[Item1], seqSeq2: Iterable[Iterable[Item2]]): ZipRemainsIterator2[Item1, Item2] =
    new ZipRemainsIterator2[Item1, Item2](seq1, seqSeq2)
}
