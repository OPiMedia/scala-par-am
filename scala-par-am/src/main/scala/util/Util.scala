package be.opimedia.scala_par_am

object Util {
  /** Return true iff assertions was enabled at compilation time.
    *
    * Be aware, the complete project need to be recompiled when assertions are enabled/disabled.
    */
  def isAssertOn: Boolean =  // TODO: Maybe try https://www.scala-lang.org/api/current/scala/annotation/elidable.html
    try {
      assert(false)
      false
    }
    catch {
      case _: AssertionError => true
    }


  def fileContent(file: String): Option[String] = {
    val fCheck = new java.io.File(file)

    if (fCheck.exists && fCheck.isFile) {
      val f = scala.io.Source.fromFile(file)(scala.io.Codec("UTF-8"))
      val content = f.getLines.mkString("\n") + '\n'
      // Empty line added to the end to avoid parsing error
      // when a Scheme program ends with a comment

      f.close()
      Option(content)
    }
    else
      None
  }


  def withFileWriter(path: String)(body: java.io.Writer => Unit): Unit = {
    val f = new java.io.File(path)
    val bw = new java.io.BufferedWriter(new java.io.FileWriter(f))
    body(bw)
    bw.close()
  }
}



import scala.concurrent.duration.Duration

class Timeout(startingTime: Long, timeout: Long) {
  def reached: Boolean = (System.nanoTime - startingTime) > timeout
  def time: Double = timeNs / 1000000000.0  // 10^9
  def timeNs: Long = System.nanoTime - startingTime
}
object Timeout {
  def start(timeout: Long): Timeout = new Timeout(System.nanoTime, timeout)
  def start(timeout: Duration): Timeout = new Timeout(System.nanoTime, timeout.toNanos)
}
