package be.opimedia.scala_par_am

/**
  * Iterable class to iterates (without deterministic order)
  * on nbPart "equals" parts on items from a sequence of sequences.
  * When totalSize is not divisible by nbPart,
  * the firsts parts contain one item more than the lasts.
  * When totalSize <= nbPart,
  * the result is a sequence of totalSize List of one item.
  *
  * totalSize and nbPart must be > 0
  * and totalSize must be equals to the total number of items in seqs
  *
  * By example, Parts(List(Set(1, 2, 3), Set(4, 5, 6, 7, 8, 9, 10), Set(), Set(11, 12)), 12, 5)
  * gives an iterator on List(3, 2, 1), List(6, 5, 4), List(8, 7), List(10, 9), List(12, 11)
  * (with maybe a different order for items from the same set).
  *
  * TODO: Possible better idea:
  * The order is not important,
  * so it may be possible to access concurrently to seqs and build parts in parallel.
  */
final class Parts[Item](seqs: Iterable[Iterable[Item]], totalSize: Int, nbPart: Int)
    extends Iterable[List[Item]] {
  assume(totalSize > 0)
  assume(seqs.flatten.size == totalSize)
  assume(nbPart > 0)

  override
  def iterator: Iterator[List[Item]] = new Iterator[List[Item]] {
    val iter: Iterator[Iterable[Item]] = seqs.iterator
    var subIter: Iterator[Item] = if (iter.hasNext) iter.next.iterator else null

    val partSize: Int = totalSize / nbPart
    var remain: Int = totalSize % nbPart


    override
    def hasNext: Boolean = iter.hasNext || ((subIter != null) && subIter.hasNext)


    override
    def next: List[Item] =
      if (hasNext) {
        var part: List[Item] = Nil
        var nb: Int = if (remain != 0) partSize + 1 else partSize
        if (remain > 0)
          remain -= 1

        while (hasNext && (nb != 0)) {
          while (subIter.hasNext && (nb != 0)) {
            part ::= subIter.next
            nb -= 1
          }
          if (!subIter.hasNext)  // get next current set
            subIter = if (iter.hasNext) iter.next.iterator else null
        }

        part
      }
      else throw new java.util.NoSuchElementException
  }
}
final object Parts {
  /**
    * For some specific values,
    * directly apply Scala functions instead apply the implementation of Parts.
    *
    * totalSize and nbPart must be > 0
    * and totalSize must be equals to the total number of items in seqs
    */
  def parts[Item](seqs: Iterable[Iterable[Item]], totalSize: Int, nbPart: Int): Iterable[Iterable[Item]] = {
    assume(totalSize > 0)
    assume(seqs.flatten.size == totalSize)
    assume(nbPart > 0)

    if (totalSize == 1) seqs                                  // ((x))       -> ((x))
    else if (nbPart == 1) List(seqs.flatten)                  // ((...) ...) -> ((...))
    else if (totalSize <= nbPart) seqs.flatten.map(List(_))   // ((...) ...) -> ((x) ...)
    else new Parts[Item](seqs, totalSize, nbPart)             // general case
  }
}



/**
  * Iterable class to iterates (without deterministic order)
  * on nbParts lists items from a sequence of sequences.
  *
  * listSize and nbPart must be > 0
  * and listSize must be equals to the size of seqs
  *
  * By example, PartsHybrid(List(Set(1, 2, 3), Set(4, 5, 6, 7, 8, 9, 10), Set(), Set(11, 12)), 5, 3)
  * gives an iterator on List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10), List(11, 12)
  * (with maybe a different order for items from the same set).
  */
final class PartsHybrid[Item](seqs: Iterable[Iterable[Item]], listSize: Int, nbPart: Int)
    extends Iterable[List[Item]] {
  assume(listSize > 0)
  assume(seqs.size == listSize)
  assume(nbPart > 0)

  override
  def iterator: Iterator[List[Item]] = new Iterator[List[Item]] {
    val iter: Iterator[Iterable[Item]] = seqs.iterator

    val partSize: Int = listSize / nbPart
    var remain: Int = listSize % nbPart

    override
    def hasNext: Boolean = iter.hasNext


    override
    def next: List[Item] =
      if (iter.hasNext) {
        var part: List[Item] = Nil
        var nb: Int = if (remain != 0) partSize + 1 else partSize
        if (remain > 0)
          remain -= 1

        while (iter.hasNext && (nb != 0)) {
          part :::= iter.next.toList
          nb -= 1
        }

        part
      }
      else throw new java.util.NoSuchElementException
  }
}
final object PartsHybrid {
  /**
    * Same than [[Parts.parts]]
    * excepted when listSize >= nbPart.
    * In this case the sequence is cut (by [[PartsHybrid.parts]]) on these items and not on items of items.
    *
    * totalSize, listSize and nbPart must be > 0,
    * totalSize must be equals to the total number of items in seqs
    * and listSize must be equals to the size of seqs
    */
  def parts[Item](seqs: Iterable[Iterable[Item]], totalSize: Int, listSize: Int, nbPart: Int): Iterable[Iterable[Item]] = {
    assume(totalSize > 0)
    assume(seqs.flatten.size == totalSize)
    assume(seqs.size == listSize)
    assume(nbPart > 0)

    if (totalSize == 1) seqs                                  // ((x))       -> ((x))
    else if (nbPart == 1) List(seqs.flatten)                  // ((...) ...) -> ((...))
    else if (totalSize <= nbPart) seqs.flatten.map(List(_))   // ((...) ...) -> ((x) ...)
    else if (listSize >= nbPart) {                            // (X ...)     -> (X ...)
      if (listSize == nbPart) seqs
      else new PartsHybrid[Item](seqs, listSize, nbPart)
    }
    else new Parts[Item](seqs, totalSize, nbPart)             // general case
  }
}
