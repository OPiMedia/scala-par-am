package be.opimedia.scala_par_am

/**
  * Helpers to configure machine
  */
object Configuration {

  val dotOutputDirectory: String = "output-graph/dot"



  /**
    * List of all available (sequential and parallel) machines.
    * See [[Main]] to have a brief description of all these machines.
    */
  object Machine extends Enumeration {
    val oldAAM,
      SeqAAM, SeqAAMS, SeqAAMLS, SeqAAMLSH,
      ParAAMLSAState, ParAAMLSASet, ParAAMLSAPart,
      ParAAMCSState, ParAAMCSSet, ParAAMCSPart,
      ParAAMCState, ParAAMCSet, ParAAMCPart, ParAAMCHybrid,
      ParAAMCSetH, ParAAMCPartH, ParAAMCHybridH = Value
  }
  implicit val machineRead: scopt.Read[Machine.Value] = scopt.Read.reads(Machine withName _)

  /** Return the instance of the corresponding machine. */
  def getMachine(machineValue: Machine.Value, lattice: SchemeLattice, address: AddressWrapper, time: TimestampWrapper): AbstractMachine[SchemeExp, lattice.L, address.A, time.T] = {
    implicit val isSchemeLattice: IsSchemeLattice[lattice.L] = lattice.isSchemeLattice
    implicit val isAddress = address.isAddress
    implicit val isTimestamp = time.isTimestamp

    machineValue match {
      case Configuration.Machine.oldAAM => throw new Exception("The oldAAM machine correspond to the AAMNS (AAM No Subsumption) of modified version of Scala-AM and must be run by scala-am.jar. See <https://bitbucket.org/OPiMedia/scala-am>.")

      // Sequential machines, the references for comparison tests
      case Configuration.Machine.SeqAAM => new SeqAAM[SchemeExp, lattice.L, address.A, time.T]
      case Configuration.Machine.SeqAAMS => new SeqAAMS[SchemeExp, lattice.L, address.A, time.T]
      case Configuration.Machine.SeqAAMLS => new SeqAAMLS[SchemeExp, lattice.L, address.A, time.T]
      case Configuration.Machine.SeqAAMLSH => new SeqAAMLSH[SchemeExp, lattice.L, address.A, time.T]

      // Parallel machines: ParAAM-L-SA-* for Parallel AAM - Loop - SenderAggregator - *
      case Configuration.Machine.ParAAMLSAState => new ParAAMLSAState[SchemeExp, lattice.L, address.A, time.T]
      case Configuration.Machine.ParAAMLSASet => new ParAAMLSASet[SchemeExp, lattice.L, address.A, time.T]
      case Configuration.Machine.ParAAMLSAPart => new ParAAMLSAPart[SchemeExp, lattice.L, address.A, time.T]

      // Parallel machines: ParAAM-C-S-* for Parallel AAM - Concurrent - Sender - *
      case Configuration.Machine.ParAAMCSState => new ParAAMCSState[SchemeExp, lattice.L, address.A, time.T]
      case Configuration.Machine.ParAAMCSSet => new ParAAMCSSet[SchemeExp, lattice.L, address.A, time.T]
      case Configuration.Machine.ParAAMCSPart => new ParAAMCSPart[SchemeExp, lattice.L, address.A, time.T]

      // Parallel machine: ParAAM-C-* for Parallel AAM - Concurrent - *
      case Configuration.Machine.ParAAMCState => new ParAAMCState[SchemeExp, lattice.L, address.A, time.T]
      case Configuration.Machine.ParAAMCSet => new ParAAMCSet[SchemeExp, lattice.L, address.A, time.T]
      case Configuration.Machine.ParAAMCPart => new ParAAMCPart[SchemeExp, lattice.L, address.A, time.T]
      case Configuration.Machine.ParAAMCHybrid => new ParAAMCHybrid[SchemeExp, lattice.L, address.A, time.T]

      case Configuration.Machine.ParAAMCSetH => new ParAAMCSetH[SchemeExp, lattice.L, address.A, time.T]
      case Configuration.Machine.ParAAMCPartH => new ParAAMCPartH[SchemeExp, lattice.L, address.A, time.T]
      case Configuration.Machine.ParAAMCHybridH => new ParAAMCHybridH[SchemeExp, lattice.L, address.A, time.T]

      case machineValue => throw new Exception(s"Machine $machineValue unknown!")
    }
  }

  /** Return true iff the machine is a parallel machine */
  def machineIsParallel(machineValue: Machine.Value): Boolean =
    ((machineValue != Machine.oldAAM) &&
     Configuration.getMachine(machineValue, Configuration.getLattice(Lattice.TypeSet, false, 0), ClassicalAddress, ZeroCFA).isParallel)



  object Lattice extends Enumeration {
    val TypeSet, BoundedInt, ConstantPropagation = Value
  }
  implicit val latticeRead: scopt.Read[Lattice.Value] = scopt.Read.reads(Lattice withName _)

  /** Return the instance of the corresponding lattice. */
  def getLattice(latticeValue: Lattice.Value, counting: Boolean, bound: Int): SchemeLattice =
    latticeValue match {
      case Lattice.TypeSet =>
        new MakeSchemeLattice[Type.S, Concrete.B, Type.I, Type.F, Type.C, Type.Sym](counting)

      case Lattice.BoundedInt =>
        val bounded = new BoundedInteger(bound)
        new MakeSchemeLattice[Type.S, Concrete.B, bounded.I, Type.F, Type.C, Type.Sym](counting)

      case Lattice.ConstantPropagation =>
        new MakeSchemeLattice[ConstantPropagation.S, Concrete.B, ConstantPropagation.I, ConstantPropagation.F, ConstantPropagation.C, ConstantPropagation.Sym](counting)
    }



  object Address extends Enumeration {
    val Classical, ValueSensitive = Value
  }
  implicit val addressRead: scopt.Read[Address.Value] = scopt.Read.reads(Address withName _)

  /** Return the instance of the corresponding address. */
  def getAddress(addressValue: Address.Value): AddressWrapper =
    addressValue match {
      case Configuration.Address.Classical => ClassicalAddress
      case Configuration.Address.ValueSensitive => ValueSensitiveAddress
    }



  /**
    * Build a name for dot file, with information known a this time.
    */
  def automaticName(config: MachineConfig, timedOut: Boolean,
                    directory: String, extension: String): String = {
    (new java.io.File(directory)).mkdirs()

    val filename: String = config.filename.replace(java.io.File.separator, "_")
    val i: Int = filename.lastIndexOf('.')
    val basename: String = if (i >= 0) filename.substring(0, i) else filename

    val timedOutStr: String = if (timedOut) "TimedOut__" else ""
    val processStr: String =
      if (machineIsParallel(config.machine)) s"__p${config.process}"
      else ""

    List(
      s"$directory${java.io.File.separator}$timedOutStr$basename",
      config.lattice.toString,
      s"bound_${config.bound}",
      config.address.toString,
      if (config.stepFilterEnabled) "step-filter" else "step",
      config.machine.toString
    ).mkString("__") + s"$processStr.$extension"
  }



  import scala.concurrent.duration.FiniteDuration

  /** The complete configuration */
  case class MachineConfig(
    lattice: Lattice.Value = Lattice.TypeSet,
    bound: Int = 100,
    address: Address.Value = Address.Classical,
    filename: String = "",
    stepFilterEnabled: Boolean = false,
    machine: Machine.Value = Machine.SeqAAMLS,
    process: Int = 1,

    timeout: FiniteDuration = FiniteDuration(1, "day"),

    statsEnabled: Boolean = false,

    outputGraphEnabled: Boolean = false,
  ) {
    override
    def toString: String = s"MachineConfig:\tlattice: $lattice\tbound: $bound\taddress: $address\tfilename: $filename\tstepFilterEnabled: $stepFilterEnabled\tmachine: $machine\tprocess: $process\ttimeout: $timeout\tstatsEnabled: $statsEnabled\toutputGraphEnabled: $outputGraphEnabled"
  }
}
