package be.opimedia.scala_par_am

/**
 * Abstract syntax of Scheme programs (probably far from complete)
 */
trait SchemeExp extends MemoHashCode  {
  val pos: Position
}
object SchemeExp {
  implicit val isExp: Expression[SchemeExp] = new Expression[SchemeExp] {
    def pos(e: SchemeExp) = e.pos
  }
}

/**
 * A lambda expression: (lambda (args...) body...)
 * Not supported: "rest"-arguments, of the form (lambda arg body), or (lambda (arg1 . args) body...)
 */
case class SchemeLambda(args: List[Identifier], body: List[SchemeExp], pos: Position) extends SchemeExp {
  assert(body.nonEmpty)
  override def toString = {
    val a = args.mkString(" ")
    val b = body.mkString(" ")
    s"(lambda ($a) $b)"
  }
}

/**
 * A function call: (f args...)
 */
case class SchemeFuncall(f: SchemeExp, args: List[SchemeExp], pos: Position) extends SchemeExp {
  override def toString = {
    if (args.isEmpty) {
      s"($f)"
    } else {
      val a = args.mkString(" ")
      s"($f $a)"
    }
  }
}
/**
 * An if statement: (if cond cons alt)
 * If without alt clauses need to be encoded with an empty begin as alt clause
 */
case class SchemeIf(cond: SchemeExp, cons: SchemeExp, alt: SchemeExp, pos: Position) extends SchemeExp {
  override def toString = s"(if $cond $cons $alt)"
}
/**
 * Let-bindings: (let ((v1 e1) ...) body...)
 */
case class SchemeLet(bindings: List[(Identifier, SchemeExp)], body: List[SchemeExp], pos: Position) extends SchemeExp {
  override def toString = {
    val bi = bindings.map({ case (name, exp) => s"($name $exp)" }).mkString(" ")
    val bo = body.mkString(" ")
    s"(let ($bi) $bo)"
  }
}
/**
 * Let*-bindings: (let* ((v1 e1) ...) body...)
 */
case class SchemeLetStar(bindings: List[(Identifier, SchemeExp)], body: List[SchemeExp], pos: Position) extends SchemeExp {
  override def toString = {
    val bi = bindings.map({ case (name, exp) => s"($name $exp)" }).mkString(" ")
    val bo = body.mkString(" ")
    s"(let* ($bi) $bo)"
  }
}
/**
 * Letrec-bindings: (letrec ((v1 e1) ...) body...)
 */
case class SchemeLetrec(bindings: List[(Identifier, SchemeExp)], body: List[SchemeExp], pos: Position) extends SchemeExp {
  override def toString = {
    val bi = bindings.map({ case (name, exp) => s"($name $exp)" }).mkString(" ")
    val bo = body.mkString(" ")
    s"(letrec ($bi) $bo)"
  }
}

/**
 * Named-let: (let name ((v1 e1) ...) body...)
 */
case class SchemeNamedLet(name: Identifier, bindings: List[(Identifier, SchemeExp)], body: List[SchemeExp], pos: Position) extends SchemeExp {
  override def toString = {
    val bi = bindings.map({ case (name, exp) => s"($name $exp)" }).mkString(" ")
    val bo = body.mkString(" ")
    s"(let $name ($bi) $bo)"
  }
}
/**
 * A set! expression: (set! variable value)
 */
case class SchemeSet(variable: Identifier, value: SchemeExp, pos: Position) extends SchemeExp {
  override def toString = s"(set! $variable $value)"
}
/**
 * A begin clause: (begin body...)
 */
case class SchemeBegin(exps: List[SchemeExp], pos: Position) extends SchemeExp {
  override def toString = {
    val body = exps.mkString(" ")
    s"(begin $body)"
  }
}
/**
 * A cond expression: (cond (test1 body1...) ...)
 */
case class SchemeCond(clauses: List[(SchemeExp, List[SchemeExp])], pos: Position) extends SchemeExp {
  override def toString = {
    val c = clauses.map({ case (cond, cons) => {
      val b = cons.mkString(" ")
      s"($cond $b)"
    }}).mkString(" ")
    s"(cond $c)"
  }
}

/**
 * A case expression: (case key ((vals1...) body1...) ... (else default...))
 */
case class SchemeCase(key: SchemeExp, clauses: List[(List[SchemeValue], List[SchemeExp])], default: List[SchemeExp], pos: Position) extends SchemeExp {
  override def toString = {
    val c = clauses.map({ case (datums, cons) => {
      val d = datums.mkString(" ")
      val b = cons.mkString(" ")
      s"(($d) $b)"
    }}).mkString(" ")
    if (default.isEmpty) {
      s"(case $key $c)"
    } else {
      s"(case $key $c (else ${default.mkString(" ")}))"
    }
  }
}

/**
 * An and expression: (and exps...)
 */
case class SchemeAnd(exps: List[SchemeExp], pos: Position) extends SchemeExp {
  override def toString = {
    val e = exps.mkString(" ")
    s"(and $e)"
  }
}
/**
 * An or expression: (or exps...)
 */
case class SchemeOr(exps: List[SchemeExp], pos: Position) extends SchemeExp {
  override def toString = {
    val e = exps.mkString(" ")
    s"(or $e)"
  }
}
/**
 * A variable definition: (define name value)
 */
case class SchemeDefineVariable(name: Identifier, value: SchemeExp, pos: Position) extends SchemeExp {
  override def toString = s"(define $name $value)"
}
/**
 * A function definition: (define (name args...) body...)
 */
case class SchemeDefineFunction(name: Identifier, args: List[Identifier], body: List[SchemeExp], pos: Position) extends SchemeExp {
  override def toString = {
    val a = args.mkString(" ")
    val b = body.mkString(" ")
    s"(define ($name $a) $b)"
  }
}

/**
 * Do notation: (do ((<variable1> <init1> <step1>) ...) (<test> <expression> ...) <command> ...)
 */
case class SchemeDo(vars: List[(Identifier, SchemeExp, Option[SchemeExp])], test: SchemeExp, finals: List[SchemeExp], commands: List[SchemeExp], pos: Position) extends SchemeExp {
  override def toString = {
    val varsstr = vars.map({ case (v, i, s) => s"($v $i $s)" }).mkString(" ")
    val finalsstr = finals.mkString(" ")
    val commandsstr = commands.mkString(" ")
    s"(do ($varsstr) ($test $finalsstr) $commandsstr)"
  }
}
/**
 * An identifier: name
 */
case class SchemeVar(id: Identifier) extends SchemeExp {
  val pos = id.pos
  override def toString = id.name
}

/**
 * A quoted expression: '(foo (bar baz))
 *  The quoted expression is *not* converted to a Scheme expression, and remains
 * a simple s-expression, because that's exactly what it should be.
 */
case class SchemeQuoted(quoted: SExp, pos: Position) extends SchemeExp {
  override def toString = s"'$quoted"
}

/**
 * A literal value (number, symbol, string, ...)
 */
case class SchemeValue(value: Value, pos: Position) extends SchemeExp {
  override def toString = value.toString
}


/**
 * Object that provides a method to compile an s-expression into a Scheme expression
 */
object SchemeCompiler {
  /**
    * Reserved keywords
    */
  val reserved: List[String] = List("lambda", "if", "let", "let*", "letrec", "cond", "case", "set!", "begin", "define", "do")

  def compile(exp: SExp): SchemeExp = exp match {
    case SExpPair(SExpId(Identifier("quote", _)), SExpPair(quoted, SExpValue(ValueNil, _), _), _) =>
      compile(SExpQuoted(quoted, exp.pos))
    case SExpPair(SExpId(Identifier("quote", _)), _, _) =>
      throw new Exception(s"Invalid Scheme quote: $exp (${exp.pos})")
    case SExpPair(SExpId(Identifier("lambda", _)),
      SExpPair(args, SExpPair(first, rest, _), _), _) =>
      SchemeLambda(compileArgs(args), compile(first) :: compileBody(rest), exp.pos)
    case SExpPair(SExpId(Identifier("lambda", _)), _, _) =>
      throw new Exception(s"Invalid Scheme lambda: $exp (${exp.pos})")
    case SExpPair(SExpId(Identifier("if", _)),
      SExpPair(cond, SExpPair(cons, SExpPair(alt, SExpValue(ValueNil, _), _), _), _), _) =>
      SchemeIf(compile(cond), compile(cons), compile(alt), exp.pos)
    case SExpPair(SExpId(Identifier("if", _)),
      SExpPair(cond, SExpPair(cons, SExpValue(ValueNil, _), _), _), _) =>
      /* Empty else branch is replaced by #f (R5RS states it's unspecified) */
      SchemeIf(compile(cond), compile(cons), SchemeValue(ValueBoolean(false), exp.pos), exp.pos)
    case SExpPair(SExpId(Identifier("if", _)), _, _) =>
        throw new Exception(s"Invalid Scheme if: $exp (${exp.pos})")
    case SExpPair(SExpId(Identifier("let", _)),
      SExpPair(SExpId(name), SExpPair(bindings, SExpPair(first, rest, _), _), _), _) =>
      SchemeNamedLet(name, compileBindings(bindings), compile(first) :: compileBody(rest), exp.pos)
    case SExpPair(SExpId(Identifier("let", _)),
      SExpPair(bindings, SExpPair(first, rest, _), _), _) =>
      SchemeLet(compileBindings(bindings), compile(first) :: compileBody(rest), exp.pos)
    case SExpPair(SExpId(Identifier("let", _)), _, _) =>
      throw new Exception(s"Invalid Scheme let: $exp")
    case SExpPair(SExpId(Identifier("let*", _)),
      SExpPair(bindings, SExpPair(first, rest, _), _), _) =>
      SchemeLetStar(compileBindings(bindings), compile(first) :: compileBody(rest), exp.pos)
    case SExpPair(SExpId(Identifier("let*", _)), _, _) =>
      throw new Exception(s"Invalid Scheme let*: $exp")
    case SExpPair(SExpId(Identifier("letrec", _)),
      SExpPair(bindings, SExpPair(first, rest, _), _), _) =>
      SchemeLetrec(compileBindings(bindings), compile(first) :: compileBody(rest), exp.pos)
    case SExpPair(SExpId(Identifier("letrec", _)), _, _) =>
      throw new Exception(s"Invalid Scheme letrec: $exp")
    case SExpPair(SExpId(Identifier("set!", _)),
      SExpPair(SExpId(v), SExpPair(value, SExpValue(ValueNil, _), _), _), _) =>
      SchemeSet(v, compile(value), exp.pos)
    case SExpPair(SExpId(Identifier("set!", _)), _, _) =>
      throw new Exception(s"Invalid Scheme set!: $exp")
    case SExpPair(SExpId(Identifier("begin", _)), body, _) =>
      SchemeBegin(compileBody(body), exp.pos)
    case SExpPair(SExpId(Identifier("cond", _)), clauses, _) =>
      SchemeCond(compileCondClauses(clauses), exp.pos)
    case SExpPair(SExpId(Identifier("case", _)), SExpPair(exp, clauses, _), _) =>
      val (c, d) = compileCaseClauses(clauses)
      SchemeCase(compile(exp), c, d, exp.pos)
    case SExpPair(SExpId(Identifier("and", _)), args, _) =>
      SchemeAnd(compileBody(args), exp.pos)
      case SExpPair(SExpId(Identifier("or", _)), args, _) =>
      SchemeOr(compileBody(args), exp.pos)
    case SExpPair(SExpId(Identifier("define", _)),
      SExpPair(SExpId(name), SExpPair(value, SExpValue(ValueNil, _), _), _), _) =>
      SchemeDefineVariable(name, compile(value), exp.pos)
    case SExpPair(SExpId(Identifier("define", _)),
      SExpPair(SExpPair(SExpId(name), args, _),
        SExpPair(first, rest, _), _), _) =>
      SchemeDefineFunction(name, compileArgs(args), compile(first) :: compileBody(rest), exp.pos)
    case SExpPair(SExpId(Identifier("do", _)),
      SExpPair(bindings, SExpPair(SExpPair(test, finals, _), commands, _), _), _) =>
      SchemeDo(compileDoBindings(bindings), compile(test), compileBody(finals), compileBody(commands), exp.pos)

    case SExpPair(f, args, _) =>
      SchemeFuncall(compile(f), compileBody(args), exp.pos)
    case SExpId(v) => if (reserved.contains(v.name)) {
      throw new Exception(s"Invalid Scheme identifier (reserved): $exp")
    } else {
      SchemeVar(v)
    }
    case SExpValue(value, _) => SchemeValue(value, exp.pos)
    case SExpQuoted(quoted, _) => SchemeQuoted(quoted, exp.pos)
  }

  def compileArgs(args: SExp): List[Identifier] = args match {
    case SExpPair(SExpId(id), rest, _) => id :: compileArgs(rest)
    case SExpValue(ValueNil, _) => Nil
    case _ => throw new Exception(s"Invalid Scheme argument list: $args (${args.pos})")
  }

  def compileBody(body: SExp): List[SchemeExp] = body match {
    case SExpPair(exp, rest, _) => compile(exp) :: compileBody(rest)
    case SExpValue(ValueNil, _) => Nil
    case _ => throw new Exception(s"Invalid Scheme body: $body (${body.pos})")
  }

  def compileBindings(bindings: SExp): List[(Identifier, SchemeExp)] = bindings match {
    case SExpPair(SExpPair(SExpId(v),
      SExpPair(value, SExpValue(ValueNil, _), _), _), rest, _) =>
      if (reserved.contains(v.name)) {
        throw new Exception(s"Invalid Scheme identifier (reserved): $v (${bindings.pos})")
      } else {
        (v, compile(value)) :: compileBindings(rest)
      }
    case SExpValue(ValueNil, _) => Nil
    case _ => throw new Exception(s"Invalid Scheme bindings: $bindings (${bindings.pos})")
  }

  def compileDoBindings(bindings: SExp): List[(Identifier, SchemeExp, Option[SchemeExp])] = bindings match {
    case SExpPair(SExpPair(SExpId(v),
      SExpPair(value, SExpValue(ValueNil, _), _), _), rest, _) =>
      if (reserved.contains(v.name)) {
        throw new Exception(s"Invalid Scheme identifier (reserved): $v (${bindings.pos})")
      } else {
        (v, compile(value), None) :: compileDoBindings(rest)
      }
    case SExpPair(SExpPair(SExpId(v),
      SExpPair(value, SExpPair(step, SExpValue(ValueNil, _), _), _), _), rest, _) =>
      if (reserved.contains(v.name)) {
        throw new Exception(s"Invalid Scheme identifier (reserved): $v (${bindings.pos})")
      } else {
        (v, compile(value), Some(compile(step))) :: compileDoBindings(rest)
      }
    case SExpValue(ValueNil, _) => Nil
    case _ => throw new Exception(s"Invalid Scheme do-bindings: $bindings (${bindings.pos})")
  }

  def compileCondClauses(clauses: SExp): List[(SchemeExp, List[SchemeExp])] = clauses match {
    case SExpPair(SExpPair(SExpId(Identifier("else", _)), SExpPair(first, rest, _), _),
                  SExpValue(ValueNil, _), _) =>
      List((SchemeValue(ValueBoolean(true), clauses.pos), compile(first) :: compileBody(rest)))
    case SExpPair(SExpPair(cond, SExpPair(first, rest, _), _), restClauses, _) =>
      (compile(cond), compile(first) :: compileBody(rest)) :: compileCondClauses(restClauses)
    case SExpPair(SExpPair(cond, SExpValue(ValueNil, _), _), restClauses, _) =>
      (compile(cond), Nil) :: compileCondClauses(restClauses)
    case SExpValue(ValueNil, _) => Nil
    case _ => throw new Exception(s"Invalid Scheme cond clauses: $clauses ${clauses.pos})")
  }

  def compileCaseClauses(clauses: SExp): (List[(List[SchemeValue], List[SchemeExp])], List[SchemeExp]) = clauses match {
    case SExpPair(SExpPair(SExpId(Identifier("else", _)), SExpPair(first, rest, _), _),
                  SExpValue(ValueNil, _), _) =>
      (List(), compile(first) :: compileBody(rest))
    case SExpPair(SExpPair(objects, body, _), restClauses, _) =>
      val (compiled, default) = compileCaseClauses(restClauses)
      ((compileCaseObjects(objects), compileBody(body)) :: compiled, default)
    case SExpValue(ValueNil, _) => (Nil, Nil)
    case _ => throw new Exception(s"Invalid Scheme case clauses: $clauses (${clauses.pos})")
  }

  def compileCaseObjects(objects: SExp): List[SchemeValue] = objects match {
    case SExpPair(SExpValue(v, _), rest, _) =>
      SchemeValue(v, objects.pos) :: compileCaseObjects(rest)
    case SExpPair(SExpId(id), rest, _) =>
      /* identifiers in case expressions are treated as symbols */
      SchemeValue(ValueSymbol(id.name), id.pos) :: compileCaseObjects(rest)
    case SExpValue(ValueNil, _) => Nil
    case _ => throw new Exception(s"Invalid Scheme case objects: $objects (${objects.pos})")
  }
}


/**
 * Remove defines from a Scheme expression, replacing them by let bindings.
 * For example:
 *   (define foo 1)
 *   (define (f x) x)
 *   (f foo)
 * Will be converted to:
 *   (letrec ((foo 1)
 *            (f (lambda (x) x)))
 *     (f foo))
 * Which is semantically equivalent with respect to the end result
 */
object SchemeUndefiner {
  def undefine(exps: List[SchemeExp]): SchemeExp =
    undefine(exps, List())

  def undefine(exps: List[SchemeExp], defs: List[(Identifier, SchemeExp)]): SchemeExp = exps match {
    case Nil => SchemeBegin(Nil, Position.none)
    case SchemeDefineFunction(name, args, body, pos) :: rest => undefine(SchemeDefineVariable(name, SchemeLambda(args, undefineBody(body), exps.head.pos), pos) :: rest, defs)
    case SchemeDefineVariable(name, value, _) :: rest => undefine(rest, (name, undefine1(value)) :: defs)
    case _ :: _ => if (defs.isEmpty) {
      undefineBody(exps) match {
        case Nil => SchemeBegin(Nil, Position.none)
        case exp :: Nil => exp
        case exps => SchemeBegin(exps, exps.head.pos)
      }
    } else {
      SchemeLetrec(defs.reverse, undefineBody(exps), exps.head.pos)
    }
  }

  def undefine1(exp: SchemeExp): SchemeExp = undefine(List(exp))

  def undefineBody(exps: List[SchemeExp]): List[SchemeExp] = exps match {
    case Nil => Nil
    case SchemeDefineFunction(_, _, _, _) :: _ => List(undefine(exps, List()))
    case SchemeDefineVariable(_, _, _) :: _ => List(undefine(exps, List()))
    case exp :: rest => {
      val exp2 = exp match {
        case SchemeLambda(args, body, pos) => SchemeLambda(args, undefineBody(body), pos)
        case SchemeFuncall(f, args, pos) => SchemeFuncall(undefine1(f), args.map(undefine1), pos)
        case SchemeIf(cond, cons, alt, pos) => SchemeIf(undefine1(cond), undefine1(cons), undefine1(alt), pos)
        case SchemeLet(bindings, body, pos) => SchemeLet(bindings.map({ case (b, v) => (b, undefine1(v)) }), undefineBody(body), pos)
        case SchemeLetStar(bindings, body, pos) => SchemeLetStar(bindings.map({ case (b, v) => (b, undefine1(v)) }), undefineBody(body), pos)
        case SchemeLetrec(bindings, body, pos) => SchemeLetrec(bindings.map({ case (b, v) => (b, undefine1(v)) }), undefineBody(body), pos)
        case SchemeNamedLet(name, bindings, body, pos) => SchemeNamedLet(name, bindings.map({ case (b, v) => (b, undefine1(v)) }), undefineBody(body), pos)
        case SchemeSet(variable, value, pos) => SchemeSet(variable, undefine1(value), pos)
        case SchemeBegin(exps, pos) => SchemeBegin(undefineBody(exps), pos)
        case SchemeCond(clauses, pos) => SchemeCond(clauses.map({ case (cond, body) => (undefine1(cond), undefineBody(body)) }), pos)
        case SchemeCase(key, clauses, default, pos) => SchemeCase(undefine1(key), clauses.map({ case (vs, body) => (vs, undefineBody(body)) }), undefineBody(default), pos)
        case SchemeAnd(args, pos) => SchemeAnd(args.map(undefine1), pos)
        case SchemeOr(args, pos) => SchemeOr(args.map(undefine1), pos)
        case SchemeDo(vars, test, finals, commands, pos) => SchemeDo(
          vars.map({ case (id, init, step) => (id, undefine1(init), step.map(undefine1)) }),
          undefine1(test), undefineBody(finals), undefineBody(commands), pos)
        case SchemeVar(id) => SchemeVar(id)
        case SchemeQuoted(quoted, pos) => SchemeQuoted(quoted, pos)
        case SchemeValue(value, pos) => SchemeValue(value, pos)
      }
      exp2 :: undefineBody(rest)
    }
  }
}


object Scheme {
  /**
   * Compiles a s-expression into a scheme expression
   */
  def compile(exp: SExp): SchemeExp = SchemeCompiler.compile(exp)

  /**
   * Replace defines in a program (a list of expressions) by a big letrec as a single expression
   */
  def undefine(exps: List[SchemeExp]): SchemeExp = SchemeUndefiner.undefine(exps)

  /**
   * Parse a string representing a Scheme program
   */
  def parse(s: String): SchemeExp = undefine(SExpParser.parse(s).map(compile _))
}
