package be.opimedia.scala_par_am

trait Expression[E] {
  def pos(e: E): Position
}

object Expression {
  def apply[E : Expression]: Expression[E] = implicitly
}
