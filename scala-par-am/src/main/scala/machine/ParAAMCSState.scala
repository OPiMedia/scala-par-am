package be.opimedia.scala_par_am

/**
  * ParAAMCSState: ParAAM-C-S-state for Parallel AAM - Concurrent - Sender - state
  *
  * The worklist is implemented by an immutable list of sets of states.
  *
  * The main method eval() just starts the unique actor ActorSender.
  * This actor sends the initial state to the first actor ActorEval.
  *
  * Each actor ActorEval evaluates a state, updates data,
  * and sends that it is now available to the actor ActorSender.
  *
  * Each time that the ActorSender receives a message from an actor ActorEval,
  * it sends one state of the worklist to each actor ActorEval available,
  * until to the worklist becomes empty.
  *
  * Extend [[ParAAMLSAState]] class to reuse common code.
  *
  * <strong>Source</strong> [[https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/machine/ParAAMCSState.scala]]
  */
class ParAAMCSState[Exp : Expression, Abs : JoinLattice, Addr : Address, Time : Timestamp]
    extends ParAAMLSAState[Exp, Abs, Addr, Time] {
  override def name: String = "ParAAM-C-S-state"

  override def minProcess: Int = 2  // 1 ActorSender and at least 1 ActorEval
  override def nbNotEvalProcess: Int = 1  // the ActorSender

  /** Print function use during development and debug */
  private def p(s: String): Unit = synchronized {
    if (false) {
      System.err.println(s)
      System.err.flush
    }
  }
  p(s"DEBUG p $name!")



  /**
    * Performs the evaluation of an expression as [[SeqAAM.eval]].
    * It is the main work that is parallelized.
    */
  override
  def eval(exp: Exp, sem: Semantics[Exp, Abs, Addr, Time], config: Configuration.MachineConfig): Output = {
    require(config.process >= minProcess)

    val computeGraph: Boolean = config.outputGraphEnabled
    val timeout: Timeout = Timeout.start(config.timeout)

    import akka.actor.{Actor, ActorRef, ActorSystem, Props}
    import akka.pattern.ask
    import scala.concurrent.{Await, Future}
    import scala.concurrent.duration._
    import scala.language.postfixOps

    implicit val _ = akka.util.Timeout(config.timeout.toNanos nanoseconds)
    // implicit val _ = akka.util.Timeout(10 second)  // for debug


    // Set the evaluation step function, with filtering enabled or not
    def step(state: State, sem: Semantics[Exp, Abs, Addr, Time], visited: Set[State]): Set[State] =
      if (config.stepFilterEnabled) state.stepFilter(sem, visited) else state.step(sem)


    // Starts with the initial state resulting from injecting the program below
    var worklist: List[Iterable[State]] = Nil

    // Set of states already visited
    var visited: Set[State] = Set.empty[State]

    val stats: ParAAMStats =
      if (config.statsEnabled) new ParAAMStatsEnabled
      else new ParAAMStatsDisabled


    // Lock used to access and modify worklist
    val worklistLock: AnyRef = AnyRef

    // Lock used to modify visited
    val visitedLock: AnyRef = AnyRef


    val maxActorEval: Int = config.process - nbNotEvalProcess

    assert(maxActorEval > 0)

    val actorSystem: ActorSystem = ActorSystem("Akka-System-ParAAMCSState-eval")
    val actorEvals: Array[ActorRef] = Array.ofDim[ActorRef](maxActorEval)


    //
    // Definition and instantiation of the 2 types of actors
    //

    final object ActorSender {
      val ActorEvalAvailable: Int = 1
      val Start: Int = 2
    }

    final object ActorEval {
      final case class Eval(state: State)
      val ReturnLocalResults: Int = 11
    }


    // Actor that activates actorEvals and sends states to them.
    final class ActorSender extends Actor {
      import ActorSender.{ActorEvalAvailable, Start}

      // List of all available ActorEval
      var availableActorEvals: List[ActorRef] = null
      var availableActorEvalsSize: Int = 0

      // Reference to main thread that wait result
      var mainCaller: ActorRef = null

      override
      def receive = {
        case ActorEvalAvailable =>  // sends one state to each ActorEval available
          availableActorEvals ::= sender  // makes the sender actor available
          availableActorEvalsSize += 1

          assert(availableActorEvals.size == availableActorEvalsSize)

          var currentWorklist: List[Iterable[State]] = null

          worklistLock.synchronized {
            currentWorklist = worklist
            worklist = Nil
          }

          if (currentWorklist.nonEmpty) {  // some states are available
            assert(availableActorEvals.nonEmpty)

            val actorEvalStateIter: ZipRemainsIterator2[ActorRef, State] =
              ZipRemainsIterator2(availableActorEvals, currentWorklist)

            // Sends available states to available actorEvals
            for ((actorEval: ActorRef, state: State) <- actorEvalStateIter) {
              stats.pickStates(1)
              stats.incSentNb
              actorEval ! ActorEval.Eval(state)
              availableActorEvalsSize -= 1
            }

            // Keeps remaining actors xor states
            val (remain1, remain2) = actorEvalStateIter.remains
            availableActorEvals = remain1.toList
            currentWorklist = remain2.toList

            assert(availableActorEvals.size == availableActorEvalsSize)
          }

          assert(availableActorEvals.isEmpty || currentWorklist.isEmpty)

          if (currentWorklist.nonEmpty)                        // updates with remaining states
            worklistLock.synchronized { worklist :::= currentWorklist }
          else if (availableActorEvalsSize == maxActorEval) {  // finished
            assert(worklist.isEmpty)
            val results: CollectedLocalResults =
              collectLocalResults(actorEvals, ActorEval.ReturnLocalResults, config.timeout,
                                  computeGraph)

            mainCaller ! results  // returns to main caller
          }

          // finished message sent or at least one ActorEval is "busy" and will answer

        case Start =>  // initializes and sends initial state
          mainCaller = sender
          availableActorEvals = (1 until maxActorEval).map(actorEvals).toList
          availableActorEvalsSize = maxActorEval - 1

          stats.incSentNb
          actorEvals.head ! ActorEval.Eval(State.inject(exp, sem.initialEnv, sem.initialStore))

        // case x => p(s"!!! Sender $x")  // for debug
      }
    }

    val actorSender: ActorRef = actorSystem.actorOf(Props(new ActorSender), "ActorSender")


    // Actor to evaluate state
    final class ActorEval extends Actor {
      import ActorEval.{Eval, ReturnLocalResults}

      var localHalted: List[State] = Nil
      var localEdges: List[(State, Set[State])] = Nil

      override
      def receive = {
        case Eval(state: State) =>  // evaluates state, updates, and sends that it is available
          if (!visited.contains(state)) {  // state probably not yet visited
            visitedLock.synchronized { visited += state }

            if (state.halted)  // halted state
              localHalted ::= state
            else {             // new state with successors
              val successors: Set[State] = step(state, sem, visited)
              if (successors.nonEmpty) {
                stats.newStates(successors.size)
                worklistLock.synchronized { worklist ::= successors }
                if (computeGraph)
                  localEdges ::= (state, successors)
              }
            }
          }
          else                             // already visited
            stats.incAlreadyVisitedNb

          actorSender ! ActorSender.ActorEvalAvailable

        case ReturnLocalResults => sender ! (localHalted, localEdges)

        // case x => p(s"!!! Eval $x")  // for debug
      }
    }

    for (i <- 0 until maxActorEval)  // create all ActorEval
      actorEvals(i) = actorSystem.actorOf(Props(new ActorEval), s"ActorEval-$i")


    //
    // Start by sending the initial state
    //
    val (timedOut: Boolean, (halted: List[State], graph: Option[Graph[State]])) =
      try {    // run during available time
        val future: Future[CollectedLocalResults] =
          (actorSender ? ActorSender.Start).mapTo[CollectedLocalResults]

        (false, Await.result(future, config.timeout))
      }
      catch {  // timed out
        case _: scala.concurrent.TimeoutException => (true, (Nil, None))
      }

    actorSystem.terminate

    val haltedSet: Set[State] = halted.toSet

    // Return result
    SeqAAMOutput(
      haltedSet,
      visited.size,
      timeout.time,
      graph,
      timedOut,
      stats)
  }
}
