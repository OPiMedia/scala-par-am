package be.opimedia.scala_par_am

import akka.actor.ActorRef
import akka.pattern.ask
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * ParAAMLSAState: ParAAM-L-SA-state for Parallel AAM - Loop - SenderAggregator - state
  *
  * The worklist is implemented by an immutable list of sets of states.
  *
  * The main method eval() loops until the worklist is empty.
  * An unique actor ActorSenderAggregator
  * sends successively each state of the worklist to actors ActorEval (modulo the number of these actors)
  * and then collects each of these results.
  *
  * Extend [[SeqAAM]] class to reuse common code.
  *
  * <strong>Source</strong> [[https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/machine/ParAAMLSAState.scala]]
  */
class ParAAMLSAState[Exp : Expression, Abs : JoinLattice, Addr : Address, Time : Timestamp]
    extends SeqAAM[Exp, Abs, Addr, Time] {
  override def name: String = "ParAAM-L-SA-state"

  override def isParallel: Boolean = true
  override def minProcess: Int = 2  // 1 ActorSenderAggregator and at least 1 ActorEval
  override def nbNotEvalProcess: Int = 1  // the ActorSenderAggregator

  /** Print function use during development and debug */
  private def p(s: String): Unit = synchronized {
    if (false) {
      System.err.println(s)
      System.err.flush
    }
  }
  p(s"DEBUG p $name!")



  /**
    * To collect statistic information about the evaluation of the expression.
    */
  protected abstract class ParAAMStats extends AbstractMachineStats {
    def incSentNb(): Unit
  }

  protected trait ParAAMStatsDisabledTrait extends AbstractMachineStatsDisabledTrait {
    def incSentNb(): Unit = ()

    override protected def toTsvNameList: List[String] = Nil

    override protected def toTsvList: List[String] = Nil
  }
  protected final class ParAAMStatsDisabled extends ParAAMStats with ParAAMStatsDisabledTrait

  protected trait ParAAMStatsEnabledTrait extends AbstractMachineStatsEnabledTrait {
    var sentNb: Int = 0

    def incSentNb(): Unit = this.synchronized { sentNb += 1 }

    override protected def toTsvNameList: List[String] = this.synchronized {
      super.toTsvNameList :+ "# sent"
    }

    override protected def toTsvList: List[String] = this.synchronized {
      super.toTsvList :+ sentNb.toString
    }
  }
  protected final class ParAAMStatsEnabled extends ParAAMStats with ParAAMStatsEnabledTrait



  type LocalResults = (List[State], List[(State, Set[State])])

  type CollectedLocalResults = (List[State], Option[Graph[State]])


  /**
    * Sends message to each actor
    * to ask halted and edges collected locally.
    * Builds and returns complete halted and graph results.
    */
  def collectLocalResults(actors: Array[ActorRef], message: Int, timeout: FiniteDuration,
                          computeGraph: Boolean,
                          selfActor: ActorRef=null, selfLocal: LocalResults=(Nil, Nil)):
      CollectedLocalResults = {
    implicit val _ = akka.util.Timeout(timeout)

    var halted: List[State] = selfLocal._1
    var graph: Graph[State] = if (computeGraph) Graph.empty[State] else null

    if (computeGraph) {
      for ((state, successors) <- selfLocal._2.reverse)
        graph = graph.addEdges(state, successors)
    }

    for (actor <- actors if actor != selfActor) {  // collects local results and builds complete results
      val future: Future[LocalResults] = (actor ? message).mapTo[LocalResults]
      val (localHalted, localEdges): LocalResults = Await.result(future, timeout)

      halted :::= localHalted

      if (computeGraph) {
        for ((state, successors) <- localEdges.reverse)
          graph = graph.addEdges(state, successors)
      }
    }

    (halted, if (computeGraph) Some(graph) else None)
  }



  /**
    * Performs the evaluation of an expression as [[SeqAAM.eval]].
    * It is the main work that is parallelized.
    */
  override
  def eval(exp: Exp, sem: Semantics[Exp, Abs, Addr, Time], config: Configuration.MachineConfig): Output = {
    require(config.process >= minProcess)

    val computeGraph: Boolean = config.outputGraphEnabled
    val timeout: Timeout = Timeout.start(config.timeout)

    import akka.actor.{Actor, ActorSystem, Props}

    implicit val _ = akka.util.Timeout(config.timeout.toNanos nanoseconds)
    // implicit val _ = akka.util.Timeout(10 second)  // for debug


    // Set the evaluation step function, with filtering enabled or not
    def step(state: State, sem: Semantics[Exp, Abs, Addr, Time], visited: Set[State]): Set[State] =
      if (config.stepFilterEnabled) state.stepFilter(sem, visited) else state.step(sem)


    // Starts with the initial state resulting from injecting the program (only modified/used in ActorSenderAggregator and used in main loop)
    var worklist: List[Set[State]] = List(Set(State.inject(exp, sem.initialEnv, sem.initialStore)))

    // Set of states already visited (only modified/used in ActorSenderAggregator.Start and used in ActorEval.Eval)
    var visited: Set[State] = Set.empty[State]

    val stats: ParAAMStats =
      if (config.statsEnabled) new ParAAMStatsEnabled
      else new ParAAMStatsDisabled


    val maxActorEval: Int = config.process - nbNotEvalProcess

    assert(maxActorEval > 0)

    val actorSystem: ActorSystem = ActorSystem("Akka-System-ParAAMLSAState-eval")
    val actorEvals: Array[ActorRef] = Array.ofDim[ActorRef](maxActorEval)


    //
    // Definition and instantiation of the 2 types of actors
    //

    final object ActorSenderAggregator {
      val ResultNoSuccessors: Int = 1
      final case class ResultSuccessors(successors: Set[State])
      val Start: Int = 2
    }

    final object ActorEval {
      final case class Eval(state: State, currentVisited: Set[State])
      val ReturnLocalResults: Int = 11
    }

    // Actor that collects all results of evaluated states.
    // Starts by send all states (one by one) to actorsEvals.
    final class ActorSenderAggregator extends Actor {
      import ActorSenderAggregator.{ResultNoSuccessors, ResultSuccessors, Start}

      // True iff all state of the current worklist are sent
      var isAllStateSent: Boolean = false

      // Number of results waited for the current loop
      var nbResultWaited: Int = 0

      // Reference to main thread that wait result
      var mainCaller: ActorRef = null

      def finishIfAllCollected(): Unit =
        if (isAllStateSent && (nbResultWaited == 0))  // this loop is finished
          mainCaller ! Nil  // just say finished

      override
      def receive = {
        case ResultSuccessors(successors: Set[State]) =>  // updates successors and returns if finished
          if (successors.nonEmpty) worklist ::= successors

          nbResultWaited -= 1
          finishIfAllCollected

        case ResultNoSuccessors =>  // returns if finished
          nbResultWaited -= 1
          finishIfAllCollected

        case Start =>  // initializes and sends all states
          isAllStateSent = false
          nbResultWaited = 0
          mainCaller = sender

          // Sends all states (state by state) from current worklist to actorEvals
          var i: Int = 0
          for (states: Set[State] <- worklist;
               state: State <- states) {
            stats.pickStates(1)

            stats.incSentNb
            actorEvals(i % maxActorEval) ! ActorEval.Eval(state, visited)
            visited += state
            nbResultWaited += 1
            i += 1
          }

          worklist = Nil  // a new empty worklist

          isAllStateSent = true

        // case x => p(s"!!! SenderAggregator $x")  // for debug
      }
    }

    val actorSenderAggregator: ActorRef = actorSystem.actorOf(Props(new ActorSenderAggregator), "ActorSenderAggregator")


    // Actor to evaluate state
    final class ActorEval extends Actor {
      import ActorEval.{Eval, ReturnLocalResults}

      var localHalted: List[State] = Nil
      var localEdges: List[(State, Set[State])] = Nil

      override
      def receive = {  // evaluates state and sends results
        case Eval(state: State, currentVisited: Set[State]) =>
          if (!currentVisited.contains(state)) {  // state not yet visited
            if (state.halted) {  // halted state
              actorSenderAggregator ! ActorSenderAggregator.ResultNoSuccessors
              localHalted ::= state
            }
            else {               // new state with successors
              val successors: Set[State] = step(state, sem, visited)
              actorSenderAggregator ! ActorSenderAggregator.ResultSuccessors(successors)
              stats.newStates(successors.size)
              if (computeGraph)
                localEdges ::= (state, successors)
            }
          }
          else {                                  // already visited
            stats.incAlreadyVisitedNb
            actorSenderAggregator ! ActorSenderAggregator.ResultNoSuccessors
          }

        case ReturnLocalResults => sender ! (localHalted, localEdges)

        // case x => p(s"!!! Eval $x")  // for debug
      }
    }

    for (i <- 0 until maxActorEval)  // create all ActorEval
      actorEvals(i) = actorSystem.actorOf(Props(new ActorEval), s"ActorEval-$i")


    //
    // Main loop
    //

    val (timedOut: Boolean, (halted: List[State], graph: Option[Graph[State]])) =
      try {    // run during available time
        while (worklist.nonEmpty) {  // remains state in worklist
          stats.incLoopIterNb

          val future = actorSenderAggregator ? ActorSenderAggregator.Start

          Await.ready(future, config.timeout)
        }
        val results: CollectedLocalResults =
          collectLocalResults(actorEvals, ActorEval.ReturnLocalResults, config.timeout,
                              computeGraph)

        (false, results)
      }
      catch {  // timed out
        case _: scala.concurrent.TimeoutException => (true, (Nil, None))
      }

    actorSystem.terminate

    val haltedSet: Set[State] = halted.toSet

    // Return result
    SeqAAMOutput(
      haltedSet,
      visited.size,
      timeout.time,
      graph,
      timedOut,
      stats)
  }
}
