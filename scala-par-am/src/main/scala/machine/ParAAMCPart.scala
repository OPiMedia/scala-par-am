package be.opimedia.scala_par_am

/**
  * ParAAMCPart: ParAAM-C-set for Parallel AAM - Concurrent - set
  *
  * Similar to [[ParAAMCState]]
  * but sends equal part instead state, and there is NO global worklist.
  *
  * The local worklist (named newStates) is implemented by an immutable list of set of states.
  *
  * The main method eval() just send the initial state to the first ActorEval.
  *
  * Each actor ActorEval evaluates a set of states, updates data,
  * and sends equal part of its local worklist to each actor ActorEval available,
  * until there is no more state to evaluate.
  *
  * <strong>Source</strong> [[https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/machine/ParAAMCPart.scala]]
  */
class ParAAMCPart[Exp : Expression, Abs : JoinLattice, Addr : Address, Time : Timestamp]
    extends ParAAMCState[Exp, Abs, Addr, Time] {
  override def name: String = "ParAAM-C-part"

  /** Print function use during development and debug */
  private def p(s: String): Unit = synchronized {
    if (false) {
      System.err.println(s)
      System.err.flush
    }
  }
  p(s"DEBUG p $name!")



  /**
    * Performs the evaluation of an expression as [[SeqAAM.eval]].
    * It is the main work that is parallelized.
    */
  override
  def eval(exp: Exp, sem: Semantics[Exp, Abs, Addr, Time], config: Configuration.MachineConfig): Output = {
    require(config.process >= minProcess)

    val computeGraph: Boolean = config.outputGraphEnabled
    val timeout: Timeout = Timeout.start(config.timeout)

    import akka.actor.{Actor, ActorRef, ActorSystem, Props}
    import akka.pattern.ask
    import scala.concurrent.{Await, Future}
    import scala.concurrent.duration._
    import scala.language.postfixOps

    implicit val _ = akka.util.Timeout(config.timeout.toNanos nanoseconds)
    // implicit val _ = akka.util.Timeout(10 second)  // for debug


    // Set the evaluation step function, with filtering enabled or not
    def step(state: State, sem: Semantics[Exp, Abs, Addr, Time], visited: Set[State]): Set[State] =
      if (config.stepFilterEnabled) state.stepFilter(sem, visited) else state.step(sem)


    // Set of states already visited
    var visited: Set[State] = Set.empty[State]

    val stats: ParAAMStats =
      if (config.statsEnabled) new ParAAMStatsEnabled
      else new ParAAMStatsDisabled

    // List of all available ActorEval
    var availableActorEvals: List[ActorRef] = null
    var availableActorEvalsSize: Int = 0


    // Lock used to modify visited
    val visitedLock: AnyRef = AnyRef

    // Lock used to access and modify availableActorEvals
    val availableActorEvalsLock: AnyRef = AnyRef


    val maxActorEval: Int = config.process - nbNotEvalProcess

    assert(maxActorEval > 0)

    val actorSystem: ActorSystem = ActorSystem("Akka-System-ParAAMCPart-eval")
    val actorEvals: Array[ActorRef] = Array.ofDim[ActorRef](maxActorEval)


    // Reference to main thread that wait result
    var mainCaller: ActorRef = null


    //
    // Definition and instantiation of one type of actors
    //

    final object ActorEval {
      final case class Eval(states: Iterable[State])
      val ReturnLocalResults: Int = 1
      val Start: Int = 2
    }

    // Actor to evaluate state
    final class ActorEval extends Actor {
      import ActorEval.{Eval, ReturnLocalResults, Start}

      var localHalted: List[State] = Nil
      var localEdges: List[(State, Set[State])] = Nil


      /**
        * Check if all work is finished,
        * and then sends local results to the main caller.
        *
        * Warning! Must be call in availableActorEvalsLock.synchronized {}.
        */
      def checkAndReturns(): Unit = {
        if (availableActorEvalsSize == maxActorEval) {  // finished
          val results: CollectedLocalResults =
            collectLocalResults(actorEvals, ActorEval.ReturnLocalResults, config.timeout,
                                computeGraph,
                                self, (localHalted, localEdges))

          mainCaller ! results  // returns to main caller
        }
      }


      override
      def receive = {
        case Eval(states: Iterable[State]) =>  // evaluates states, updates, and sends other states available
          var currentStates: Iterable[State] = states
          var loop: Boolean = false

          do {
            var newStates: List[Iterable[State]] = Nil
            var newStatesSize: Int = 0

            //
            // Evaluates
            //
            for (state: State <- currentStates) {
              if (!visited.contains(state)) {  // for each state probably not yet visited
                visitedLock.synchronized { visited += state }

                if (state.halted)  // halted state
                  localHalted ::= state
                else {             // new state with successors
                  val successors: Set[State] = step(state, sem, visited)
                  if (successors.nonEmpty) {
                    stats.newStates(successors.size)
                    newStates ::= successors
                    newStatesSize += successors.size
                    if (computeGraph)
                      localEdges ::= (state, successors)
                  }
                }
              }
              else                             // already visited
                stats.incAlreadyVisitedNb
            }

            //
            // Updates
            //
            assert(newStates.flatten.size == newStatesSize)

            if (newStatesSize == 0) {  // no state available
              loop = false

              //
              // Only updates available actors
              //
              availableActorEvalsLock.synchronized {
                availableActorEvals ::= self  // makes this actor available
                availableActorEvalsSize += 1

                assert(availableActorEvals.size == availableActorEvalsSize)

                checkAndReturns  // if finished then returns to main caller
              }
            }
            else {                    // at least one state available
              //
              // Distributes available work
              //

              if (newStatesSize == 1) {  // only one state, avoid cutting parts
                loop = true

                // Loop on this actor for evaluate this state
                stats.pickStates(1)
                currentStates = newStates.head
                assert(newStates.tail.isEmpty)
                assert(currentStates.size == 1)
              }
              else {                     // no state or several states
                // Get current list of available actors
                var currentAvailableActorEvals: List[ActorRef] = null
                var currentAvailableActorEvalsSize: Int = 0

                availableActorEvalsLock.synchronized {
                  currentAvailableActorEvals = availableActorEvals
                  availableActorEvals = Nil
                  currentAvailableActorEvalsSize = availableActorEvalsSize
                  availableActorEvalsSize = 0
                }
                assert(currentAvailableActorEvals.size == currentAvailableActorEvalsSize)

                // This actor is NOT in currentAvailableActorEvals
                if (newStatesSize != 0) {  // some states
                  val actorEvalStateIter: ZipRemainsIterator[ActorRef, Iterable[State]] =
                    ZipRemainsIterator(currentAvailableActorEvals,
                      Parts.parts(newStates, newStatesSize, currentAvailableActorEvalsSize + 1))  // + 1 to have (maybe) a part for this actor

                  // Sends all states (by "equal" part of states) to available actorEvals
                  for ((actor: ActorRef, states: Iterable[State]) <- actorEvalStateIter) {
                    stats.pickStates(states.size)
                    stats.incSentNb
                    actor ! ActorEval.Eval(states)
                    currentAvailableActorEvalsSize -= 1
                  }

                  // Keeps remaining actors xor states
                  val (remain1, remain2) = actorEvalStateIter.remains
                  currentAvailableActorEvals = remain1.toList
                  newStates = remain2.toList

                  assert(currentAvailableActorEvals.size == currentAvailableActorEvalsSize)
                  assert(newStates.isEmpty || newStates.size == 1)
                }
                else                       // no states
                  newStates = Nil

                assert(currentAvailableActorEvals.isEmpty || newStates.isEmpty)
                assert(newStates.isEmpty || newStates.size == 1)

                // If available state then loop again this actor on this state
                loop = newStates.nonEmpty
                if (loop) {  // keep all remaining state (must be only 1)
                  assert(currentAvailableActorEvals.isEmpty)
                  currentStates = newStates.head
                  assert(newStates.tail.isEmpty)
                }
                else {       // else make this actor available
                  //
                  // Updates with remaining actors and this actor
                  //
                  availableActorEvalsLock.synchronized {
                    availableActorEvals :::= self :: currentAvailableActorEvals
                    availableActorEvalsSize += currentAvailableActorEvalsSize + 1

                    assert(availableActorEvals.size == availableActorEvalsSize)

                    checkAndReturns  // if finished then returns to main caller
                  }
                }
              }
            }
          } while (loop)
          // finished message sent or at least one ActorEval is "busy" and will check later

        case ReturnLocalResults => sender ! (localHalted, localEdges)

        case Start =>  // initializes and sends initial state
          mainCaller = sender
          availableActorEvals = (1 until maxActorEval).map(actorEvals).toList
          availableActorEvalsSize = maxActorEval - 1

          stats.incSentNb
          actorEvals.head ! ActorEval.Eval(List(State.inject(exp, sem.initialEnv, sem.initialStore)))

        // case x => p(s"!!! Eval $x")  // for debug
      }
    }

    for (i <- 0 until maxActorEval)  // create all ActorEval
      actorEvals(i) = actorSystem.actorOf(Props(new ActorEval), s"ActorEval-$i")


    //
    // Start by sending the initial state
    //
    val (timedOut: Boolean, (halted: List[State], graph: Option[Graph[State]])) =
      try {    // run during available time
        val future: Future[CollectedLocalResults] =
          (actorEvals.head ? ActorEval.Start).mapTo[CollectedLocalResults]

        (false, Await.result(future, config.timeout))
      }
      catch {  // timed out
        case _: scala.concurrent.TimeoutException => (true, (Nil, None))
      }

    actorSystem.terminate

    val haltedSet: Set[State] = halted.toSet

    // Return result
    SeqAAMOutput(
      haltedSet,
      visited.size,
      timeout.time,
      graph,
      timedOut,
      stats)
  }
}
