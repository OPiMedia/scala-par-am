package be.opimedia.scala_par_am

/**
  * SeqAAMLSH: SeqAAMLS with direct identification of Halted states
  *
  * Version of [[SeqAAMLS]] that includes to the worklist only the not halted states.
  * Halted states are directly identified in the set of successors.
  *
  * The worklist is implemented by an immutable list of sets of states.
  *
  * Extend [[SeqAAM]] class to reuse common code.
  *
  * <strong>Source</strong> [[https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/machine/SeqAAMLSH.scala]]
  */
class SeqAAMLSH[Exp : Expression, Abs : JoinLattice, Addr : Address, Time : Timestamp]
    extends SeqAAM[Exp, Abs, Addr, Time] {
  override def name: String = "SeqAAMLS-halt"

  /** Print function use during development and debug */
  private def p(s: String): Unit = synchronized {
    if (false) {
      System.err.println(s)
      System.err.flush
    }
  }
  p(s"DEBUG p $name!")



  /**
    * Performs the evaluation of an expression as [[SeqAAM.eval]].
    */
  override
  def eval(exp: Exp, sem: Semantics[Exp, Abs, Addr, Time], config: Configuration.MachineConfig): Output = {
    val computeGraph: Boolean = config.outputGraphEnabled
    val timeout: Timeout = Timeout.start(config.timeout)

    // Set the evaluation step function, with filtering enabled or not
    def step(state: State, sem: Semantics[Exp, Abs, Addr, Time], visited: Set[State]): Set[State] =
      if (config.stepFilterEnabled) state.stepFilter(sem, visited) else state.step(sem)


    // Starts with the initial state resulting from injecting the program below
    var worklist: List[Set[State]] = Nil

    // Set of states already visited (halted states are added at the end)
    var visited: Set[State] = Set.empty[State]

    // List of sets of final and error states
    var halted: List[Set[State]] = Nil

    // Output graph
    var graph: Graph[State] = Graph.empty[State]

    val stats: SeqAAMStats =
      if (config.statsEnabled) new SeqAAMStatsEnabled
      else new SeqAAMStatsDisabled


    //
    // Main loop
    //

    val initial: State = State.inject(exp, sem.initialEnv, sem.initialStore)

    if (initial.halted) {  // initial state is a final or error state
      halted ::= Set(initial)
      if (computeGraph)
        graph = graph.addNode(initial)
    }
    else {               // main process
      worklist = List(Set(initial))

      scala.util.control.Breaks.breakable {
        while (worklist.nonEmpty) {  // remains states and time available
          val currentWorklist: List[Set[State]] = worklist
          worklist = Nil

          for (states: Set[State] <- currentWorklist;
               state: State <- states) {  // for each state from each set
            stats.incLoopIterNb
            stats.pickStates(1)

            if (!visited.contains(state)) {  // state not yet visited
              visited += state

              val successors: Set[State] = step(state, sem, visited)
              val (newHalted: Set[State], newNotHalted: Set[State]) = successors.partition(_.halted)

              if (newNotHalted.nonEmpty) {
                stats.newStates(newNotHalted.size)
                // Only not halted states are added to the worklist and will be visited after
                worklist ::= newNotHalted
              }

              halted ::= newHalted

              if (computeGraph)
                graph = graph.addEdges(state, successors)
            }
            else                             // already visited
              stats.incAlreadyVisitedNb

            if (timeout.reached) scala.util.control.Breaks.break
          }
        }
      }
    }

    val haltedSet: Set[State] = halted.flatten.toSet
    visited ++= haltedSet

    // Return result
    SeqAAMOutput(
      haltedSet,
      visited.size,
      timeout.time,
      if (computeGraph) Some(graph) else None,
      worklist.nonEmpty,  // timed out
      stats)
  }
}
