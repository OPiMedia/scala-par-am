package be.opimedia.scala_par_am

/**
  * The interface of the abstract machine itself.
  * Abstract class that should be implemented by an abstract machine.
  * Abstract machines that implement that in Scala-Par-AM
  * are AAM*.scala and ParAAM*.scala.
  *
  * The abstract machine is parameterized by abstract values, addresses and expressions.
  * Look into AAM.scala for an example of how to define these parameters.
  *
  * <strong>Source</strong> [[https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/machine/AbstractMachine.scala]]
  */
abstract class AbstractMachine[Exp : Expression, Abs : JoinLattice, Addr : Address, Time : Timestamp] {
  /** The name of the abstract machine. */
  def name: String

  /** Returns true iff the machine is a parallel machine */
  def isParallel: Boolean = false

  /**
    * Returns the minimum processes possible for this machine.
    * Must be >= 1 and must be 1 if !isParallel.
    */
  def minProcess: Int = 1

  /**
    * Returns the number of processes that whose task is not to perform step evaluation.
    * Must be >= 0.
    */
  def nbNotEvalProcess: Int = 0


  /**
    * Additional classes (implementation(s) must be thread safe)
    * to collect some statistic information
    * about the evaluation of the expression.
    *
    * Designed to minimized the cost and do nothing by default
    * to not the disturb the "normal" performance of the machine.
    */
  protected abstract class AbstractMachineStats {
    def incAlreadyVisitedNb(): Unit
    def incLoopIterNb(): Unit
    def newStates(nb: => Int): Unit
    def pickStates(nb: => Int): Unit
    def println(): Unit
    def toString(escaped: => Boolean): String
    def toTsv: String
    protected def toTsvNameList: List[String]
    protected def toTsvList: List[String]
  }

  protected trait AbstractMachineStatsDisabledTrait extends AbstractMachineStats {
    def incAlreadyVisitedNb(): Unit = ()

    def incLoopIterNb(): Unit = ()

    def newStates(nb: => Int): Unit = ()

    def pickStates(nb: => Int): Unit = ()

    def println(): Unit = ()

    override def toString: String = ""

    def toString(escaped: => Boolean): String = ""

    def toTsv: String = ""

    protected def toTsvNameList: List[String] = Nil

    protected def toTsvList: List[String] = Nil
  }

  protected trait AbstractMachineStatsEnabledTrait extends AbstractMachineStats {
    val newStateNbs: collection.mutable.Map[Int, Int] = collection.mutable.Map.empty

    /** Sizes of the worklist on each pick operation, in the reverse order. */
    var workListReverseSizes: List[Int] = Nil

    var alreadyVisitedNb: Int = 0
    var loopIterNb: Int = 0
    var newStateMaxNb: Int = 0
    var workListSize: Int = 1  // initial size
    var workListMaxSize: Int = workListSize
    var workListSumSize: Int = 0

    def incAlreadyVisitedNb(): Unit = this.synchronized { alreadyVisitedNb += 1 }

    def incLoopIterNb(): Unit = this.synchronized {
      loopIterNb += 1
      workListSumSize += workListSize
    }

    def newStates(nb: => Int): Unit = this.synchronized {
      newStateMaxNb = math.max(newStateMaxNb, nb)
      newStateNbs(nb) = newStateNbs.getOrElse(nb, 0) + 1
      workListSize += nb
      workListMaxSize = math.max(workListMaxSize, workListSize)
    }

    def pickStates(nb: => Int): Unit = this.synchronized {
      workListReverseSizes ::= workListSize
      workListSize -= nb
    }

    def println(): Unit = this.synchronized { scala.Predef.println(toString) }

    override def toString: String = this.synchronized { toString(false) }

    def toString(escaped: => Boolean): String = this.synchronized {
      (toTsvNameList zip toTsvList).map({ case (name, value) => s"$name: $value" }).mkString(if (escaped) "\\t" else "\t")
    }

    def toTsv: String = this.synchronized { toTsvList.mkString("\t") }

    protected def toTsvNameList: List[String] = this.synchronized {
      List("# iterations", "Max size", "Sum sizes", "# already", "Max new", "Histo new", "Sizes")
    }

    protected def toTsvList: List[String] = this.synchronized {
      val histo = newStateNbs.toSeq.sortBy(_._1).map({ case (stateNb, nb) => s"$stateNb:$nb" }).mkString(" ")

      List(loopIterNb.toString,
        workListMaxSize.toString, workListSumSize.toString,
        alreadyVisitedNb.toString,
        newStateMaxNb.toString,
        histo,
        workListReverseSizes.reverse.mkString(","))
    }
  }


  /**
    * The output of the abstract machine.
    */
  trait Output {
    /**
      * Returns the number of final states.
      */
    def nbFinalState: Int

    /**
      * Returns the set of final values that can be reached by the abstract machine.
      * Example: the Scheme program (+ 1 2) has as final values the set {3}, in the concrete case.
      * Returns the set as a list, to have a deterministic order.
      */
    def finalValues: List[Abs]

    /**
      * Returns the set number of error states.
      */
    def nbErrorState: Int

    /**
      * Returns the set of error values that can be reached by the abstract machine.
      * Returns the set as a list, to allow a deterministic order (not implemented).
      */
    def errorValues: List[SemanticError]

    /**
      * Returns the stats keep on the evaluation (if it computed).
      */
    def machineStats: AbstractMachineStats

    /**
      * Returns information about the final graph (if it computed).
      */
    def graphInfo: Option[GraphInfo]

    /**
      * Returns the number of states visited to evaluate the program.
      */
    def numberOfStates: Int

    /**
      * Returns the time it took to evaluate the program.
      */
    def time: Double

    /**
      * Does this output comes from a computation that timed out?
      */
    def timedOut: Boolean

    /**
      * Outputs the graph computed by the machine in a file, according to the given output format.
      */
    def toFile(path: String)(config: Option[Configuration.MachineConfig], output: GraphOutput): Unit


    /** If optionReference is Some()
      * then compare this output with it value and return (optionReference, separated semi-colon list of possible errors),
      * else return (this output, "")
      */
    def compareToReference(optionReference: Option[Output]): (Option[Output], String) = {
      optionReference match {
        case Some(reference) =>
          import scala.collection.mutable.ArrayBuffer

          val errors: ArrayBuffer[String] = ArrayBuffer.empty[String]

          if (timedOut)  // evaluation not finished
            errors += "TimedOut"
          else {         // compares to reference result
            // Compare # states
            if (numberOfStates != reference.numberOfStates)
              errors += s"Different # states! $numberOfStates != ${reference.numberOfStates}"

            // Compare # error states
            if (nbErrorState != reference.nbErrorState)
              errors += s"Different # error states! $nbErrorState != ${reference.nbErrorState}"

            // Compare # final states
            if (nbFinalState != reference.nbFinalState)
              errors += s"Different # final states! $nbFinalState != ${reference.nbFinalState}"

            // Compare error values
            val currentErrorValuesString: String = errorValues.mkString(", ")
            val referenceErrorValuesString: String = reference.errorValues.mkString(", ")
            if (currentErrorValuesString != referenceErrorValuesString)
              errors += s"Different error values! $currentErrorValuesString != $referenceErrorValuesString"

            // Compare final values
            val currentFinalValuesString: String =
              finalValues.sortBy(abs => abs.toString).mkString(", ")
            val referenceFinalValuesString: String =
              reference.finalValues.sortBy(abs => abs.toString).mkString(", ")
            if (currentFinalValuesString != referenceFinalValuesString)
              errors += s"Different final values! $currentFinalValuesString != $referenceFinalValuesString"
          }

          (optionReference, errors.mkString("; "))

        case None => (Some(this), if (timedOut) "TimedOut" else "")
      }
    }

    /** Returns a TSV representation of the stats, used to print results */
    def statsToTsv(escaped: Boolean): String = machineStats.toString(escaped)

    /** Returns a TSV representation, used to print results */
    def toTsv(includeValues: Boolean, afterValues: List[String]): String =
      ((if (includeValues)
        List(numberOfStates.toString,
             nbErrorState.toString,
             errorValues.size.toString, // errorValues.mkString(", "),
             nbFinalState.toString,
             finalValues.size.toString, finalValues.mkString(", "))
        ++ afterValues
      else Nil) ++ List((!timedOut).toString, time.toString)).mkString("\t")
  }

  /**
    * Evaluates a program, given a semantics.
    *
    * @param exp The program to be evaluate
    * @param config Specify some parameters of the machine
    *
    * @return An object implementing the Output trait, containing information about the evaluation.
    */
  def eval(exp: Exp, sem: Semantics[Exp, Abs, Addr, Time], config: Configuration.MachineConfig): Output
}



/**
  * Abstract machine with a control component that works in an eval-kont way:
  * it can either be evaluating something, or have reached a value and will pop a continuation.
  */
abstract class EvalKontMachine[Exp : Expression, Abs : JoinLattice, Addr : Address, Time : Timestamp]
    extends AbstractMachine[Exp, Abs, Addr, Time] {

  /**
    * The control component of the machine
    */
  trait Control extends MemoHashCode {
    def subsumes(that: Control): Boolean
  }

  /**
    * It can either be an eval component, where an expression needs to be
    * evaluated in an environment
    */
  case class ControlEval(exp: Exp, env: Environment[Addr]) extends Control {
    override def toString = s"ev(${exp})"

    def subsumes(that: Control) = that match {
      case ControlEval(exp2, env2) => exp.equals(exp2) && env.subsumes(env2)
      case _ => false
    }
  }

  /**
    * Or it can be a continuation component, where a value has been reached and a
    * continuation should be popped from the stack to continue the evaluation
    */
  case class ControlKont(v: Abs) extends Control {
    override def toString = s"ko(${v})"

    def subsumes(that: Control) = that match {
      case ControlKont(v2) => JoinLattice[Abs].subsumes(v, v2)
      case _ => false
    }
  }

  /**
    * Or an error component, in case an error is reached (e.g., incorrect number
    * of arguments in a function call)
    */
  case class ControlError(err: SemanticError) extends Control {
    override def toString = s"err($err)"
    def subsumes(that: Control) = that.equals(this)
  }
}
