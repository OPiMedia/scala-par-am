package be.opimedia.scala_par_am

/**
  * SeqAAMLS: Sequential AAM with List of Sets
  *
  * Version of SeqAAM with a different worklist implementation.
  *
  * The worklist is implemented by an immutable list of sets of states.
  *
  * Extend [[SeqAAM]] class to reuse common code.
  *
  * <strong>Source</strong> [[https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/machine/SeqAAMLS.scala]]
  */
class SeqAAMLS[Exp : Expression, Abs : JoinLattice, Addr : Address, Time : Timestamp]
    extends SeqAAM[Exp, Abs, Addr, Time] {
  override def name: String = "SeqAAMLS"

  /** Print function use during development and debug */
  private def p(s: String): Unit = synchronized {
    if (false) {
      System.err.println(s)
      System.err.flush
    }
  }
  p(s"DEBUG p $name!")



  /**
    * Performs the evaluation of an expression as [[SeqAAM.eval]].
    */
  override
  def eval(exp: Exp, sem: Semantics[Exp, Abs, Addr, Time], config: Configuration.MachineConfig): Output = {
    val computeGraph: Boolean = config.outputGraphEnabled
    val timeout: Timeout = Timeout.start(config.timeout)

    // Set the evaluation step function, with filtering enabled or not
    def step(state: State, sem: Semantics[Exp, Abs, Addr, Time], visited: Set[State]): Set[State] =
      if (config.stepFilterEnabled) state.stepFilter(sem, visited) else state.step(sem)


    // Starts with the initial state resulting from injecting the program
    var worklist: List[Set[State]] = List(Set(State.inject(exp, sem.initialEnv, sem.initialStore)))

    // Set of states already visited
    var visited: Set[State] = Set.empty[State]

    // List of final and error states
    var halted: List[State] = Nil

    // Output graph
    var graph: Graph[State] = Graph.empty[State]

    val stats: SeqAAMStats =
      if (config.statsEnabled) new SeqAAMStatsEnabled
      else new SeqAAMStatsDisabled


    //
    // Main loop
    //

    scala.util.control.Breaks.breakable {
      while (worklist.nonEmpty) {  // remains states and time available
        val currentWorklist: List[Set[State]] = worklist
        worklist = Nil

        for (states: Set[State] <- currentWorklist;
             state: State <- states) {  // for each state from each set
          stats.incLoopIterNb
          stats.pickStates(1)

          if (!visited.contains(state)) {  // state not yet visited
            visited += state

            if (state.halted)  // final or error state, add its value to set
              halted ::= state
            else {             // add successors of state to worklist and update graph
              val successors: Set[State] = step(state, sem, visited)
              if (successors.nonEmpty) {
                stats.newStates(successors.size)
                worklist ::= successors
                if (computeGraph)
                  graph = graph.addEdges(state, successors)
              }
            }
          }
          else                             // already visited
            stats.incAlreadyVisitedNb

          if (timeout.reached) scala.util.control.Breaks.break
        }
      }
    }

    val haltedSet: Set[State] = halted.toSet

    // Return result
    SeqAAMOutput(
      haltedSet,
      visited.size,
      timeout.time,
      if (computeGraph) Some(graph) else None,
      worklist.nonEmpty,  // timed out
      stats)
  }
}
