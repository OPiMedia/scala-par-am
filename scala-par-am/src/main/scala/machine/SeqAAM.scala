package be.opimedia.scala_par_am

/**
  * SeqAAM: Sequential AAM (with list)
  *
  * This is a reimplementation of the AAM class of Scala-AM written by Quentin Stiévenart.
  * A fork of the original version of Scala-AM used is available here:
  * [[https://bitbucket.org/OPiMedia/scala-am]].
  * The subsumption part has been removed.
  * So this class corresponds in fact to the AAMNS (AAM No Subsumption) class of Scala-AM
  * in this copy repository.
  *
  * Compared to AAMNS of Scala-AM
  * this class used Set Scala native data structures
  * instead to specific WorkList and VisitedSet.
  * The recursive loop has been replaced by a for loop.
  *
  * The worklist is implemented by an immutable list of states.
  *
  * This sequential machine (or the SeqAAMS and SeqAAMLS alternative) is the comparison point
  * for the different version of parallel machines implemented in Scala-Par-AM.
  *
  *
  * The original description by Quentin Stiévenart:
  * Implementation of a CESK machine following the AAM approach
  * (Van Horn, David, and Matthew Might. "Abstracting abstract machines."
  * ACM Sigplan Notices. Vol. 45. No. 9. ACM, 2010).
  *
  * A difference with the paper is that we separate the continuation store
  * (KontStore) from the value store (Store). That simplifies the implementation
  * of both stores, and the only change it induces is that we are not able to
  * support first-class continuation as easily (we don't support them at all, but
  * they could be added).
  *
  * Also, in the paper, a CESK state is made of 4 components:
  * Control, Environment, Store, and Kontinuation.
  * Here, we include the environment in the control component,
  * and we distinguish "eval" states from "continuation" states.
  * An eval state has an attached environment,
  * as an expression needs to be evaluated within this environment,
  * whereas a continuation state only contains the value reached.
  *
  * <strong>Source</strong> [[https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/machine/SeqAAM.scala]]
  */
class SeqAAM[Exp : Expression, Abs : JoinLattice, Addr : Address, Time : Timestamp]
    extends EvalKontMachine[Exp, Abs, Addr, Time] {
  def name: String = "SeqAAM"

  /** Print function use during development and debug */
  private def p(s: String): Unit = synchronized {
    if (false) {
      System.err.println(s)
      System.err.flush
    }
  }
  p(s"DEBUG p $name!")



  /**
    * The store used for continuations is a KontStore (defined in
    * Kontinuation.scala). It is parameterized by continuation addresses, that
    * are element of the KontAddress typeclass.
    */
  trait KontAddr extends MemoHashCode

  private case class NormalKontAddress(exp: Exp, time: Time) extends KontAddr {
    override def toString = s"NormalKontAddress($exp)"
  }

  private case object HaltKontAddress extends KontAddr {
    override def toString = "HaltKontAddress"
  }

  private object KontAddr {
    implicit object KontAddrKontAddress extends KontAddress[KontAddr]
  }

  /**
    * A machine state is made of a control component, a value store, a continuation store,
    * and an address representing where the current continuation lives.
    */
  protected case class State(control: Control, store: Store[Addr, Abs], kstore: KontStore[KontAddr], a: KontAddr, t: Time) {
    // Explicit memoization of the hash code instead used of the trait MemoHashCode
    // to allow disabled MemoHashCode without disabled this memoization.
    private lazy val memoizedHashCode: Int = scala.runtime.ScalaRunTime._hashCode(this)
    override def hashCode: Int = memoizedHashCode


    override def toString: String = control.toString
    // override def toString: String = s"$control\t$store\t $kstore\t $a"  // for debug


    /**
      * Integrates a action (returned by the semantics, see Semantics.scala),
      * in order to generate a state that succeeds this one.
      */
    private def integrate(a: KontAddr, action: Action[Exp, Abs, Addr]): State =
      action match {
        // When a value is reached, we go to a continuation state
        case ActionReachedValue(v, store, _) =>
          State(ControlKont(v), store, kstore, a, Timestamp[Time].tick(t))

        // When a continuation needs to be pushed, push it in the continuation store
        case ActionPush(frame, e, env, store, _) =>
          val next = NormalKontAddress(e, t)
          State(ControlEval(e, env), store, kstore.extend(next, Kont(frame, a)), next, Timestamp[Time].tick(t))

        // When a value needs to be evaluated, we go to an eval state
        case ActionEval(e, env, store, _) =>
          State(ControlEval(e, env), store, kstore, a, Timestamp[Time].tick(t))

        // When a function is stepped in, we also go to an eval state
        case ActionStepIn(fexp, _, e, env, store, _, _) =>
          State(ControlEval(e, env), store, kstore, a, Timestamp[Time].tick(t, fexp))

        // When an error is reached, we go to an error state
        case ActionError(err) =>
          State(ControlError(err), store, kstore, a, Timestamp[Time].tick(t))
      }


    import scala.collection.IterableView

    /**
      * Integrates a set of actions (returned by the semantics, see Semantics.scala),
      * in order to generate a set of states that succeeds this one.
      */
    private def integrate(a: KontAddr, actions: Set[Action[Exp, Abs, Addr]]): IterableView[State, Set[State]] =
      actions.map(integrate(a, _)).view

    /**
      * Result of integrate(a, actions) - visited
      * This alternative step function can be improved speed,
      * but be careful because in general that causes missing some edges in the output graph.
      */
    private def integrateFilter(a: KontAddr, actions: Set[Action[Exp, Abs, Addr]],
                                visited: Set[State]): IterableView[State, Set[State]] =
      integrate(a, actions).filterNot(visited.contains)


    /**
      * Computes the set of states that follow the current state
      */
    def step(sem: Semantics[Exp, Abs, Addr, Time]): Set[State] =
      control match {
        // In a eval state, call the semantic's evaluation method
        case ControlEval(e, env) => integrate(a, sem.stepEval(e, env, store, t)).force

        // In a continuation state, call the semantic's continuation method
        case ControlKont(v) => kstore.lookup(a).flatMap({
          case Kont(frame, next) => integrate(next, sem.stepKont(v, frame, store, t))
        })

        // In an error state, the state is not able to make a step
        case ControlError(_) => Set.empty
      }

    /**
      * Result of step(sem) - visited
      */
    def stepFilter(sem: Semantics[Exp, Abs, Addr, Time], visited: Set[State]): Set[State] =
      control match {
        // In a eval state, call the semantic's evaluation method
        case ControlEval(e, env) => integrateFilter(a, sem.stepEval(e, env, store, t), visited).force

        // In a continuation state, call the semantic's continuation method
        case ControlKont(v) => kstore.lookup(a).flatMap({
          case Kont(frame, next) => integrateFilter(next, sem.stepKont(v, frame, store, t), visited)
        })

        // In an error state, the state is not able to make a step
        case ControlError(_) => Set.empty
      }


    /**
      * Checks if the current state is a final state.
      * It is the case if it reached the end of the computation, or an error.
      */
    def halted: Boolean = control match {
      case ControlEval(_, _) => false
      case ControlKont(_) => a == HaltKontAddress
      case ControlError(_) => true
    }
  }
  protected object State {
    def inject(exp: Exp, env: Iterable[(String, Addr)], store: Iterable[(Addr, Abs)]) =
      State(ControlEval(exp, Environment.initial[Addr](env)),
        Store.initial[Addr, Abs](store), KontStore.empty[KontAddr], HaltKontAddress, Timestamp[Time].initial(""))

    implicit val _ = new GraphOutputNode[State] {
      override
      def color(state: State): Color =
        state.control match {
          case ControlEval(_, _) => Colors.StateEval
          case ControlKont(_) => if (state.halted) Colors.Halted else Colors.StateKont
          case ControlError(_) => Colors.Error
        }

      override
      def label(state: State): String =
        state.control match {
          case ControlEval(e, _) => e.toString
          case _ => state.toString
        }
    }
  }


  /**
    * To collect statistic information about the evaluation of the expression.
    */
  protected abstract class SeqAAMStats extends AbstractMachineStats

  protected final class SeqAAMStatsDisabled extends SeqAAMStats with AbstractMachineStatsDisabledTrait

  protected final class SeqAAMStatsEnabled extends SeqAAMStats with AbstractMachineStatsEnabledTrait


  /**
    * The output of the abstract machine.
    */
  protected case class SeqAAMOutput(halted: Set[State], numberOfStates: Int, time: Double, graphOption: Option[Graph[State]], timedOut: Boolean, stats: AbstractMachineStats)
      extends Output {

    /** Return number of error states */
    def nbErrorState: Int = halted.collect(state => state.control match { case ControlError(_) => state }).size

    /** Return error values (ordered by string representation) */
    def errorValues: List[SemanticError] = halted.collect(state => state.control match { case ControlError(err) => err }).toList.sortBy(err => err.toString)

    /** Return number of final states */
    def nbFinalState: Int = halted.collect(state => state.control match { case ControlKont(_) => state }).size

    /**
      * Return final values in "forced" total order
      * (to have a deterministic return "corresponding" to the real partial order)
      */
    def finalValues: List[Abs] = halted.collect(state => state.control match { case ControlKont(v) => v }).toList.sortWith(JoinLattice[Abs].totalLessThan)

    def graphInfo: Option[GraphInfo] = graphOption match {
      case Some(graph) => Some(graph.info)
      case None => None
    }

    def machineStats: AbstractMachineStats = stats

    def toFile(path: String)(config: Option[Configuration.MachineConfig], output: GraphOutput): Unit =
      graphOption match {
        case Some(graph) => output.toFile(config, graph)(path)
        case None => println("Not generating graph because no graph was computed")
      }
  }



  /**
    * Performs the evaluation of an expression exp (more generally, a program)
    * under the given semantics sem.
    *
    * If config.dotfile.nonEmpty is true,
    * it will compute and generate the graph corresponding to the execution of the program
    * (otherwise it will just visit every reachable state).
    *
    * A timeout is also given by config.
    */
  def eval(exp: Exp, sem: Semantics[Exp, Abs, Addr, Time], config: Configuration.MachineConfig): Output = {
    val computeGraph: Boolean = config.outputGraphEnabled
    val timeout: Timeout = Timeout.start(config.timeout)

    // Set the evaluation step function, with filtering enabled or not
    def step(state: State, sem: Semantics[Exp, Abs, Addr, Time], visited: Set[State]): Set[State] =
      if (config.stepFilterEnabled) state.stepFilter(sem, visited) else state.step(sem)


    // Starts with the initial state resulting from injecting the program
    var worklist: List[State] = List(State.inject(exp, sem.initialEnv, sem.initialStore))

    // Set of states already visited
    var visited: Set[State] = Set.empty[State]

    // List of final and error states
    var halted: List[State] = Nil

    // Output graph
    var graph: Graph[State] = Graph.empty[State]

    val stats: SeqAAMStats =
      if (config.statsEnabled) new SeqAAMStatsEnabled
      else new SeqAAMStatsDisabled


    //
    // Main loop
    //

    scala.util.control.Breaks.breakable {
      while (worklist.nonEmpty) {  // remains states and time available
        val currentWorklist: List[State] = worklist
        worklist = Nil

        for (state: State <- currentWorklist) {  // for each state from the list
          stats.incLoopIterNb
          stats.pickStates(1)

          if (!visited.contains(state)) {  // state not yet visited
            visited += state

            if (state.halted)  // final or error state, add its value to set
              halted ::= state
            else {             // add successors of state to worklist and update graph
              val successors: Set[State] = step(state, sem, visited)
              if (successors.nonEmpty) {
                stats.newStates(successors.size)
                worklist :::= successors.toList  // seems better than ++:= successors
                if (computeGraph)
                  graph = graph.addEdges(state, successors)
              }
            }
          }
          else                             // already visited
            stats.incAlreadyVisitedNb

          if (timeout.reached) scala.util.control.Breaks.break
        }
      }
    }

    val haltedSet: Set[State] = halted.toSet

    // Return result
    SeqAAMOutput(
      haltedSet,
      visited.size,
      timeout.time,
      if (computeGraph) Some(graph) else None,
      worklist.nonEmpty,  // timed out
      stats)
  }
}
