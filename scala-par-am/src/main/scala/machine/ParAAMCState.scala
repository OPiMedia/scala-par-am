package be.opimedia.scala_par_am

/**
  * ParAAMCState: ParAAM-C-state for Parallel AAM - Concurrent - state
  *
  * The worklist is implemented by an immutable list of sets of states.
  *
  * The main method eval() just sends the initial state to the first ActorEval.
  *
  * Each actor ActorEval evaluates a state, updates data,
  * and sends one state of the worklist to each actor ActorEval available,
  * until to the worklist becomes empty.
  *
  * Extend [[ParAAMCSState]] class to reuse common code.
  *
  * <strong>Source</strong> [[https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/machine/ParAAMCState.scala]]
  */
class ParAAMCState[Exp : Expression, Abs : JoinLattice, Addr : Address, Time : Timestamp]
    extends ParAAMCSState[Exp, Abs, Addr, Time] {
  override def name: String = "ParAAM-C-state"

  override def minProcess: Int = 1  // at least 1 ActorEval
  override def nbNotEvalProcess: Int = 0  // no specific actor

  /** Print function use during development and debug */
  private def p(s: String): Unit = synchronized {
    if (false) {
      System.err.println(s)
      System.err.flush
    }
  }
  p(s"DEBUG p $name!")



  /**
    * Return the first item and the remain of list.
    *
    * list must be not empty
    * and list.head must be not empty
    */
  def extractHead(list: List[Iterable[State]]): (State, List[Iterable[State]]) = {
    assert(list.nonEmpty)
    assert(list.head.nonEmpty)

    val tail: List[Iterable[State]] = list.tail
    val head: List[State] = list.head.toList
    val states: List[State] = head.tail
    (head.head, if (states.isEmpty) tail else states::tail)
  }



  /**
    * Performs the evaluation of an expression as [[SeqAAM.eval]].
    * It is the main work that is parallelized.
    */
  override
  def eval(exp: Exp, sem: Semantics[Exp, Abs, Addr, Time], config: Configuration.MachineConfig): Output = {
    require(config.process >= minProcess)

    val computeGraph: Boolean = config.outputGraphEnabled
    val timeout: Timeout = Timeout.start(config.timeout)

    import akka.actor.{Actor, ActorRef, ActorSystem, Props}
    import akka.pattern.ask
    import scala.concurrent.{Await, Future}
    import scala.concurrent.duration._
    import scala.language.postfixOps

    implicit val _ = akka.util.Timeout(config.timeout.toNanos nanoseconds)
    // implicit val _ = akka.util.Timeout(10 second)  // for debug


    // Set the evaluation step function, with filtering enabled or not
    def step(state: State, sem: Semantics[Exp, Abs, Addr, Time], visited: Set[State]): Set[State] =
      if (config.stepFilterEnabled) state.stepFilter(sem, visited) else state.step(sem)


    // Starts with the initial state resulting from injecting the program below
    var worklist: List[Iterable[State]] = Nil

    // Set of states already visited
    var visited: Set[State] = Set.empty[State]

    val stats: ParAAMStats =
      if (config.statsEnabled) new ParAAMStatsEnabled
      else new ParAAMStatsDisabled

    // List of all available ActorEval
    var availableActorEvals: List[ActorRef] = null
    var availableActorEvalsSize: Int = 0


    // Lock used to access and modify worklist and availableActorEvals together
    val worklistAvailableActorEvalsLock: AnyRef = AnyRef

    // Lock used to modify visited
    val visitedLock: AnyRef = AnyRef


    val maxActorEval: Int = config.process - nbNotEvalProcess

    assert(maxActorEval > 0)

    val actorSystem: ActorSystem = ActorSystem("Akka-System-ParAAMCState-eval")
    val actorEvals: Array[ActorRef] = Array.ofDim[ActorRef](maxActorEval)


    // Reference to main thread that wait result
    var mainCaller: ActorRef = null


    //
    // Definition and instantiation of one type of actors
    //

    final object ActorEval {
      final case class Eval(state: State)
      val ReturnLocalResults: Int = 1
      val Start: Int = 2
    }

    // Actor to evaluate state
    final class ActorEval extends Actor {
      import ActorEval.{Eval, ReturnLocalResults, Start}

      var localHalted: List[State] = Nil
      var localEdges: List[(State, Set[State])] = Nil


      /**
        * Check if all work is finished,
        * and then sends local results to the main caller.
        *
        * Warning! Must be call in worklistAvailableActorEvalsLock.synchronized {}.
        */
      def checkAndReturns(): Unit = {
        if ((availableActorEvalsSize == maxActorEval) && worklist.isEmpty) {  // finished
          val results: CollectedLocalResults =
            collectLocalResults(actorEvals, ActorEval.ReturnLocalResults, config.timeout,
                                computeGraph,
                                self, (localHalted, localEdges))

          mainCaller ! results  // returns to main caller
        }
      }


      override
      def receive = {
        case Eval(state: State) =>  // evaluates state (and maybe next), updates, and sends other states available
          var currentState: State = state
          var loop: Boolean = false

          do {
            var successors: Set[State] = Set.empty[State]

            //
            // Evaluates
            //
            if (!visited.contains(currentState)) {  // state probably not yet visited
              visitedLock.synchronized { visited += currentState }

              if (currentState.halted)  // halted state
                localHalted ::= currentState
              else {                    // new state with successors
                successors = step(currentState, sem, visited)
                if (successors.nonEmpty) {
                  stats.newStates(successors.size)
                  // these successors will be send directly or added to worklist below
                  if (computeGraph)
                    localEdges ::= (currentState, successors)
                }
              }
            }
            else                                    // already visited
              stats.incAlreadyVisitedNb

            //
            // Updates
            //

            if (successors.isEmpty && worklist.isEmpty) {  // no state available
              loop = false

              //
              // Only updates available actors
              //
              worklistAvailableActorEvalsLock.synchronized {
                // At this point maybe some states are available
                // because maybe other actor updated the worklist

                availableActorEvals ::= self  // makes this actor available
                availableActorEvalsSize += 1

                assert(availableActorEvals.size == availableActorEvalsSize)

                checkAndReturns  // if finished then returns to main caller
              }
            }
            else {                                         // at least one state available
              //
              // Distributes available work
              //

              // Gets current list of other available actors and worklist
              var currentAvailableActorEvals: List[ActorRef] = Nil
              var currentAvailableActorEvalsSize: Int = 0
              var currentWorklist: List[Iterable[State]] = Nil

              worklistAvailableActorEvalsLock.synchronized {
                // At this point maybe available states are no longer available
                // because maybe other actor get them from worklist

                currentAvailableActorEvals = availableActorEvals
                availableActorEvals = Nil
                currentAvailableActorEvalsSize = availableActorEvalsSize
                availableActorEvalsSize = 0

                currentWorklist = worklist
                worklist = Nil
              }

              assert(currentAvailableActorEvals.size == currentAvailableActorEvalsSize)

              if (successors.nonEmpty)
                currentWorklist ::= successors

              if (currentAvailableActorEvals.nonEmpty && currentWorklist.nonEmpty) {  // some actorEvals and states are available
                val actorEvalStateIter: ZipRemainsIterator2[ActorRef, State] =
                  ZipRemainsIterator2(currentAvailableActorEvals, currentWorklist)

                // Sends available states to available actorEvals
                for ((actorEval: ActorRef, state: State) <- actorEvalStateIter) {
                  stats.pickStates(1)
                  stats.incSentNb
                  actorEval ! ActorEval.Eval(state)
                  currentAvailableActorEvalsSize -= 1
                }

                // Keeps remaining actors xor states
                val (remain1, remain2) = actorEvalStateIter.remains
                currentAvailableActorEvals = remain1.toList
                currentWorklist = remain2.toList

                assert(currentAvailableActorEvals.size == currentAvailableActorEvalsSize)
              }

              // If available state then loop again this actor on the first state
              loop = currentWorklist.nonEmpty
              if (loop) {  // keep the first state
                val (newCurrentState, newCurrentWorklist) = extractHead(currentWorklist)
                currentState = newCurrentState
                currentWorklist = newCurrentWorklist
              }
              else {       // else make this actor available
                currentAvailableActorEvals ::= self
                currentAvailableActorEvalsSize += 1
              }

              assert(currentAvailableActorEvals.isEmpty || currentWorklist.isEmpty)

              //
              // Updates with remaining actors xor states
              //
              if (currentAvailableActorEvalsSize != 0) {  // remaining actors
                assert(currentAvailableActorEvals.nonEmpty && currentWorklist.isEmpty)

                worklistAvailableActorEvalsLock.synchronized {
                  availableActorEvals :::= currentAvailableActorEvals
                  availableActorEvalsSize += currentAvailableActorEvalsSize

                  assert(availableActorEvals.size == availableActorEvalsSize)

                  checkAndReturns  // if finished then returns to main caller
                }
              }
              else if (currentWorklist.nonEmpty)          // remaining states
                worklistAvailableActorEvalsLock.synchronized { worklist :::= currentWorklist }
            }
          } while (loop)
          // finished message sent or at least one ActorEval is "busy" and will check later

        case ReturnLocalResults => sender ! (localHalted, localEdges)

        case Start =>  // initializes and sends initial state
          mainCaller = sender
          availableActorEvals = (1 until maxActorEval).map(actorEvals).toList
          availableActorEvalsSize = maxActorEval - 1

          stats.incSentNb
          actorEvals.head ! ActorEval.Eval(State.inject(exp, sem.initialEnv, sem.initialStore))

        // case x => p(s"!!! Eval $x")  // for debug
      }
    }

    for (i <- 0 until maxActorEval)  // create all ActorEval
      actorEvals(i) = actorSystem.actorOf(Props(new ActorEval), s"ActorEval-$i")


    //
    // Start by sending the initial state
    //
    val (timedOut: Boolean, (halted: List[State], graph: Option[Graph[State]])) =
      try {    // run during available time
        val future: Future[CollectedLocalResults] =
          (actorEvals.head ? ActorEval.Start).mapTo[CollectedLocalResults]

        (false, Await.result(future, config.timeout))
      }
      catch {  // timed out
        case _: scala.concurrent.TimeoutException => (true, (Nil, None))
      }

    actorSystem.terminate

    val haltedSet: Set[State] = halted.toSet

    // Return result
    SeqAAMOutput(
      haltedSet,
      visited.size,
      timeout.time,
      graph,
      timedOut,
      stats)
  }
}
