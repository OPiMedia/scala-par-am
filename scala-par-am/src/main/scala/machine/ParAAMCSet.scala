package be.opimedia.scala_par_am

/**
  * ParAAMCSet: ParAAM-C-set for Parallel AAM - Concurrent - set
  *
  * Similar to [[ParAAMCState]]
  * but sends set of states instead state.
  *
  * The worklist is implemented by an immutable list of sets of states.
  *
  * The main method eval() just sends the initial state to the first ActorEval.
  *
  * Each actor ActorEval evaluates a set of states, updates data,
  * and sends one set of states of the worklist to each actor ActorEval available,
  * until to the worklist becomes empty.
  *
  * <strong>Source</strong> [[https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/machine/ParAAMCSet.scala]]
  */
class ParAAMCSet[Exp : Expression, Abs : JoinLattice, Addr : Address, Time : Timestamp]
    extends ParAAMCState[Exp, Abs, Addr, Time] {
  override def name: String = "ParAAM-C-set"

  /** Print function use during development and debug */
  private def p(s: String): Unit = synchronized {
    if (false) {
      System.err.println(s)
      System.err.flush
    }
  }
  p(s"DEBUG p $name!")



  /**
    * Performs the evaluation of an expression as [[SeqAAM.eval]].
    * It is the main work that is parallelized.
    */
  override
  def eval(exp: Exp, sem: Semantics[Exp, Abs, Addr, Time], config: Configuration.MachineConfig): Output = {
    require(config.process >= minProcess)

    val computeGraph: Boolean = config.outputGraphEnabled
    val timeout: Timeout = Timeout.start(config.timeout)

    import akka.actor.{Actor, ActorRef, ActorSystem, Props}
    import akka.pattern.ask
    import scala.concurrent.{Await, Future}
    import scala.concurrent.duration._
    import scala.language.postfixOps

    implicit val _ = akka.util.Timeout(config.timeout.toNanos nanoseconds)
    // implicit val _ = akka.util.Timeout(10 second)  // for debug


    // Set the evaluation step function, with filtering enabled or not
    def step(state: State, sem: Semantics[Exp, Abs, Addr, Time], visited: Set[State]): Set[State] =
      if (config.stepFilterEnabled) state.stepFilter(sem, visited) else state.step(sem)


    // Starts with the initial state resulting from injecting the program below
    var worklist: List[Set[State]] = Nil

    // Set of states already visited
    var visited: Set[State] = Set.empty[State]

    val stats: ParAAMStats =
      if (config.statsEnabled) new ParAAMStatsEnabled
      else new ParAAMStatsDisabled

    // List of all available ActorEval
    var availableActorEvals: List[ActorRef] = null
    var availableActorEvalsSize: Int = 0


    // Lock used to access and modify worklist and availableActorEvals together
    val worklistAvailableActorEvalsLock: AnyRef = AnyRef

    // Lock used to modify visited
    val visitedLock: AnyRef = AnyRef


    val maxActorEval: Int = config.process - nbNotEvalProcess

    assert(maxActorEval > 0)

    val actorSystem: ActorSystem = ActorSystem("Akka-System-ParAAMCSet-eval")
    val actorEvals: Array[ActorRef] = Array.ofDim[ActorRef](maxActorEval)


    // Reference to main thread that wait result
    var mainCaller: ActorRef = null


    //
    // Definition and instantiation of one type of actors
    //

    final object ActorEval {
      final case class Eval(states: Set[State])
      val ReturnLocalResults: Int = 1
      val Start: Int = 2
    }

    // Actor to evaluate state
    final class ActorEval extends Actor {
      import ActorEval.{Eval, ReturnLocalResults, Start}

      var localHalted: List[State] = Nil
      var localEdges: List[(State, Set[State])] = Nil


      /**
        * Check if all work is finished,
        * and then sends local results to the main caller.
        *
        * Warning! Must be call in worklistAvailableActorEvalsLock.synchronized {}.
        */
      def checkAndReturns(): Unit = {
        if ((availableActorEvalsSize == maxActorEval) && worklist.isEmpty) {  // finished
          val results: CollectedLocalResults =
            collectLocalResults(actorEvals, ActorEval.ReturnLocalResults, config.timeout,
                                computeGraph,
                                self, (localHalted, localEdges))

          mainCaller ! results  // returns to main caller
        }
      }


      override
      def receive = {
        case Eval(states: Set[State]) =>  // evaluates states (and maybe next), updates, and sends other states available
          var currentStates: Set[State] = states
          var loop: Boolean = false

          do {
            var newStates: List[Set[State]] = Nil

            //
            // Evaluates
            //
            for (state: State <- currentStates) {
              if (!visited.contains(state)) {  // for each state probably not yet visited
                visitedLock.synchronized { visited += state }

                if (state.halted)  // halted state
                  localHalted ::= state
                else {             // new state with successors
                  val successors: Set[State] = step(state, sem, visited)
                  if (successors.nonEmpty) {
                    stats.newStates(successors.size)
                    newStates ::= successors
                    if (computeGraph)
                      localEdges ::= (state, successors)
                  }
                }
              }
              else                             // already visited
                stats.incAlreadyVisitedNb
            }

            //
            // Updates
            //

            if (newStates.isEmpty && worklist.isEmpty) {  // no state available
              loop = false

              //
              // Only updates available actors
              //
              worklistAvailableActorEvalsLock.synchronized {
                // At this point maybe some states are available
                // because maybe other actor updated the worklist

                availableActorEvals ::= self  // makes this actor available
                availableActorEvalsSize += 1

                assert(availableActorEvals.size == availableActorEvalsSize)

                checkAndReturns  // if finished then returns to main caller
              }
            }
            else {                                        // at least one state available
              //
              // Distributes available work
              //

              // Gets current list of other available actors and worklist
              var currentAvailableActorEvals: List[ActorRef] = Nil
              var currentAvailableActorEvalsSize: Int = 0
              var currentWorklist: List[Set[State]] = Nil

              worklistAvailableActorEvalsLock.synchronized {
                // At this point maybe available states are no longer available
                // because maybe other actor get them from worklist

                currentAvailableActorEvals = availableActorEvals
                availableActorEvals = Nil
                currentAvailableActorEvalsSize = availableActorEvalsSize
                availableActorEvalsSize = 0

                currentWorklist = worklist
                worklist = Nil
              }

              currentWorklist :::= newStates

              if (currentAvailableActorEvals.nonEmpty && currentWorklist.nonEmpty) {  // some actorEvals and states are available
                val actorEvalStateIter: ZipRemainsIterator[ActorRef, Set[State]] =
                  ZipRemainsIterator(currentAvailableActorEvals, currentWorklist)

                // Sends available states (by set of states) to available actorEvals
                for ((actorEval: ActorRef, states: Set[State]) <- actorEvalStateIter) {
                  stats.pickStates(states.size)
                  stats.incSentNb
                  actorEval ! ActorEval.Eval(states)
                  currentAvailableActorEvalsSize -= 1
                }

                // Keeps remaining actors xor states
                val (remain1, remain2) = actorEvalStateIter.remains
                currentAvailableActorEvals = remain1.toList
                currentWorklist = remain2.toList

                assert(currentAvailableActorEvals.size == currentAvailableActorEvalsSize)
              }

              // If available state then loop again this actor on the first set of states
              loop = currentWorklist.nonEmpty
              if (loop) {  // keep the first set
                currentStates = currentWorklist.head
                currentWorklist = currentWorklist.tail
              }
              else {       // else make this actor available
                currentAvailableActorEvals ::= self
                currentAvailableActorEvalsSize += 1
              }

              assert(currentAvailableActorEvals.isEmpty || currentWorklist.isEmpty)

              //
              // Updates with remaining actors xor states
              //
              if (currentAvailableActorEvalsSize != 0) {  // remaining actors
                assert(currentAvailableActorEvals.nonEmpty && currentWorklist.isEmpty)

                worklistAvailableActorEvalsLock.synchronized {
                  availableActorEvals :::= currentAvailableActorEvals
                  availableActorEvalsSize += currentAvailableActorEvalsSize

                  assert(availableActorEvals.size == availableActorEvalsSize)

                  checkAndReturns  // if finished then returns to main caller
                }
              }
              else if (currentWorklist.nonEmpty)          // remaining states
                worklistAvailableActorEvalsLock.synchronized { worklist :::= currentWorklist }
            }
          } while (loop)
          // finished message sent or at least one ActorEval is "busy" and will check later

        case ReturnLocalResults => sender ! (localHalted, localEdges)

        case Start =>  // initializes and sends initial state
          mainCaller = sender
          availableActorEvals = (1 until maxActorEval).map(actorEvals).toList
          availableActorEvalsSize = maxActorEval - 1

          stats.incSentNb
          actorEvals.head ! ActorEval.Eval(Set(State.inject(exp, sem.initialEnv, sem.initialStore)))

        // case x => p(s"!!! Eval $x")  // for debug
      }
    }

    for (i <- 0 until maxActorEval)  // create all ActorEval
      actorEvals(i) = actorSystem.actorOf(Props(new ActorEval), s"ActorEval-$i")


    //
    // Start by sending the initial state
    //
    val (timedOut: Boolean, (halted: List[State], graph: Option[Graph[State]])) =
      try {    // run during available time
        val future: Future[CollectedLocalResults] =
          (actorEvals.head ? ActorEval.Start).mapTo[CollectedLocalResults]

        (false, Await.result(future, config.timeout))
      }
      catch {  // timed out
        case _: scala.concurrent.TimeoutException => (true, (Nil, None))
      }

    actorSystem.terminate

    val haltedSet: Set[State] = halted.toSet

    // Return result
    SeqAAMOutput(
      haltedSet,
      visited.size,
      timeout.time,
      graph,
      timedOut,
      stats)
  }
}
