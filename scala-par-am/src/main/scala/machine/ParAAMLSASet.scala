package be.opimedia.scala_par_am

/**
  * ParAAMLSASet: ParAAM-L-SA-set for Parallel AAM - Loop - SenderAggregator - set
  *
  * Similar to [[ParAAMLSAState]]
  * but sends successively set of states instead state.
  *
  * The worklist is implemented by an immutable list of sets of states.
  *
  * The main method eval() loops until the worklist is empty.
  * An unique actor ActorSenderAggregator
  * sends successively each set of states of the worklist to actors ActorEval (modulo the number of these actors)
  * and then collects each of these results.
  *
  * <strong>Source</strong> [[https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/machine/ParAAMLSASet.scala]]
  */
class ParAAMLSASet[Exp : Expression, Abs : JoinLattice, Addr : Address, Time : Timestamp]
    extends ParAAMLSAState[Exp, Abs, Addr, Time] {
  override def name: String = "ParAAM-L-SA-set"

  /** Print function use during development and debug */
  private def p(s: String): Unit = synchronized {
    if (false) {
      System.err.println(s)
      System.err.flush
    }
  }
  p(s"DEBUG p $name!")



  /**
    * Performs the evaluation of an expression as [[SeqAAM.eval]].
    * It is the main work that is parallelized.
    */
  override
  def eval(exp: Exp, sem: Semantics[Exp, Abs, Addr, Time], config: Configuration.MachineConfig): Output = {
    require(config.process >= minProcess)

    val computeGraph: Boolean = config.outputGraphEnabled
    val timeout: Timeout = Timeout.start(config.timeout)

    import akka.actor.{Actor, ActorRef, ActorSystem, Props}
    import akka.pattern.ask
    import scala.concurrent.Await
    import scala.concurrent.duration._
    import scala.language.postfixOps

    implicit val _ = akka.util.Timeout(config.timeout.toNanos nanoseconds)
    // implicit val _ = akka.util.Timeout(10 second)  // for debug


    // Set the evaluation step function, with filtering enabled or not
    def step(state: State, sem: Semantics[Exp, Abs, Addr, Time], visited: Set[State]): Set[State] =
      if (config.stepFilterEnabled) state.stepFilter(sem, visited) else state.step(sem)


    // Starts with the initial state resulting from injecting the program (only modified/used in ActorSenderAggregator and used in main loop)
    var worklist: List[Set[State]] = List(Set(State.inject(exp, sem.initialEnv, sem.initialStore)))

    // Set of states already visited (only modified/used in ActorSenderAggregator.Start and used in ActorEval.Eval)
    var visited: Set[State] = Set.empty[State]

    val stats: ParAAMStats =
      if (config.statsEnabled) new ParAAMStatsEnabled
      else new ParAAMStatsDisabled


    val maxActorEval: Int = config.process - nbNotEvalProcess

    assert(maxActorEval > 0)

    val actorSystem: ActorSystem = ActorSystem("Akka-System-ParAAMLSASet-eval")
    val actorEvals: Array[ActorRef] = Array.ofDim[ActorRef](maxActorEval)


    //
    // Definition and instantiation of the 2 types of actors
    //

    final object ActorSenderAggregator {
      final case class Result(newStates: List[Set[State]])
      val Start: Int = 1
    }

    final object ActorEval {
      final case class Eval(states: Set[State], currentVisited: Set[State])
      val ReturnLocalResults: Int = 11
    }

    // Actor that collects all results of evaluated states.
    // Starts by send all states (set by set) to actorsEvals.
    final class ActorSenderAggregator extends Actor {
      import ActorSenderAggregator.{Result, Start}

      // True iff all state of the current worklist are sent
      var isAllStateSent: Boolean = false

      // Number of results waited for the current loop
      var nbResultWaited: Int = 0

      // Reference to main thread that wait result
      var mainCaller: ActorRef = null

      override
      def receive = {
        case Result(newStates: List[Set[State]]) =>  // updates successors and returns if finished
          if (newStates.nonEmpty) worklist :::= newStates

          nbResultWaited -= 1
          if (isAllStateSent && (nbResultWaited == 0))  // this loop is finished
            mainCaller ! Nil  // just say finished

        case Start =>  // initializes and sends all states
          isAllStateSent = false
          nbResultWaited = 0
          mainCaller = sender

          // Sends all states (by set of states) from current worklist to actorEvals
          for ((states: Set[State], i: Int) <- worklist.zipWithIndex) {
            stats.pickStates(1)

            stats.incSentNb
            actorEvals(i % maxActorEval) ! ActorEval.Eval(states, visited)
            visited ++= states
            nbResultWaited += 1
          }

          worklist = Nil  // a new empty worklist

          isAllStateSent = true

        // case x => p(s"!!! SenderAggregator $x")  // for debug
      }
    }

    val actorSenderAggregator: ActorRef = actorSystem.actorOf(Props(new ActorSenderAggregator), "ActorSenderAggregator")


    // Actor to evaluate state
    final class ActorEval extends Actor {
      import ActorEval.{Eval, ReturnLocalResults}

      var localHalted: List[State] = Nil
      var localEdges: List[(State, Set[State])] = Nil

      override
      def receive = {  // evaluates states and sends results
        case Eval(states: Set[State], currentVisited: Set[State]) =>
          var newStates: List[Set[State]] = Nil

          for (state: State <- states) {
            if (!currentVisited.contains(state)) {  // state not yet visited
              if (state.halted)  // halted state
                localHalted ::= state
              else {             // new state with successors
                val successors: Set[State] = step(state, sem, visited)
                if (successors.nonEmpty) {
                  stats.newStates(successors.size)
                  newStates ::= successors
                  if (computeGraph)
                    localEdges ::= (state, successors)
                }
              }
            }
            else                                    // already visited
              stats.incAlreadyVisitedNb
          }

          actorSenderAggregator ! ActorSenderAggregator.Result(newStates)

        case ReturnLocalResults => sender ! (localHalted, localEdges)

        // case x => p(s"!!! Eval $x")  // for debug
      }
    }

    for (i <- 0 until maxActorEval)  // create all ActorEval
      actorEvals(i) = actorSystem.actorOf(Props(new ActorEval), s"ActorEval-$i")


    //
    // Main loop
    //

    val (timedOut: Boolean, (halted: List[State], graph: Option[Graph[State]])) =
      try {    // run during available time
        while (worklist.nonEmpty) {  // remains state in worklist
          stats.incLoopIterNb

          val future = actorSenderAggregator ? ActorSenderAggregator.Start

          Await.ready(future, config.timeout)
        }
        val results: CollectedLocalResults =
          collectLocalResults(actorEvals, ActorEval.ReturnLocalResults, config.timeout,
                              computeGraph)

        (false, results)
      }
      catch {  // timed out
        case _: scala.concurrent.TimeoutException => (true, (Nil, None))
      }

    actorSystem.terminate

    val haltedSet: Set[State] = halted.toSet

    // Return result
    SeqAAMOutput(
      haltedSet,
      visited.size,
      timeout.time,
      graph,
      timedOut,
      stats)
  }
}
