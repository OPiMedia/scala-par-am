package be.opimedia.scala_par_am

/**
  * ParAAMCHybrid: similar than ParAAMCPart and ParAAMCSet when the number of sets in the worklist
  *                if greater or equal to the number of available actors.
  *
  * The worklist is implemented by an immutable list of set of states.
  *
  * The main method eval() just send the initial state to the first ActorEval.
  *
  * Each actor ActorEval evaluates a set of states, updates data,
  * and sends equal part of the worklist to each actor ActorEval available,
  * until to the worklist becomes empty.
  *
  * <strong>Source</strong> [[https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/machine/ParAAMCHybrid.scala]]
  */
class ParAAMCHybrid[Exp : Expression, Abs : JoinLattice, Addr : Address, Time : Timestamp]
    extends ParAAMCState[Exp, Abs, Addr, Time] {
  override def name: String = "ParAAM-C-hybrid"

  /** Print function use during development and debug */
  private def p(s: String): Unit = synchronized {
    if (false) {
      System.err.println(s)
      System.err.flush
    }
  }
  p(s"DEBUG p $name!")



  /**
    * Performs the evaluation of an expression as [[SeqAAM.eval]].
    * It is the main work that is parallelized.
    */
  override
  def eval(exp: Exp, sem: Semantics[Exp, Abs, Addr, Time], config: Configuration.MachineConfig): Output = {
    require(config.process >= minProcess)

    val computeGraph: Boolean = config.outputGraphEnabled
    val timeout: Timeout = Timeout.start(config.timeout)

    import akka.actor.{Actor, ActorRef, ActorSystem, Props}
    import akka.pattern.ask
    import scala.concurrent.{Await, Future}
    import scala.concurrent.duration._
    import scala.language.postfixOps

    implicit val _ = akka.util.Timeout(config.timeout.toNanos nanoseconds)
    // implicit val _ = akka.util.Timeout(10 second)  // for debug


    // Set the evaluation step function, with filtering enabled or not
    def step(state: State, sem: Semantics[Exp, Abs, Addr, Time], visited: Set[State]): Set[State] =
      if (config.stepFilterEnabled) state.stepFilter(sem, visited) else state.step(sem)


    // Set of states already visited
    var visited: Set[State] = Set.empty[State]

    val stats: ParAAMStats =
      if (config.statsEnabled) new ParAAMStatsEnabled
      else new ParAAMStatsDisabled

    // List of all available ActorEval
    var availableActorEvals: List[ActorRef] = null
    var availableActorEvalsSize: Int = 0


    // Lock used to modify visited
    val visitedLock: AnyRef = AnyRef

    // Lock used to access and modify availableActorEvals
    val availableActorEvalsLock: AnyRef = AnyRef


    val maxActorEval: Int = config.process - nbNotEvalProcess

    assert(maxActorEval > 0)

    val actorSystem: ActorSystem = ActorSystem("Akka-System-ParAAMCHybrid-eval")
    val actorEvals: Array[ActorRef] = Array.ofDim[ActorRef](maxActorEval)


    // Reference to main thread that wait result
    var mainCaller: ActorRef = null


    //
    // Definition and instantiation of one type of actors
    //

    final object ActorEval {
      final case class Eval(states: Iterable[State])
      val ReturnLocalResults: Int = 1
      val Start: Int = 2
    }

    // Actor to evaluate state
    final class ActorEval extends Actor {
      import ActorEval.{Eval, ReturnLocalResults, Start}

      var localHalted: List[State] = Nil
      var localEdges: List[(State, Set[State])] = Nil


      /**
        * Check if all work is finished,
        * and then sends local results to the main caller.
        */
      def checkAndReturns(): Unit = {
        if (availableActorEvalsSize == maxActorEval) {
          val results: CollectedLocalResults =
            collectLocalResults(actorEvals, ActorEval.ReturnLocalResults, config.timeout,
                                computeGraph,
                                self, (localHalted, localEdges))

          mainCaller ! results  // returns to main caller
        }
      }


      override
      def receive = {
        case Eval(states: Iterable[State]) =>  // evaluates states, updates, and sends other states available
          var newStates: List[Set[State]] = Nil
          var newStatesSize: Int = 0
          var newStatesSizeList: Int = 0

          //
          // Evaluates
          //
          for (state: State <- states) {
            if (!visited.contains(state)) {  // for each state not yet visited
              visitedLock.synchronized { visited += state }

              if (state.halted)  // halted state
                localHalted ::= state
              else {             // new state with successors
                val successors: Set[State] = step(state, sem, visited)
                if (successors.nonEmpty) {
                  stats.newStates(successors.size)
                  newStates ::= successors
                  newStatesSize += successors.size
                  newStatesSizeList += 1
                  if (computeGraph)
                    localEdges ::= (state, successors)
                }
              }
            }
            else                             // already visited
              stats.incAlreadyVisitedNb
          }

          //
          // Updates
          //
          assert(newStates.flatten.size == newStatesSize)
          assert(newStates.size == newStatesSizeList)

          if (newStatesSize == 0) {  // no state available
            //
            // Only updates available actors
            //
            availableActorEvalsLock.synchronized {
              // at this point maybe some states are available

              availableActorEvals ::= self  // makes this actor available
              availableActorEvalsSize += 1

              assert(availableActorEvals.size == availableActorEvalsSize)

              checkAndReturns  // if finished then returns to main caller
            }
          }
          else {                    // at least one state available
            //
            // Distributes available work
            //

            if (newStatesSize == 1) {  // only one state
              assert(newStates.size == 1)

              // Sends the state to this actor
              stats.pickStates(states.size)
              stats.incSentNb
              self ! ActorEval.Eval(newStates.head)
            }
            else {                     // no state or several states
              // Get current list of available actors
              var currentAvailableActorEvals: List[ActorRef] = null
              var currentAvailableActorEvalsSize: Int = 0

              availableActorEvalsLock.synchronized {
                // at this point maybe available states are no longer available

                currentAvailableActorEvals = availableActorEvals
                availableActorEvals = Nil
                currentAvailableActorEvalsSize = availableActorEvalsSize
                availableActorEvalsSize = 0
              }

              // Adds this actor and new successors
              currentAvailableActorEvals :+= self  // makes this actor available (at the end)
              currentAvailableActorEvalsSize += 1

              assert(currentAvailableActorEvals.size == currentAvailableActorEvalsSize)

              if (newStatesSize != 0) {  // some states are available
                assert(currentAvailableActorEvals.nonEmpty)

                // Sends all states (by "equal" part of states) from current worklist to actorEvals
                var nb: Int = 0
                for ((actor: ActorRef, states: Iterable[State]) <- currentAvailableActorEvals zip PartsHybrid.parts(newStates, newStatesSize, newStatesSizeList, currentAvailableActorEvalsSize)) {
                  stats.pickStates(states.size)
                  stats.incSentNb
                  actor ! ActorEval.Eval(states)
                  nb += 1
                }

                // Keeps remaining actors
                currentAvailableActorEvals = currentAvailableActorEvals.drop(nb)
                currentAvailableActorEvalsSize -= nb
              }

              //
              // Updates with remaining actors
              //
              if (currentAvailableActorEvalsSize != 0) {  // remaining actors
                assert(currentAvailableActorEvals.nonEmpty)

                availableActorEvalsLock.synchronized {
                  availableActorEvals :::= currentAvailableActorEvals
                  availableActorEvalsSize += currentAvailableActorEvalsSize

                  assert(availableActorEvals.size == availableActorEvalsSize)

                  checkAndReturns  // if finished then returns to main caller
                }
              }
            }
          }

          // finished message sent or at least one ActorEval is "busy" and will check later

        case ReturnLocalResults => sender ! (localHalted, localEdges)

        case Start =>  // initializes and sends initial state
          mainCaller = sender
          availableActorEvals = (1 until maxActorEval).map(actorEvals).toList
          availableActorEvalsSize = maxActorEval - 1

          stats.incSentNb
          actorEvals.head ! ActorEval.Eval(List(State.inject(exp, sem.initialEnv, sem.initialStore)))

        // case x => p(s"!!! Eval $x")  // for debug
      }
    }

    for (i <- 0 until maxActorEval)  // create all ActorEval
      actorEvals(i) = actorSystem.actorOf(Props(new ActorEval), s"ActorEval-$i")


    //
    // Start by sending the initial state
    //
    val (timedOut: Boolean, (halted: List[State], graph: Option[Graph[State]])) =
      try {    // run during available time
        val future: Future[CollectedLocalResults] =
          (actorEvals.head ? ActorEval.Start).mapTo[CollectedLocalResults]

        (false, Await.result(future, config.timeout))
      }
      catch {  // timed out
        case _: scala.concurrent.TimeoutException => (true, (Nil, None))
      }

    actorSystem.terminate

    val haltedSet: Set[State] = halted.toSet

    // Return result
    SeqAAMOutput(
      haltedSet,
      visited.size,
      timeout.time,
      graph,
      timedOut,
      stats)
  }
}
