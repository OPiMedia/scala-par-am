package be.opimedia.scala_par_am

import java.nio.file.{Path, Paths}
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration.{Duration, FiniteDuration}

/**
  * <strong>Scala-Par-AM</strong> is a modified version of <strong>Scala-AM</strong>
  * with the goal to study the <strong>parallelization of abstract interpretation</strong>,
  * focused on analysis of Scheme programs.
  * Some unused parts of Scala-AM have been removed.
  * The essential parts are in the machine/ sub-directory.
  *
  * This main program executes some abstract machines on some Scheme programs
  * depending of some parameters
  * and print results in TSV format (data separated by tabulation).
  *
  * Run with `--help` command line parameter to show all options:
  * <pre>$ java -jar scala-par-am.jar --help</pre>
  *
  * Or directly with the shell script that also set some JVM configurations:
  * <pre>$ ./scala-par-am.sh --help</pre>
  *
  * This example:
  * <pre>$ ./scala-par-am.sh --files "Scheme-examples/OPi/factorial*.scm" --machines SeqAAMLS,ParAAMLSAPart,ParAAMCPart --processes 1,4,2</pre>
  * runs on files that match the "Scheme-examples/OPi/factorial*.scm" path
  * with machine SeqAAMLS on 1 process,
  * with machine ParAAMLSAPart on 4 and 2 processes,
  * and with machine ParAAMCPart with 1, 4 and 2 processes.
  *
  * scala-par-am.jar can be downloaded directly
  * [[https://bitbucket.org/OPiMedia/scala-par-am/downloads/]]
  *
  * scala-am.jar must be accessible in the classpath to use oldAAM machine.
  * Download it if necessary:
  * [[https://bitbucket.org/OPiMedia/scala-am/downloads/]]
  *
  * Depending of command line parameters,
  * for each lattice specified,
  * for each bound,
  * for each address,
  * for each file Scheme program,
  * for default step and/or step with filter,
  * for each machine
  * and for each number of processes
  * this program do the following:
  *
  * 1. The input Scheme program is parsed. It is done by:
  *    - Parsing the file as a list of s-expressions (exp/SExp.scala,
  *      exp/SExpParser.scala)
  *    - Compiling these s-expressions into Scheme expressions
  *      (exp/scheme/Scheme.scala)
  *
  * 2. To run the program, we need an abstract machine and some semantics.
  *    Semantics definitions have to implement the Semantics interface
  *    (semantics/Semantics.scala).
  *
  * 3. Once the abstract machine is created
  *    and we have a semantics for the program we want to analyze,
  *    the abstract machine can perform its evaluation,
  *    relying on methods of the semantics class to know how to evaluate expressions.
  *    The abstract machine only deals with which states to evaluate in which order,
  *    where to store values, where to store continuations,
  *    how to push and pop continuations, etc.
  *    The semantics encode what to do when encountering a program construct.
  *    For example, the semantics can tell what to evaluate next,
  *    that a continuation needs to be pushed, or that a variable needs to be updated.
  *    The abstract machine will then respectively evaluate the expression needed,
  *    push the continuation, or update the variable.
  *
  *    Multiple abstract machine implementations are available, defined in the machine/ directory.
  *    Every abstract machine implementation
  *    has to implement the AbstractMachine interface (machine/AbstractMachine.scala).
  *
  * List of all available machines:
  *   - oldAAM: to call AAMNS (AAM No Subsumption) of Scala-AM [[https://bitbucket.org/OPiMedia/scala-am]]
  *
  *   - [[SeqAAM]]:   sequential machine where the worklist implemented with a list of states
  *   - [[SeqAAMS]]:  sequential machine where the worklist implemented with a set of states
  *   - [[SeqAAMLS]]: sequential machine where the worklist implemented with a list of sets of states
  *   - [[SeqAAMLSH]]: SeqAAMLS that includes to the worklist only the not halted states
  *
  *   - [[ParAAMLSAState]]: ParAAM-L-SA-state for Parallel AAM - Loop - SenderAggregator - state
  *   - [[ParAAMLSASet]]:   ParAAM-L-SA-set   for Parallel AAM - Loop - SenderAggregator - set
  *   - [[ParAAMLSAPart]]:  ParAAM-L-SA-part  for Parallel AAM - Loop - SenderAggregator - part
  *
  *   - [[ParAAMCSState]]: ParAAM-C-S-state for Parallel AAM - Concurrent - Sender - state
  *   - [[ParAAMCSSet]]:   ParAAM-C-S-set   for Parallel AAM - Concurrent - Sender - set
  *   - [[ParAAMCSPart]]:  ParAAM-C-S-part  for Parallel AAM - Concurrent - Sender - part
  *
  *   - [[ParAAMCState]]:  ParAAM-C-state for Parallel AAM - Concurrent - state
  *   - [[ParAAMCSet]]:    ParAAM-C-set   for Parallel AAM - Concurrent - set
  *   - [[ParAAMCPart]]:   ParAAM-C-part  for Parallel AAM - Concurrent - part
  *   - [[ParAAMCHybrid]]: ParAAM-C-hybrid
  *
  *   - [[ParAAMCSetH]]:    ParAAM-C-set-halt
  *   - [[ParAAMCPartH]]:   ParAAM-C-part-halt
  *   - [[ParAAMCHybridH]]: ParAAM-C-hybrid-halt
  *
  *    The abstract machine also uses a lattice to represent values.
  *    Lattices should implement the JoinLattice trait
  *    that can be found in lattice/JoinLattice.scala,
  *    which provides the basic features of a lattice.
  *
  *   - The repository of <strong>Scala-Par-AM</strong>:
  *     [[https://bitbucket.org/OPiMedia/scala-par-am]]
  *   - <strong>ScalaDoc</strong> of this code:
  *     [[http://www.opimedia.be/DS/online-documentations/Scala-Par-AM/html/be/opimedia/scala_par_am/Main$.html]]
  *
  *   - The repository of the starting fork of Scala-AM:
  *     [[https://bitbucket.org/OPiMedia/scala-am]]
  *   - The original repository of Scala-AM:
  *     [[https://github.com/acieroid/scala-am]]
  *
  *   - <strong>Master thesis</strong> about this project:
  *     [[https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala/]]
  *
  * <strong>Source</strong> [[https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/src/main/scala/Main.scala]]
  *
  * @version June 18, 2020
  *
  * @author Olivier Pirson
  * @author Quentin Stiévenart (for the original Scala-AM that composes the biggest part of this project)
  */
object Main extends App {
  val scalaParAMVersion: String = "01.00.02 --- June 18, 2020"

  case class ParamsConfig(

    // Valid patterns for filenamesPattern:
    // https://docs.oracle.com/javase/8/docs/api/java/nio/file/FileSystem.html#getPathMatcher-java.lang.String-

    root: String = "",
    exceptsFilename: String = "",

    lattices: Seq[Configuration.Lattice.Value] = List(Configuration.Lattice.TypeSet),
    bounds: Seq[Int] = List(100),
    addresses: Seq[Configuration.Address.Value] = List(Configuration.Address.Classical),
    filenamesPattern: String = "regex:.+\\.scm",
    stepFilters: Seq[Boolean] = List(false),
    machines: Seq[Configuration.Machine.Value] = List(Configuration.Machine.SeqAAMLS),
    processes: Seq[Int] = List(1),

    filenamePrepare: String = "",
    nbRepetition: Int = 1,
    timeout: FiniteDuration = FiniteDuration(1, "day"),
    betweenTime: FiniteDuration = FiniteDuration(1, "second"),

    headerEnabled: Boolean = true,
    infoHeaderEnabled: Boolean = false,
    statsEnabled: Boolean = false,
    statsEscaped: Boolean = false,
    kamonEnabled: Boolean = false,

    outputGraphEnabled: Boolean = false,
    statsOutputGraphEnabled: Boolean = false,
    dotGraphEnabled: Boolean = false,

    isListMachines: Boolean = false,
    justListEnabled: Boolean = false,
    estimationTime: FiniteDuration = FiniteDuration(1, "second")
  ) {
    override
    def toString: String = s"ParamsConfig:\troot: $root\texceptsFilename: $exceptsFilename\tlattices: $lattices\tbounds: $bounds\taddresses: $addresses\tfilenamesPattern: $filenamesPattern\tstepFilters: $stepFilters\tmachines: $machines\tprocesses: $processes\tfilenamePrepare: $filenamePrepare\tnbRepetition: $nbRepetition\ttimeout: $timeout\tbetweenTime: $betweenTime\theaderEnabled: $headerEnabled\tinfoHeaderEnabled: $infoHeaderEnabled\tstatsEnabled: $statsEnabled\tstatsEscaped: $statsEscaped\tkamonEnabled: $kamonEnabled\toutputGraphEnabled: $outputGraphEnabled\tstatsOutputGraphEnabled: $statsOutputGraphEnabled\tdotGraphEnabled: $dotGraphEnabled\tisListMachines: $isListMachines\tjustListEnabled: $justListEnabled\testimationTime: $estimationTime"
  }

  val default: ParamsConfig = ParamsConfig()

  /**
    * To run oldAAM machine (AAMNS - AAM No Subsumption) of Scala-AM on file
    * with MainAsScalaParAM.main of scala-am.jar
    */
  lazy val runOldMainMethod: java.lang.reflect.Method = loadRunOldMainMethod


  /**
    * For each file described in config,
    * for each machine, lattice, process from config.machines, config.lattices and config.processes,
    * apply run(lattice, bound,
    *           address,
    *           file,
    *           stepFilterEnabled,
    *           machine,
    *           process,
    *           config.timeout, config.nbRepetition, config.betweenTime,
    *           config.outputGraphEnabled, config.statsOutputGraphEnabled,
    *           config.dotGraphEnabled,
    *           config.statsEnabled, config.statsEscaped,
    *           config.justListEnabled).
    *
    * If machine is oldAAM
    * then apply runOld(lattice, bound, address, file, config.timeout, config.nbRepetition)
    * instead run.
    */
  def foreachRun[U](config: ParamsConfig): Int = {
    import java.nio.file.{Files, FileSystems, FileVisitResult, PathMatcher, SimpleFileVisitor}

    val excepts: Set[String] = readExcepts(config.exceptsFilename)
    val pairFiles:ArrayBuffer[(Path, List[String])] = ArrayBuffer.empty[(Path, List[String])]

    try {
      val pathMatcher: PathMatcher =
        FileSystems.getDefault().getPathMatcher(config.filenamesPattern)

      // Fill pairFiles array
      // with all files corresponding to filenamesPattern and not contain in exceptsFilename
      Files.walkFileTree(Paths.get(config.root),
        new SimpleFileVisitor[Path] {
          override
          def visitFile(path: Path, attrs: java.nio.file.attribute.BasicFileAttributes) = {
            if (pathMatcher.matches(path) && !excepts.contains(path.toString)) {
              pairFiles += ((path, path.toString.split('/').toList))
            }
            FileVisitResult.CONTINUE
          }
        })
    }
    catch {
        case _ : Throwable =>
    }

    // Comparison function used to sort filenames
    def lt(a: List[String], b: List[String]): Boolean = {
      def compare(a: String, b: String): Int = {
        val ucomp = a.toUpperCase.compareTo(b.toUpperCase)

        if (ucomp < 0) -1
        else if (ucomp > 0) 1
        else a.compareTo(b)
      }

      (a, b) match {
        case (a1::Nil, b1::Nil) => compare(a1, b1) < 0
        case (_::Nil, _::_) => true
        case (_::_, _::Nil) => false
        case (a1::as, b1::bs) => {
          val c = compare(a1, b1)
          if (c != 0) c < 0 else lt(as, bs)
        }
        case _ => {
          assert(false)
          false
        }
      }
    }


    // Run machine on each lattice, Scheme program (in order), etc.
    var nbExecuted: Int = 0
    for (lattice <- config.lattices;
         bound <- config.bounds;
         address <- config.addresses;
         file <- pairFiles.sortWith((a, b) => lt(a._2, b._2));
         stepFilterEnabled <- config.stepFilters;
         machine <- config.machines;
         process <- config.processes) {
      nbExecuted += run(lattice, bound,
                        address,
                        file._1,
                        stepFilterEnabled,
                        machine,
                        process,
                        config.timeout, config.nbRepetition, config.betweenTime,
                        config.outputGraphEnabled, config.statsOutputGraphEnabled,
                        config.dotGraphEnabled,
                        config.statsEnabled, config.statsEscaped,
                        config.justListEnabled)
    }
    nbExecuted
  }


  /**
    * Load and return the MainAsScalaParAM.main method from scala-am.jar.
    */
  def loadRunOldMainMethod: java.lang.reflect.Method =
    try {
      val classLoader = new java.net.URLClassLoader(Array(new java.io.File("scala-am.jar").toURI.toURL))
      val mainClass = classLoader.loadClass("MainAsScalaParAM")

      mainClass.getMethod("main", Array.empty[String].getClass)
    }
    catch {  // eval failed
      case _: Throwable => null
    }


  /**
    * Dispatch to runOld (for oldAAM machine from Scala-AM)
    * or to runNew (Scala-Par-AM machines).
    */
  def run(latticeValue: Configuration.Lattice.Value, bound: Int,
          addressValue: Configuration.Address.Value,
          file: Path,
          stepFilterEnabled: Boolean,
          machineValue: Configuration.Machine.Value,
          process: Int,
          timeout: FiniteDuration, nbRepetition: Int, betweenTime: FiniteDuration,
          outputGraphEnabled: Boolean, statsOutputGraphEnabled: Boolean,
          dotGraphEnabled: Boolean,
          statsEnabled: Boolean, statsEscaped: Boolean,
          justListEnabled: Boolean): Int = {
    require(process >= 0)
    require(nbRepetition >= 0)

    if (machineValue == Configuration.Machine.oldAAM) {  // oldAAM from Scala-AM
      if ((process <= 1) && !stepFilterEnabled) {
        if (justListEnabled)
          println(s"$latticeValue\t$bound\t$addressValue\t${file.toString}\t$stepFilterEnabled\t$machineValue\t$process")
        else
          runOld(latticeValue, bound,
                 addressValue,
                 file,
                 timeout, nbRepetition)

        nbRepetition
      }
      else
        0
    }
    else                                                 // all machine of Scala-Par-AM
      runNew(latticeValue, bound,
             addressValue,
             file,
             stepFilterEnabled,
             machineValue,
             process,
             timeout, nbRepetition, betweenTime,
             outputGraphEnabled, statsOutputGraphEnabled,
             dotGraphEnabled,
             statsEnabled, statsEscaped,
             justListEnabled)
  }


  /**
    * Run the machine (!= oldAAM) on file
    * with corresponding parameters.
    */
  def runNew(latticeValue: Configuration.Lattice.Value, bound: Int,
             addressValue: Configuration.Address.Value,
             file: Path,
             stepFilterEnabled: Boolean,
             machineValue: Configuration.Machine.Value,
             process: Int,
             timeout: FiniteDuration, nbRepetition: Int, betweenTime: FiniteDuration,
             outputGraphEnabled: Boolean, statsOutputGraphEnabled: Boolean,
             dotGraphEnabled: Boolean,
             statsEnabled: Boolean, statsEscaped: Boolean,
             justListEnabled: Boolean): Int = {
    require(process > 0)
    require(nbRepetition >= 0)

    val machineConfig = new Configuration.MachineConfig(
      lattice=latticeValue,
      bound=bound,
      address=addressValue,
      filename=file.toString,
      stepFilterEnabled=stepFilterEnabled,
      machine=machineValue,
      process=process,
      timeout=timeout,
      statsEnabled=statsEnabled,
      outputGraphEnabled=outputGraphEnabled
    )

    val lattice: SchemeLattice = Configuration.getLattice(latticeValue, false, machineConfig.bound)
    implicit val isSchemeLattice: IsSchemeLattice[lattice.L] = lattice.isSchemeLattice

    val address: AddressWrapper = Configuration.getAddress(addressValue)
    implicit val isAddress = address.isAddress

    val time: TimestampWrapper = ZeroCFA
    implicit val isTimestamp = time.isTimestamp

    val machine = Configuration.getMachine(machineValue, lattice, address, time)

    val sem = new SchemeSemantics[lattice.L, address.A, time.T](new SchemePrimitives[address.A, lattice.L])

    if ((!machine.isParallel && (process == 1)) ||
      (machine.isParallel && (process >= machine.minProcess))) {
      print(s"$latticeValue\t$bound\t$addressValue\t${file.toString}\t$stepFilterEnabled\t${machine.name}\t$process")

      if (justListEnabled)
        println
      else {
        System.out.flush
        val program: String = Util.fileContent(file.toString).getOrElse("")  // read Scheme program

        val errors: ArrayBuffer[String] = ArrayBuffer.empty[String]
        val finishedTimes: ArrayBuffer[Double] = ArrayBuffer.empty[Double]
        val stats: ArrayBuffer[String] = if (statsEnabled) ArrayBuffer.empty[String] else null

        var firstResult: Option[machine.Output] = None
        var anyTimedOut: Boolean = false
        var anyError: Boolean = false
        var graphInfoStr: String = ""

        for (repetition <- 0 until nbRepetition) {
          if (!justListEnabled) {
            System.gc  // ask to free memory
            Thread.sleep(betweenTime.toMillis)
          }

          try {
            // Run the machine on the Scheme program
            val result: machine.Output = machine.eval(sem.parse(program), sem, machineConfig)

            // Print result
            print('\t' + result.toTsv(firstResult.isEmpty, List(nbRepetition.toString)))
            System.out.flush

            // Check result
            val (newFirstResult, error) = result.compareToReference(firstResult)
            firstResult = newFirstResult
            errors += error

            anyTimedOut |= result.timedOut
            anyError |= !result.timedOut && error.nonEmpty

            if (!result.timedOut)  // updates array of times
              finishedTimes += result.time

            if (statsEnabled)
              stats += result.statsToTsv(statsEscaped)

            if (outputGraphEnabled && (repetition == 0)) {
              if (statsOutputGraphEnabled) {  // prints stats output graph
                graphInfoStr = result.graphInfo match {
                  case Some(graphInfo) =>
                    (s"${graphInfo}"
                      :: graphInfo.outdegrees.toSeq.sortBy(_._1).map({
                        case (outdegree, nb) => s"# $outdegree: $nb"
                      }).toList).mkString("\t").replace("\t", "\\t")

                  case None => ""
                }
              }

              if (dotGraphEnabled) {  // writes dot file
                val dotFilename: String =
                  Configuration.automaticName(machineConfig, result.timedOut, Configuration.dotOutputDirectory, "dot")
                result.toFile(dotFilename)(Some(machineConfig), GraphDOTOutput)
              }
            }
          }
          catch {  // eval failed
            case e: Throwable =>
              System.gc  // ask to free memory
              Thread.sleep(betweenTime.toMillis)
              errors += e.toString
              anyError = true
              // e.printStackTrace
              // System.exit(0)

              if (statsEnabled)
                stats += ""
          }
        }

        if (nbRepetition > 1)
          printAverage(finishedTimes)

        print(s"""\t${!anyError}\t${errors.mkString("\t")}""")

        if (statsEnabled)
          print(s"""\t${stats.mkString("\t")}""")

        if (outputGraphEnabled && statsOutputGraphEnabled)
          print(s"\t$graphInfoStr")

        println
        System.out.flush
      }

      nbRepetition
    }
    else
      0
  }

  /**
    * Run oldAAM machine of Scala-AM on file
    * with MainAsScalaParAM.main of scala-am.jar
    * and corresponding parameters.
    *
    * The oldAAM machine of Scala-AM
    * corresponds to the AAMNS machine of Scala-AM
    * It is a version of the AAM machine of Scala-AM (written by Quentin Stiévenart)
    * without the subsumption part (AAMNS - AAM No Subsumption).
    * It can be considered as the original version of AAM of this Scala-Par-AM.
    */
  def runOld(latticeValue: Configuration.Lattice.Value, bound: Int,
             address: Configuration.Address.Value,
             file: Path,
             timeout: FiniteDuration, nbRepetition: Int): Int = {
    val args: Array[String] = Array[String](
      "-l", latticeValue.toString,
      "-b", bound.toString,
      "-a", address.toString,
      "-f", file.toString,
      "-m", "AAMNS",
      "-r", nbRepetition.toString,
      "-t", timeout.toString,
      "--no-header",
      "--print-oldAAM"
    )

    if (runOldMainMethod == null) {
      println("scala-am.jar or MainAsScalaParAM.main not found! Download it: <https://bitbucket.org/OPiMedia/scala-am/downloads/>")
      0
    }
    else {
      runOldMainMethod.invoke(null, args)
      1
    }
  }


  /** Print average, etc. of data */
  def printAverage(data: Seq[Double]): Unit = {
    if (data.isEmpty)
      print("\t0\t\t")
    else {
      val nb: Int = data.size
      val average: Double = data.sum/nb
      val standardError: Double = data.map(time => Math.abs(time - average)).sum/nb

      print(s"\t$nb\t$average\t$standardError")
    }
  }

  /** Print number of estimation of time */
  def printEstimation(nbPrepared: Int, nbExecuted: Int, timeout: FiniteDuration, estimationTime: FiniteDuration, betweenTime: FiniteDuration): Unit = {
    val nbTotal: Int = nbPrepared + nbExecuted

    def timeStr(reference: Double): String = {
      val time: Double = (nbTotal*(reference + betweenTime.toMillis))/1000.0  // add betweenTime to count all sleep between each computation

      f"$time%.0f seconds = ${time/60.0}%.1f minutes = ${time/(60*60.0)}%.1f hours = ${time/(24*60*60.0)}%.1f days"
    }

    val withTime: String =
      if (estimationTime == FiniteDuration(1, "second")) ""
      else s"\nIf each evaluation takes $estimationTime ---> ${timeStr(estimationTime.toMillis)}"

    println(s"""===== $nbTotal = $nbPrepared prepared + $nbExecuted executed
If each evaluation takes 1 second ---> ${timeStr(1000)} (considering waiting $betweenTime between each computation)$withTime
If maximum with timeout of $timeout ---> ${timeStr(timeout.toMillis)}""")
  }


  /** Print the header corresponding to TSV data */
  def printHeader(nbRepetition: Int, statsEnabled: Boolean, statsEscaped: Boolean): Unit = {
    def num(title: String): String =
      if (nbRepetition > 1)
        (1 to nbRepetition).map(i => s"$title $i").mkString
      else
        title

    print(s"""Lattice\tBound\tAddress\tScheme program\tStep filter?\tMachine\tp\t# states\t# error states\t# error values\t# final states\t# final values\tFinal values\t#${num("\t?\tTime")}""")

    if (nbRepetition > 1)
      print("\t# finished\tAverage time\tStandard error")

    print(s"""\tCorrect?${num("\tError")}""")

    if (statsEnabled) {
      if (statsEscaped) print(s"""${num("\tStats")}""")
      else print("\tStats...")
    }

    println
  }


  /**
    * Print the list of available machines with parallel information
    */
  def printListMachines(): Unit = {
    val machineConfig = new Configuration.MachineConfig

    val lattice: SchemeLattice = Configuration.getLattice(Configuration.Lattice.TypeSet, false, machineConfig.bound)
    val address: AddressWrapper = ClassicalAddress
    val time: TimestampWrapper = ZeroCFA

    println("oldAAM")
    for (machineValue <- Configuration.Machine.values
         if machineValue != Configuration.Machine.oldAAM) {
      val machine = Configuration.getMachine(machineValue, lattice, address, time)
      println(s"$machineValue\t${machine.name}     \tisParallel: ${machine.isParallel}\tminProcess: ${machine.minProcess}\tnbNotEvalProcess: ${machine.nbNotEvalProcess}")
    }
  }


  /**
    * Reads the file filename and returns set of files.
    *
    * The format of read filename is, for each line:
    * [#] FILENAME [# COMMENT]
    * Each line beginning by # is ignored.
    *
    * If filename doesn't exist then return an empty set.
    */
  def readExcepts(filename: String): Set[String] = {
    val excepts: ArrayBuffer[String] = ArrayBuffer.empty[String]

    if (new java.io.File(filename).exists) {
      val file = scala.io.Source.fromFile(filename)

      for (line <- file.getLines if !line.trim.startsWith("#")) {
        val except: String = line.split('#')(0).trim
        if (except != "")
          excepts += except
      }

      file.close
    }

    excepts.toSet
  }



  /** Parses command line parameters and runs */
  val parser = new scopt.OptionParser[ParamsConfig]("Scala-Par-AM") {
    private val separator = ", "

    val now = java.util.Calendar.getInstance()
    head("Scala-Par-AM",
      s"""$scalaParAMVersion (executed with Java ${System.getProperty("java.version")} ${System.getProperty("java.vendor")})${if (Util.isAssertOn) " ASSERT ON!" else ""}""")

    note("""Abstract interpreter for Scheme programs.
The main goal of this program is to test and compare several parallel implementations of a simple abstract interpreter.
""")

    help("help").text("Prints this usage text and exits")

    version("version").text("Prints the version of Scala-Par-AM and exits")

    note("")

    opt[String]("root")
      .action { (x, c) => c.copy(root = x) }
      .text(s"""Root path used to search Scheme programs ("${default.root}" by default)""")

    opt[String]('f', "files")
      .action { (x, c) => c.copy(filenamesPattern = s"glob:$x") }
      .text("Filename or glob pattern to describe file(s) contained Scheme programs to analyse (mutually exclusive with --regex-files option)")

    opt[String]("regex-files")
      .action { (x, c) => c.copy(filenamesPattern = s"regex:$x") }
      .text(s"""Regex pattern to describe file(s) contained Scheme programs to analyse ("${default.filenamesPattern.stripPrefix("regex:")}" by default)""")

    opt[String]("excepts-file")
      .action { (x, c) => c.copy(exceptsFilename = x) }
      .text("File contained a list of filename that will be exclude to --files or --regex-files options (disabled by default)")

    note("")

    opt[Seq[Configuration.Lattice.Value]]('l', "lattices")
      .action { (x, c) => c.copy(lattices = x) }
      .text(s"List of lattices to use (${Configuration.Lattice.values.mkString(separator)}) (${default.lattices.mkString(",")} by default)")

    opt[Unit]("all-lattices")
      .action { (_, c) => c.copy(lattices = Configuration.Lattice.values.toList) }
      .text("""Set all lattices for the --lattices option""")

    opt[Seq[Int]]('b', "bounds")
      .action { (x, c) => c.copy(bounds = if (x.isEmpty) List(100) else x) }
      .text(s"List of bounds for bounded lattice (defaults to ${default.bounds.mkString(",")})")

    opt[Seq[Configuration.Address.Value]]('a', "addresses")
      .action { (x, c) => c.copy(addresses = x) }
      .text(s"List of addresses to use (${Configuration.Address.values.mkString(separator)}) (${default.addresses.mkString(",")} by default)")

    opt[Unit]("all-addresses")
      .action { (_, c) => c.copy(addresses = Configuration.Address.values.toList) }
      .text("""Set all addresses for the --addresses option""")

    opt[Unit]("step-filter")
      .action { (_, c) => c.copy(stepFilters = List(true)) }
      .text("Check if successor states are already visited directly in the evaluation step function (disabled by default) (In general this option causes missing some edges in the output graph)")

    opt[Unit]("step-default-and-filter")
      .action { (_, c) => c.copy(stepFilters = List(false, true)) }
      .text("First without the --step-filter option and then with this option)")

    opt[Seq[Configuration.Machine.Value]]('m', "machines")
      .action { (x, c) => c.copy(machines = x) }
      .text(s"""List of abstract machines to use (${Configuration.Machine.values.mkString(separator)}) (${default.machines.mkString(",")} by default) (Parallel machines ParAAM* need more than 1 process, so you have to set the --processes option)""")

    note("""    * oldAAM: AAMNS (AAM No Subsumption) of Scala-AM <https://bitbucket.org/OPiMedia/scala-am>

    * SeqAAM:   sequential machine where the worklist implemented with a list of states
    * SeqAAMS:  sequential machine where the worklist implemented with a set of states
    * SeqAAMLS: sequential machine where the worklist implemented with a list of sets of states
    * SeqAAMLSH: SeqAAMLS that includes to the worklist only the not halted states

    * ParAAMLSAState: ParAAM-L-SA-state for Parallel AAM - Loop - SenderAggregator - state
    * ParAAMLSASet:   ParAAM-L-SA-set   for Parallel AAM - Loop - SenderAggregator - set
    * ParAAMLSAPart:  ParAAM-L-SA-part  for Parallel AAM - Loop - SenderAggregator - part

    * ParAAMCSState: ParAAM-C-S-state for Parallel AAM - Concurrent - Sender - state
    * ParAAMCSSet:   ParAAM-C-S-set   for Parallel AAM - Concurrent - Sender - set
    * ParAAMCSPart:  ParAAM-C-S-part  for Parallel AAM - Concurrent - Sender - part

    * ParAAMCState:  ParAAM-C-state for Parallel AAM - Concurrent - state
    * ParAAMCSet:    ParAAM-C-set   for Parallel AAM - Concurrent - set
    * ParAAMCPart:   ParAAM-C-part  for Parallel AAM - Concurrent - part
    * ParAAMCHybrid: ParAAM-C-hybrid

    * ParAAMCSetH:    ParAAM-C-set-halt
    * ParAAMCPartH:   ParAAM-C-part-halt
    * ParAAMCHybridH: ParAAM-C-hybrid-halt""")

    val SeqAAMs = List(Configuration.Machine.SeqAAM, Configuration.Machine.SeqAAMS, Configuration.Machine.SeqAAMLS, Configuration.Machine.SeqAAMLSH)

    val ParAAMLSAs = List(Configuration.Machine.ParAAMLSAState, Configuration.Machine.ParAAMLSASet, Configuration.Machine.ParAAMLSAPart)
    val ParAAMCSs = List(Configuration.Machine.ParAAMCSState, Configuration.Machine.ParAAMCSSet, Configuration.Machine.ParAAMCSPart)
    val ParAAMCs = List(Configuration.Machine.ParAAMCState, Configuration.Machine.ParAAMCSet, Configuration.Machine.ParAAMCPart, Configuration.Machine.ParAAMCHybrid,
      Configuration.Machine.ParAAMCSetH, Configuration.Machine.ParAAMCPartH, Configuration.Machine.ParAAMCHybridH)

    val allMachines = SeqAAMs ::: ParAAMLSAs ::: ParAAMCSs ::: ParAAMCs

    opt[Unit]("all-machines-and-old")
      .action { (_, c) => c.copy(machines = Configuration.Machine.values.toList) }
      .text("""Set all machines of Scala-Par-AM and oldAAM (from Scala-AM) for the --machines option""")

    opt[Unit]("all-machines")
      .action { (_, c) => c.copy(machines = allMachines) }
      .text(s"""Set all machines (${allMachines.mkString(", ")}) for the --machines option""")

    opt[Unit]("all-SeqAAM_")
      .action { (_, c) => c.copy(machines = SeqAAMs) }
      .text(s"""Set all machines SeqAAM_ (${SeqAAMs.mkString(", ")}) for the --machines option""")

    opt[Unit]("all-ParAAMLSA_")
      .action { (_, c) => c.copy(machines = ParAAMLSAs) }
      .text(s"""Set all machines ParAAMLSA_ (${ParAAMLSAs.mkString(", ")}) for the --machines option""")
    opt[Unit]("all-ParAAMCS_")
      .action { (_, c) => c.copy(machines = ParAAMCSs) }
      .text(s"""Set all machines ParAAMCS_  (${ParAAMCSs.mkString(", ")}) for the --machines option""")
    opt[Unit]("all-ParAAMC_")
      .action { (_, c) => c.copy(machines = ParAAMCs) }
      .text(s"""Set all machines ParAAMC_   (${ParAAMCs.mkString(", ")}) for the --machines option""")

    val ParAAMStates = List(Configuration.Machine.ParAAMLSAState, Configuration.Machine.ParAAMCSState, Configuration.Machine.ParAAMCState)
    val ParAAMSets = List(Configuration.Machine.ParAAMLSASet, Configuration.Machine.ParAAMCSSet, Configuration.Machine.ParAAMCSet, Configuration.Machine.ParAAMCSetH)
    val ParAAMParts = List(Configuration.Machine.ParAAMLSAPart, Configuration.Machine.ParAAMCSPart, Configuration.Machine.ParAAMCPart, Configuration.Machine.ParAAMCPartH)

    opt[Unit]("all-ParAAM_State")
      .action { (_, c) => c.copy(machines = ParAAMStates) }
      .text(s"""Set all machines ParAAM_State (${ParAAMStates.mkString(", ")}) for the --machines option""")
    opt[Unit]("all-ParAAM_Set")
      .action { (_, c) => c.copy(machines = ParAAMSets) }
      .text(s"""Set all machines ParAAM_Set   (${ParAAMSets.mkString(", ")}) for the --machines option""")
    opt[Unit]("all-ParAAM_Part")
      .action { (_, c) => c.copy(machines = ParAAMParts) }
      .text(s"""Set all machines ParAAM_Part  (${ParAAMParts.mkString(", ")}) for the --machines option""")
    opt[Unit]("list-machines")
      .action { (_, c) => c.copy(isListMachines = true) }
      .text(s"""Print the list of available machines with some information and exit""")

    opt[Seq[Int]]('p', "processes")
      .action { (x, c) => c.copy(processes = x) }
      .validate(x => if (x.exists(_ <= 0)) failure("Option --processes: each value must be > 0!")
      else success)
      .text(s"""List of Maximum numbers of "processes" for ParAAM* machines (${default.processes.mkString(",")} by default) (For each machine, each incompatible number is ignored)""")

    note("")

    opt[String]("prepare-file")
      .action { (x, c) => c.copy(filenamePrepare = x) }
      .text("""Scheme program use to "prepare" JVM before real benchmaks (disabled by default)""")

    opt[Int]('r', "repetition")
      .action { (x, c) => c.copy(nbRepetition = x) }
      .validate(x => if (x < 0) failure("Option --repetition must be > 0!")
      else success)
      .text(s"Number of repetitions of each execution, to make benchmarks (${default.nbRepetition} by default)")

    opt[Duration]('t', "timeout")
      .action { (x, c) => c.copy(timeout = FiniteDuration(x.toNanos, "nanosecond")) }
      .validate(x => if (x.toNanos <= 0) failure("Option --timeout must be positive!")
      else success)
      .text(s"Timeout in FiniteDuration format (${default.timeout} by default)")

    opt[Duration]("betweenTime")
      .action { (x, c) => c.copy(betweenTime = FiniteDuration(x.toNanos, "nanosecond")) }
      .validate(x => if (x.toNanos < 0) failure("Option --betweenTime must be non negative!")
      else success)
      .text(s"Pause duration between two each computations (${default.timeout} by default)")

    note("")

    opt[Unit]("output-graph")
      .action { (_, c) => c.copy(outputGraphEnabled = true) }
      .text(s"""Compute each output graph (disabled by default) (In general the option --step-filter causes missing some edges in this output graph)""")
    opt[Unit]("stats-output-graph")
      .action { (_, c) => c.copy(outputGraphEnabled = true).copy(statsOutputGraphEnabled = true) }
      .text(s"""Print stats about each output graph (disabled by default) (This option enable also the --output-graph option)""")
    opt[Unit]("dot-output-graph")
      .action { (_, c) => c.copy(outputGraphEnabled = true).copy(dotGraphEnabled = true) }
      .text(s"""Save each output graph (for the first repetition) in a dot file in the "${Configuration.dotOutputDirectory}/" directory (disabled by default) (This option enable also the --output-graph option)""")

    note("")

    opt[Unit]("no-header")
      .action { (_, c) => c.copy(headerEnabled = false) }
      .text("Disable header printing")

    opt[Unit]("info-header")
      .action { (_, c) => c.copy(infoHeaderEnabled = true) }
      .text("Print some information like Java version and configuration parameters (disabled by default)")

    opt[Unit]("stats")
      .action { (_, c) => c.copy(statsEnabled = true) }
      .text("Print some statistics information moreover computation times (with this option computation is slower)")

    opt[Unit]("escape-stats")
      .action { (_, c) => c.copy(statsEscaped = true) }
      .text("Escape tabulations in stats printing")

    /* Not used
    opt[Unit]("kamon")
      .action { (_, c) => c.copy(kamonEnabled = true) }
      .text("Enable actors monitoring by Kamon (only usable in sbt) and doesn't exit after computation")
    */

    opt[Unit]("just-list")
      .action { (_, c) => c.copy(justListEnabled = true) }
      .text("Only print the list without computation and print estimation time")

    opt[Duration]("just-list-with")
      .action { (x, c) => c.copy(justListEnabled = true).copy(estimationTime = FiniteDuration(x.toNanos, "nanosecond")) }
      .text("As --just-list but moreover print an estimation with the given time")

    note("""
All sources and more information about Scala-Par-AM are available on Bitbucket:
<https://bitbucket.org/OPiMedia/scala-par-am>.

The oldAAM machine runs in fact the AAMNS (AAM No Subsumption) machine of Scala-AM with scala-am.jar.
Download it if necessary: <https://bitbucket.org/OPiMedia/scala-am/downloads/>.
Only some options of this program can be transferred to this AAMNS machine.
The repository of Scala-AM: <https://bitbucket.org/OPiMedia/scala-am>.""")
  }



  //
  // Main
  //
  parser.parse(args, default) match  {
    case Some(config) =>  // Run all
      if (Util.isAssertOn) println(s"ASSERT ON!")
      if (config.infoHeaderEnabled)
        println(s"""Java: ${System.getProperty("java.version")} ${System.getProperty("java.vendor")}\tavailableProcessors: ${Runtime.getRuntime().availableProcessors()}
$config
""")

      if (config.isListMachines)  // print list of available machines
        printListMachines
      else {                      // runs all other actions
        if (Util.isAssertOn) println(config)

        var nbPrepared: Int = 0
        if (config.filenamePrepare != "") {  // run on one Scheme program to "prepare" JVM
          for (lattice <- config.lattices;
               address <- config.addresses;
               machine <- config.machines;
               process <- Set(config.processes.min, config.processes.max))
            nbPrepared += run(lattice, config.bounds(0),
              address,
              Paths.get(config.filenamePrepare),
              true,  // step filter
              machine,
              process,
              config.timeout, 1, FiniteDuration(0, "second"),
              false, false, false,  // dot disabled
              config.statsEnabled, config.statsEscaped,
              config.justListEnabled)
        }

        if (config.headerEnabled)
          printHeader(config.nbRepetition, config.statsEnabled, config.statsEscaped)

        /* Not used
        if (config.kamonEnabled)
          kamon.Kamon.addReporter(new kamon.prometheus.PrometheusReporter)
        */

        // Run on all combination parameters
        val nbExecuted: Int = foreachRun(config)

        if (config.justListEnabled)
          printEstimation(nbPrepared, nbExecuted,
            config.timeout, config.estimationTime, config.betweenTime)

        /* Not used
        if (config.kamonEnabled) {
          Thread.sleep(10*1000)
          System.err.println("FINISHED. Kamon results available on http://0.0.0.0:9095/ (only if executed in sbt)")
          System.err.flush
          // kamon.Kamon.stopAllReporters
          // The program doesn't exit if not stopAllReporters
        }
        */

        ()
      }

    case None =>  // Error in command line parameters
  }
}
