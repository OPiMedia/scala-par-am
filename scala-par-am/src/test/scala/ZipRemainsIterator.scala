import be.opimedia.scala_par_am.{ZipRemainsIterator, ZipRemainsIterator2}


import org.scalatest._

class ZipRemainsIteratorSpec extends FlatSpec with Matchers {

  "A ZipRemainsIterator" should "give identical results to zip function and the remain" in {
    val zip = ZipRemainsIterator(List(1, 2), List('a', 'b', 'c'))
    zip.hasNext should be (true)
    zip.remains._1.hasNext should be (true)
    zip.remains._2.hasNext should be (true)

    zip.next should be (1, 'a')

    zip.hasNext should be (true)
    zip.remains._1.hasNext should be (true)
    zip.remains._2.hasNext should be (true)

    zip.next should be (2, 'b')

    zip.hasNext should be (false)
    zip.hasNext should be (false)

    zip.remains._1.hasNext should be (false)
    zip.remains._1.toList should be (Nil)
    zip.remains._2.hasNext should be (true)
    zip.remains._2.toList should be (List('c'))
  }

  it should "throw NoSuchElementException if next on empty iterator" in {
    a [NoSuchElementException] should be thrownBy {
      ZipRemainsIterator(Set.empty[String], List(1, 2, 3)).next
    }
    a [NoSuchElementException] should be thrownBy {
      val zip = ZipRemainsIterator(List(1, 2), List(1, 2, 3))
      zip.next
      zip.next
      zip.next
    }
  }
}


class ZipRemainsIterator2Spec extends FlatSpec with Matchers {

  "A ZipRemainsIterator2" should "give identical results to zip function and the remain" in {
    val zip =
      ZipRemainsIterator2(List(1, 2, 3, 4, 5),
                          List(Set('a', 'b'), Set('c'), Set('d', 'e', 'f'), Set('g', 'h')))
    zip.hasNext should be (true)

    zip.next should be (1, 'a')

    zip.hasNext should be (true)

    zip.next should be (2, 'b')

    zip.hasNext should be (true)

    zip.next should be (3, 'c')

    zip.hasNext should be (true)

    zip.next should be (4, 'd')

    zip.hasNext should be (true)

    val (remains1, remains2) = zip.remains

    remains1.hasNext should be (true)
    remains2.hasNext should be (true)

    remains1.toList should be (List(5))
    remains2.toList.map(_.toSet) should be (List(Set('e', 'f'), Set('g', 'h')))
  }

  it should "with first list empty" in {
    val zip = ZipRemainsIterator2(Nil, List(Set(1), Set(2, 3)))

    zip.hasNext should be (false)

    val (remain1, remain2) = zip.remains

    remain1.toList should be (Nil)
    remain2.toList.map(_.toSet) should be (List(Set(1), Set(2, 3)))
  }

  it should "with second list empty" in {
    val zip = ZipRemainsIterator2(List(1, 2), Nil)

    zip.hasNext should be (false)

    val (remain1, remain2) = zip.remains

    remain1.toList should be (List(1, 2))
    remain2.toList should be (Nil)
  }

  it should "with two empty lists" in {
    val zip = ZipRemainsIterator2(Nil, Nil)

    zip.hasNext should be (false)

    val (remain1, remain2) = zip.remains

    remain1.toList should be (Nil)
    remain2.toList should be (Nil)
  }

  it should "throw NoSuchElementException if next on empty iterator" in {
    a [NoSuchElementException] should be thrownBy {
      ZipRemainsIterator2(Set.empty[String], List(Set(1), Set(2, 3))).next
    }
    a [NoSuchElementException] should be thrownBy {
      val zip = ZipRemainsIterator2(List(1, 2), List(Set(1), Set(2, 3)))
      zip.next
      zip.next
      zip.next
    }
  }
}



import org.scalacheck.Properties
import org.scalacheck.Prop.{BooleanOperators, forAll}

object ZipRemainsIteratorProperties extends Properties("ZipRemainsIterator") {

  property("hasNext") = forAll { (seq1: List[Int], seq2: List[String]) =>
    ZipRemainsIterator(seq1, seq2).hasNext == (seq1 zip seq2).iterator.hasNext
  }

  property("next") = forAll { (seq1: List[Int], seq2: List[String]) =>
    ZipRemainsIterator(seq1, seq2).toList == (seq1 zip seq2)
  }

  property("next - nonEmpty") = forAll { (seq1: List[Int], seq2: List[String]) =>
    (seq1.nonEmpty && seq2.nonEmpty) ==>
    (ZipRemainsIterator(seq1, seq2).next == (seq1 zip seq2).iterator.next)
  }

  property("remain") = forAll { (seq1: List[Int], seq2: List[String]) => {
    val (remain1, remain2) = ZipRemainsIterator(seq1, seq2).remains
    (remain1.toList == seq1) && (remain2.toList == seq2)
  }}
}
