import be.opimedia.scala_par_am.{Parts, PartsHybrid}


import org.scalatest._

class PartsSpec extends FlatSpec with Matchers {

  val list = List(Set(1, 2, 3), Set(4, 5), Set.empty[Int], Set(6), Set(7, 8, 9, 10, 11, 12))

  "Report examples" should "have correct parts" in {
    val parts = Parts.parts(List(0 until 42), 42, 5).toList

    parts.size should be (5)

    parts(0).size should be (9)
    parts(1).size should be (9)
    parts(2).size should be (8)
    parts(3).size should be (8)
    parts(4).size should be (8)

    parts(0) should be ((0 until 9).toList.reverse)
    parts(1) should be ((9 until 18).toList.reverse)
    parts(2) should be ((18 until 26).toList.reverse)
    parts(3) should be ((26 until 34).toList.reverse)
    parts(4) should be ((34 until 42).toList.reverse)

    val parts1 = Parts.parts(List(0 until 3), 3, 5).toList

    parts1.size should be (3)

    parts1(0).size should be (1)
    parts1(1).size should be (1)
    parts1(2).size should be (1)

    parts1(0) should be (List(0))
    parts1(1) should be (List(1))
    parts1(2) should be (List(2))
  }

  "A Parts" should "have corresponding sizes" in {
    Parts.parts(List(Set(1)), 1, 1).toList should be (List(Set(1)))
    Parts.parts(List(Set(1)), 1, 2).toList should be (List(Set(1)))
    Parts.parts(List(Set(1), Set(1), Set(1)), 3, 1).toList should be (List(List(1, 1, 1)))

    val parts = Parts.parts(List(Set(1), Set(1, 2), Set(1)), 4, 1).toList

    parts.size should be (1)
    parts.flatten.size should be (4)
    parts.flatten.toSet should be (Set(1, 2))

    val parts2 = Parts.parts(list, 12, 1).toList

    parts2.size should be (1)
    parts2(0).size should be (12)

    val parts3 = Parts.parts(list, 12, 2).toList

    parts3.size should be (2)
    parts3(0).size should be (6)
    parts3(1).size should be (6)

    val parts4 = Parts.parts(list, 12, 11).toList

    parts4.size should be (11)
    parts4.flatten.size should be (12)
    parts4(0).size should be (2)
    parts4(1).size should be (1)
    parts4(10).size should be (1)

    val parts5 = Parts.parts(list, 12, 12).toList

    parts5.size should be (12)
    parts5.flatten.size should be (12)
    parts5(0).size should be (1)
    parts5(1).size should be (1)
    parts5(11).size should be (1)

    val parts6 = Parts.parts(list, 12, 13).toList

    parts6.size should be (12)
    parts6.flatten.size should be (12)
    parts6(0).size should be (1)
    parts6(1).size should be (1)
    parts6(11).size should be (1)
  }

  "A Parts" should "have correct parts" in {
    val parts = Parts.parts(list, 12, 5).toList

    parts.size should be (5)
    parts.flatten.size should be (12)
    parts(0).toSet should be (Set(1, 2, 3))
    parts(1).toSet should be (Set(4, 5, 6))

    (parts(2).toList ++ parts(3).toList ++ parts(4)).toSet should be (Set(7, 8, 9, 10, 11, 12))
    parts(2).size should be (2)
    parts(3).size should be (2)
    parts(4).size should be (2)
  }

  "A PartsHybrid" should "have correct parts (5 parts asked)" in {
    val parts = PartsHybrid.parts(list, 12, 5, 5).toList

    parts.size should be (5)
    parts.flatten.size should be (12)
    parts(0).toSet should be (Set(1, 2, 3))
    parts(1).toSet should be (Set(4, 5))
    parts(2).toSet should be (Set.empty[Int])
    parts(3).toSet should be (Set(6))
    parts(4).toSet should be (Set(7, 8, 9, 10, 11, 12))
  }

  "A PartsHybrid" should "have correct parts (4 parts asked)" in {
    val parts = PartsHybrid.parts(list, 12, 5, 4).toList

    parts.size should be (4)
    parts.flatten.size should be (12)
    parts(0).toSet should be (Set(1, 2, 3, 4, 5))
    parts(1).toSet should be (Set.empty[Int])
    parts(2).toSet should be (Set(6))
    parts(3).toSet should be (Set(7, 8, 9, 10, 11, 12))
  }
}



import org.scalacheck.Properties
import org.scalacheck.Prop.{BooleanOperators, forAll}

object PartsProperties extends Properties("Parts") {
  property("parts") = forAll { (sets: List[Set[Int]], nbPart: Int) =>
    val totalSize = sets.flatten.size
      ((totalSize > 0) && (nbPart > 0) && sets.forall(_.nonEmpty)) ==> {
      val parts = Parts.parts(sets, totalSize, nbPart).toList
      ((parts.size <= nbPart)
        && (parts.flatten.size == totalSize)
        && (parts.flatten.toSet == sets.flatten.toSet))
    }
  }
}
