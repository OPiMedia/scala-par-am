import be.opimedia.scala_par_am._


import org.scalatest._

class MachineSpec extends FlatSpec with Matchers {
  val lattice: SchemeLattice = Configuration.getLattice(Configuration.Lattice.TypeSet, false, 100)
  implicit val isSchemeLattice: IsSchemeLattice[lattice.L] = lattice.isSchemeLattice

  val address: AddressWrapper = ClassicalAddress
  implicit val isAddress = address.isAddress

  val time: TimestampWrapper = ZeroCFA
  implicit val isTimestamp = time.isTimestamp

  val sem = new SchemeSemantics[lattice.L, address.A, time.T](new SchemePrimitives[address.A, lattice.L])

  "A SeqAAM.eval" should "OPi/abs.scm results" in {
    val machineConfig = new Configuration.MachineConfig(process=1)
    val machine = Configuration.getMachine(Configuration.Machine.SeqAAM, lattice, address, time)

    machine.isParallel should be (false)
    machine.minProcess should be (1)
    machine.nbNotEvalProcess should be (0)

    val program: String = Util.fileContent("Scheme-examples/OPi/abs.scm").getOrElse("")
    val result: machine.Output = machine.eval(sem.parse(program), sem, machineConfig)

    result.timedOut should be (false)
    result.numberOfStates should be (8)
    result.errorValues.size should be (0)
    result.finalValues.size should be (1)
    result.finalValues.toString should be ("List(Int)")
  }

  "A ParAAMLSAState.eval" should "OPi/abs.scm results" in {
    val machineConfig = new Configuration.MachineConfig(process=4)
    val machine = Configuration.getMachine(Configuration.Machine.ParAAMLSAState, lattice, address, time)
    val program: String = Util.fileContent("Scheme-examples/OPi/abs.scm").getOrElse("")
    val result: machine.Output = machine.eval(sem.parse(program), sem, machineConfig)

    machine.isParallel should be (true)
    machine.minProcess should be (2)
    machine.nbNotEvalProcess should be (1)

    result.timedOut should be (false)
    result.numberOfStates should be (8)
    result.errorValues.size should be (0)
    result.finalValues.size should be (1)
    result.finalValues.toString should be ("List(Int)")
  }

  "A ParAAMCSState.eval" should "OPi/abs.scm results" in {
    val machineConfig = new Configuration.MachineConfig(process=4)
    val machine = Configuration.getMachine(Configuration.Machine.ParAAMCSState, lattice, address, time)
    val program: String = Util.fileContent("Scheme-examples/OPi/abs.scm").getOrElse("")
    val result: machine.Output = machine.eval(sem.parse(program), sem, machineConfig)

    machine.isParallel should be (true)
    machine.minProcess should be (2)
    machine.nbNotEvalProcess should be (1)

    result.timedOut should be (false)
    result.numberOfStates should be (8)
    result.errorValues.size should be (0)
    result.finalValues.size should be (1)
    result.finalValues.toString should be ("List(Int)")
  }

  "A ParAAMCState.eval" should "OPi/abs.scm results" in {
    val machineConfig = new Configuration.MachineConfig(process=4)
    val machine = Configuration.getMachine(Configuration.Machine.ParAAMCState, lattice, address, time)
    val program: String = Util.fileContent("Scheme-examples/OPi/abs.scm").getOrElse("")
    val result: machine.Output = machine.eval(sem.parse(program), sem, machineConfig)

    machine.isParallel should be (true)
    machine.minProcess should be (1)
    machine.nbNotEvalProcess should be (0)

    result.timedOut should be (false)
    result.numberOfStates should be (8)
    result.errorValues.size should be (0)
    result.finalValues.size should be (1)
    result.finalValues.toString should be ("List(Int)")
  }



  def checkResults(filename: String, numberOfStates: Int, errorValuesSize: Int, finalValuesSize: Int, finalValuesString: String): Unit = {
    for (stepFilterEnabled <- List(false, true);
         machineValue <- Configuration.Machine.values if machineValue != Configuration.Machine.oldAAM;
         process <- List(1, 2, 3, 4, 8)) {
      val machineConfig = new Configuration.MachineConfig(stepFilterEnabled=stepFilterEnabled,
                                                          process=process)

      val machine = Configuration.getMachine(machineValue, lattice, address, time)

      if (machine.isParallel) {
        machine.minProcess >= 1 should be (true)
        machine.nbNotEvalProcess should (be >= 0 and be <= 1)
      }
      else {
        machine.minProcess should be (1)
        machine.nbNotEvalProcess should be (0)
      }

      if (process >= machine.minProcess) {
        println(s"""$filename\t${if (stepFilterEnabled) "step-filter" else "step"}\t$machineValue\t$process""")
        System.out.flush

        val program: String = Util.fileContent("Scheme-examples/" + filename).getOrElse("")
        val result: machine.Output = machine.eval(sem.parse(program), sem, machineConfig)

        result.timedOut should be (false)
        result.numberOfStates should be (numberOfStates)
        result.errorValues.size should be (errorValuesSize)
        result.finalValues.size should be (finalValuesSize)
        result.finalValues.toString should be (finalValuesString)
      }
    }
  }

  "A _.eval" should "OPi/fibonacci_recur.scm results" in {
    checkResults("OPi/fibonacci_recur.scm", 276, 0, 1, "List(Int)")
  }

  "A _.eval" should "Sergey/gcfa2/blur.scm results" in {
    checkResults("Sergey/gcfa2/blur.scm", 188, 2, 2, "List(#f, {#f,#t})")
  }

  "A _.eval" should "Larceny/Gabriel/diviter.scm results" in {
    checkResults("Larceny/Gabriel/diviter.scm", 593, 1, 2, "List(#f, {#f,#t})")
  }
}
