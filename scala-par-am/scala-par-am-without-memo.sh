#!/bin/sh

# Run Scala-Par-AM in a version compiled without memoization of the hash code (except on State)

./java.sh -jar scala-par-am-without-memo.jar "$@"
