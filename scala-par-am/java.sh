#!/bin/sh

# Run the JVM with a specific configuration

java -Dconfig.file=application.conf -Xms3G -Xmx3G -Xss4M "$@"

# -Xms<size>        set initial Java heap size
# -Xmx<size>        set maximum Java heap size
# -Xss<size>        set java thread stack size
