#!/bin/sh

# Run the JVM with a specific configuration

java -Dconfig.file=application.conf -Xms12G -Xmx12G -Xss16M "$@"

# -Xms<size>        set initial Java heap size
# -Xmx<size>        set maximum Java heap size
# -Xss<size>        set java thread stack size
