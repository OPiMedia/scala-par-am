#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
kamon.py (April 8, 2019)

Read http://0.0.0.0:9095/ and parse Kamon results.
"""

import re
import sys
import time
import urllib.request


def parse(lines):
    for line in lines:
        match = re.match('akka_system_processed_messages_total{system="Akka-System-ParAAMCPart-eval",tracked="([^"]+)"} (.*)', line)
        if match:
            print(match.group(1), int(float(match.group(2))), sep='\t')
        print(line)


########
# Main #
########
def main():
    while True:
        request = urllib.request.Request('http://0.0.0.0:9095/')
        try:
            response = urllib.request.urlopen(request)
            content = response.read()
            string = content.decode("utf8")
            lines = string.split('\n')

            break
        except:
            time.sleep(1)

    parse(lines)


if __name__ == '__main__':
    main()
